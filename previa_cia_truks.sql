--
-- Table structure for table `aceite_de_cookies`
--

DROP TABLE IF EXISTS `aceite_de_cookies`;
CREATE TABLE `aceite_de_cookies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aceite_de_cookies`
--

LOCK TABLES `aceite_de_cookies` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `espetaculos`
--

DROP TABLE IF EXISTS `espetaculos`;
CREATE TABLE `espetaculos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ficha_tecnica` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos`
--

LOCK TABLES `espetaculos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned DEFAULT NULL,
  `data` date NOT NULL,
  `horario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade_uf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gratuito` tinyint(1) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_link_home` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agenda_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `agenda_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 27/03/2022
alter table `agenda` modify `endereco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
alter table `agenda` modify `bairro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
alter table `agenda` modify `cidade_uf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
alter table `agenda` modify `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
alter table `agenda` modify `titulo_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
alter table `agenda` modify `titulo_link_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `centro_de_estudos_textos`
--

DROP TABLE IF EXISTS `centro_de_estudos_textos`;
CREATE TABLE `centro_de_estudos_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `centro_de_estudos_textos`
--

LOCK TABLES `centro_de_estudos_textos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `centro_de_estudos_textos_imagens`
--

DROP TABLE IF EXISTS `centro_de_estudos_textos_imagens`;
CREATE TABLE `centro_de_estudos_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `centro_de_estudos_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `centro_de_estudos_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `centro_de_estudos_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `centro_de_estudos_textos_imagens`
--

LOCK TABLES `centro_de_estudos_textos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `companhia`
--

DROP TABLE IF EXISTS `companhia`;
CREATE TABLE `companhia` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia`
--

LOCK TABLES `companhia` WRITE;
INSERT INTO `companhia` VALUES (1,'',NULL,'2022-02-16 03:58:53');
UNLOCK TABLES;

--
-- Table structure for table `companhia_galeria`
--

DROP TABLE IF EXISTS `companhia_galeria`;
CREATE TABLE `companhia_galeria` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_galeria`
--

LOCK TABLES `companhia_galeria` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `companhia_textos`
--

DROP TABLE IF EXISTS `companhia_textos`;
CREATE TABLE `companhia_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_textos`
--

LOCK TABLES `companhia_textos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `companhia_textos_imagens`
--

DROP TABLE IF EXISTS `companhia_textos_imagens`;
CREATE TABLE `companhia_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companhia_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `companhia_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `companhia_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_textos_imagens`
--

LOCK TABLES `companhia_textos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
CREATE TABLE `configuracoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_g` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigo_gtm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pixel_facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinify_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
INSERT INTO `configuracoes` VALUES (1,'Cia Truks','','','','','','','','0q62ghR9brVkCvLN3CytcdNMCvLBgxRs',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `contacao_historias_textos`
--

DROP TABLE IF EXISTS `contacao_historias_textos`;
CREATE TABLE `contacao_historias_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacao_historias_textos`
--

LOCK TABLES `contacao_historias_textos` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 27/03/2022
alter table `contacao_historias_textos` rename `captacao_de_patrocinios_textos`;

--
-- Table structure for table `contacao_historias_textos_imagens`
--

DROP TABLE IF EXISTS `contacao_historias_textos_imagens`;
CREATE TABLE `contacao_historias_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacao_historias_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `contacao_historias_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `contacao_historias_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacao_historias_textos_imagens`
--

LOCK TABLES `contacao_historias_textos_imagens` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 27/03/2022
alter table `contacao_historias_textos_imagens` rename `captacao_de_patrocinios_textos_imagens`;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
CREATE TABLE `contatos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
INSERT INTO `contatos` VALUES (1,'contato@truks.com.br','11 3865 8019','11 99952 8553','https://www.instagram.com/ciatruks/','',NULL,'2022-02-28 21:17:02');
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
CREATE TABLE `contatos_recebidos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `contratacoes_textos`
--

DROP TABLE IF EXISTS `contratacoes_textos`;
CREATE TABLE `contratacoes_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contratacoes_textos`
--

LOCK TABLES `contratacoes_textos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `contratacoes_textos_imagens`
--

DROP TABLE IF EXISTS `contratacoes_textos_imagens`;
CREATE TABLE `contratacoes_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contratacoes_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `contratacoes_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `contratacoes_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contratacoes_textos_imagens`
--

LOCK TABLES `contratacoes_textos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `cursos_galeria`
--

DROP TABLE IF EXISTS `cursos_galeria`;
CREATE TABLE `cursos_galeria` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_galeria`
--

LOCK TABLES `cursos_galeria` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `cursos_planejamentos`
--

DROP TABLE IF EXISTS `cursos_planejamentos`;
CREATE TABLE `cursos_planejamentos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_modulos` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_planejamentos`
--

LOCK TABLES `cursos_planejamentos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `cursos_textos`
--

DROP TABLE IF EXISTS `cursos_textos`;
CREATE TABLE `cursos_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_textos`
--

LOCK TABLES `cursos_textos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `cursos_textos_imagens`
--

DROP TABLE IF EXISTS `cursos_textos_imagens`;
CREATE TABLE `cursos_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cursos_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `cursos_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `cursos_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_textos_imagens`
--

LOCK TABLES `cursos_textos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `depoimentos`
--

DROP TABLE IF EXISTS `depoimentos`;
CREATE TABLE `depoimentos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT 0,
  `aprovado` tinyint(1) NOT NULL DEFAULT 0,
  `espetaculo_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `depoimentos_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `depoimentos_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `depoimentos`
--

LOCK TABLES `depoimentos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_home`
--

DROP TABLE IF EXISTS `espetaculos_home`;
CREATE TABLE `espetaculos_home` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_home_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_home_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_home`
--

LOCK TABLES `espetaculos_home` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_imagens`
--

DROP TABLE IF EXISTS `espetaculos_imagens`;
CREATE TABLE `espetaculos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_imagens_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_imagens_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_imagens`
--

LOCK TABLES `espetaculos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_videos`
--

DROP TABLE IF EXISTS `espetaculos_videos`;
CREATE TABLE `espetaculos_videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_videos_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_videos_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_videos`
--

LOCK TABLES `espetaculos_videos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
CREATE TABLE `fotos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
CREATE TABLE `home` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `frase_agenda` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem_fotos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem_videos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link1_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link1_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link2_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link2_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link3_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link3_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
INSERT INTO `home` VALUES (1,'Confira as datas dos próximos espetáculos e compareça!','img-horiz_20220215215546AfcDTsVyzF.jpeg','pngtree-healing-system-beautiful-starry-4220481_20220215215547E9RrGUacyA.png','CONHEÇA A COMPANHIA','https://google.com','ASSISTA A UM ESPETÁCULO','https://google.com','APRENDA COM A GENTE','https://google.com',NULL,'2022-02-16 03:55:49');
UNLOCK TABLES;

--
-- Table structure for table `tipos`
--

DROP TABLE IF EXISTS `tipos`;
CREATE TABLE `tipos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipos`
--

LOCK TABLES `tipos` WRITE;
INSERT INTO `tipos` VALUES (1,'Galeria',NULL,NULL),(2,'Link',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `data` date NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materias_tipo_id_foreign` (`tipo_id`),
  CONSTRAINT `materias_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materias`
--

LOCK TABLES `materias` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `materias_imagens`
--

DROP TABLE IF EXISTS `materias_imagens`;
CREATE TABLE `materias_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `materia_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materias_imagens_materia_id_foreign` (`materia_id`),
  CONSTRAINT `materias_imagens_materia_id_foreign` FOREIGN KEY (`materia_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materias_imagens`
--

LOCK TABLES `materias_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1012 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE `noticias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `noticias_imagens`
--

DROP TABLE IF EXISTS `noticias_imagens`;
CREATE TABLE `noticias_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `noticia_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `noticias_imagens_noticia_id_foreign` (`noticia_id`),
  CONSTRAINT `noticias_imagens_noticia_id_foreign` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias_imagens`
--

LOCK TABLES `noticias_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `politica_de_privacidade`
--

DROP TABLE IF EXISTS `politica_de_privacidade`;
CREATE TABLE `politica_de_privacidade` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `politica_de_privacidade`
--

LOCK TABLES `politica_de_privacidade` WRITE;
INSERT INTO `politica_de_privacidade` VALUES (1,'<p>Lorem ipsum dolor sit <strong>amet, consectetur adipiscing elit, sed d</strong>o eiusmod tempor incididunt<i> ut labore et dolore magna aliqua. Ut enim</i> ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><p><img class=\"image_resized\" style=\"width:70.01%;\" src=\"http://localhost:8000/assets/img/ckeditor/banner3_20220215220716WUBN0X20e2.jpg\"></p><ol><li>Lorem ipsum dolor sit amet,&nbsp;</li><li>consectetur adipiscing elit, sed do eiusmod tempor&nbsp;</li><li>incididunt ut labore et dolore magna aliqua.&nbsp;</li></ol><p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;</p><ul><li>Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;</li><li>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,&nbsp;</li><li>sunt in culpa qui officia deserunt mollit anim id est laborum.</li></ul><p><a href=\"https://google.com\">link</a></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><figure class=\"table\"><table><thead><tr><th>teste</th><th>teste</th><th>teste</th><th>teste</th></tr></thead><tbody><tr><td>teste</td><td>teste</td><td>teste</td><td>teste</td></tr></tbody></table></figure><p>...</p>',NULL,'2022-03-01 01:15:07');
UNLOCK TABLES;

--
-- Table structure for table `premiacoes`
--

DROP TABLE IF EXISTS `premiacoes`;
CREATE TABLE `premiacoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `premiacoes`
--

LOCK TABLES `premiacoes` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 27/03/2022
alter table `premiacoes` modify `data` date NULL DEFAULT NULL;
alter table `premiacoes` add column `ordem` int(11) NOT NULL DEFAULT 0 after `id`;

--
-- Table structure for table `projetos_especiais`
--

DROP TABLE IF EXISTS `projetos_especiais`;
CREATE TABLE `projetos_especiais` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `texto` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projetos_especiais`
--

LOCK TABLES `projetos_especiais` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `subtitulos`
--

DROP TABLE IF EXISTS `subtitulos`;
CREATE TABLE `subtitulos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subtitulos`
--

LOCK TABLES `subtitulos` WRITE;
INSERT INTO `subtitulos` VALUES 
(1,'A Companhia','a-companhia','O GRUPO: Cia Truks - Teatro de Bonecos',NULL,NULL),
(2,'Agenda','agenda','Acompanhe aqui todas as nossas apresentações e compareça!',NULL,NULL),
(3,'Espetáculos','espetaculos','Conheça cada detalhe de todos os nossos espetáculos',NULL,NULL),
(4,'Cursos e Oficinas','cursos-e-oficinas','Aprenda com a gente: Nossa metodologia de trabalho',NULL,NULL),
(5,'Projetos Especiais','projetos-especiais','Saiba mais sobre alguns projetos em que trabalhamos',NULL,NULL),
(6,'Centro de Estudos','centro-de-estudos','Centro de estudos e práticas do teatro de animação',NULL,NULL),
(7,'Captação de Patrocínios','captacao-de-patrocinios','Centro de estudos e práticas do teatro de animação',NULL,NULL),
(8,'Contratações','contratacoes','Informações gerais',NULL,NULL),
(9,'Galeria de Fotos','galeria-de-fotos','Confira momentos inesquecíveis da Cia Truks',NULL,NULL),
(10,'Vídeos','videos','Nossos registros animados',NULL,NULL),
(11,'Notícias','noticias','Acompanhe aqui todas as nossas novidades!',NULL,NULL),
(12,'Premiações','premiacoes','Reconhecimento e orgulho!',NULL,NULL),
(13,'Críticas e Matérias','criticas-e-materias','Onde fomos citados e comentados',NULL,NULL),
(14,'Contato','contato','Fale conosco',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net',NULL,'$2y$10$KdN74HfFHRVs16KLEo8if.x8koCMpJ2GplV60LWWZicM4m/wqavca',NULL,NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
UNLOCK TABLES;


-- Dump completed on 2022-03-01 19:14:07
