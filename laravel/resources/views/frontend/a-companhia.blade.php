@extends('frontend.layout.template')

@section('content')

<main class="a-companhia">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'a-companhia')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
        <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$companhia->video }}"></iframe>
    </section>

    @foreach($textos as $texto)
    <section class="textos">
        <div class="center">
            <article class="left">
                {!! $texto->texto !!}
            </article>
            <article class="right">
                @php $imagensTxt = $imagens->where('texto_id', $texto->id) @endphp
                @foreach($imagensTxt as $imagem)
                <img src="{{ asset('assets/img/a-companhia/imagens/'.$imagem->imagem) }}" alt="" class="img-texto">
                @endforeach
            </article>
        </div>
    </section>
    @endforeach

    @if(count($galeria) > 0)
    <section class="galeria">
        <img src="{{ asset('assets/img/layout/estrela-vermelhaLuzesBrilhofundo.png') }}" alt="" class="img-estrela">
        <div class="center masonry-grid">
            @foreach($galeria as $imagem)
            <a href="{{ asset('assets/img/a-companhia/galeria/big/'.$imagem->imagem) }}" class="link-img-galeria fancybox" rel="companhia_galeria">
                <img src="{{ asset('assets/img/a-companhia/galeria/'.$imagem->imagem) }}" class="img-galeria" alt="">
            </a>
            @endforeach
        </div>
    </section>
    @endif

    @include('frontend.depoimentos-internas')

</main>

@endsection