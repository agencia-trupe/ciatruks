@extends('frontend.layout.template')

@section('content')

<main class="noticias">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'noticias')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="lista-noticias center">
        @foreach($noticias as $noticia)
        <a href="{{ route('noticias.show', $noticia->slug) }}" class="link-noticia">
            <img src="{{ asset('assets/img/noticias/'.$noticia->imagem) }}" class="img-noticia">
            <div class="dados">
                <div class="data">{{ utf8_encode(strftime("%d %h %Y", strtotime($noticia->data))) }}</div>
                <h4 class="titulo">{{ $noticia->titulo }}</h4>
                @if($noticia->frase)
                <p class="frase">{{ $noticia->frase }}</p>
                @endif
            </div>
        </a>
        @endforeach
    </section>

    <section class="ver-mais center">
        <a href="" class="link-ver-mais-noticias">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VER MAIS</p>
        </a>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection