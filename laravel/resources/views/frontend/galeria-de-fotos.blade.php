@extends('frontend.layout.template')

@section('content')

<main class="galeria-de-fotos">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'galeria-de-fotos')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="galeria">
        <div class="center masonry-grid-fotos">
            @foreach($galeria as $imagem)
            <a href="{{ asset('assets/img/fotos/big/'.$imagem->imagem) }}" class="link-img-galeria fancybox" rel="fotos">
                <img src="{{ asset('assets/img/fotos/'.$imagem->imagem) }}" class="img-galeria" alt="">
            </a>
            @endforeach
        </div>

        <a href="" class="link-ver-mais-fotos">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VER MAIS</p>
        </a>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection