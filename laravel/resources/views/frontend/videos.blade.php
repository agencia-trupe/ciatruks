@extends('frontend.layout.template')

@section('content')

<main class="videos">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'videos')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="lista-videos center">
        @foreach($videos as $video)
        <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$video->video }}"></iframe>
        @endforeach
    </section>

    <section class="ver-mais center">
        <a href="" class="link-ver-mais-videos">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VER MAIS</p>
        </a>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection