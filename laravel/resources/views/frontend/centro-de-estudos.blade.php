@extends('frontend.layout.template')

@section('content')

<main class="centro-de-estudos">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'centro-de-estudos')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    @foreach($textos as $texto)
    <section class="textos">
        <div class="center">
            <article class="left">
                {!! $texto->texto !!}
            </article>
            <article class="right">
                @php $imagensTxt = $imagens->where('texto_id', $texto->id) @endphp
                @foreach($imagensTxt as $imagem)
                <img src="{{ asset('assets/img/centro-de-estudos/imagens/'.$imagem->imagem) }}" alt="" class="img-texto">
                @endforeach
            </article>
        </div>
    </section>
    @endforeach

    @include('frontend.depoimentos-internas')

</main>

@endsection