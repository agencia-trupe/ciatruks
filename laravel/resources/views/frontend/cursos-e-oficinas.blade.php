@extends('frontend.layout.template')

@section('content')

<main class="cursos-e-oficinas">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'cursos-e-oficinas')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    @foreach($textos as $texto)
    <section class="textos">
        <div class="center">
            <article class="left">
                {!! $texto->texto !!}
            </article>
            <article class="right">
                @php $imagensTxt = $imagens->where('texto_id', $texto->id) @endphp
                @foreach($imagensTxt as $imagem)
                <img src="{{ asset('assets/img/cursos-e-oficinas/imagens/'.$imagem->imagem) }}" alt="" class="img-texto">
                @endforeach
            </article>
        </div>
    </section>
    @endforeach

    @if(count($planejamentos) > 0)
    <section class="planejamentos">
        <div class="center">
            <h4 class="titulo">CONHEÇA AQUI OS PLANEJAMENTOS BÁSICOS DAS OFICINAS:</h4>
            @foreach($planejamentos as $planejamento)
            <a href="{{ route('cursos-e-oficinas.planejamento', $planejamento->slug) }}" class="link-planejamento">
                <img src="{{ asset('assets/img/layout/seta-reta-LuzesesBrilhoFundo-direita.png') }}" alt="" class="img-seta">
                {{ $planejamento->titulo }}
            </a>
            @endforeach
        </div>
    </section>
    @endif

    @if(count($galeria) > 0)
    <section class="galeria">
        <div class="center masonry-grid">
            @foreach($galeria as $imagem)
            <a href="{{ asset('assets/img/cursos-e-oficinas/galeria/big/'.$imagem->imagem) }}" class="link-img-galeria fancybox" rel="cursos_galeria">
                <img src="{{ asset('assets/img/cursos-e-oficinas/galeria/'.$imagem->imagem) }}" class="img-galeria" alt="">
            </a>
            @endforeach
        </div>
    </section>
    @endif

    @include('frontend.depoimentos-internas')

</main>

@endsection