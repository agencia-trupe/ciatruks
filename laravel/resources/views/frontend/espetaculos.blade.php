@extends('frontend.layout.template')

@section('content')

<main class="espetaculos">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'espetaculos')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="lista-espetaculos center">
        @foreach($espetaculos as $espetaculo)
        <a href="{{ route('espetaculos.show', $espetaculo->slug) }}" class="link-espetaculo" style="background-image: url({{ asset('assets/img/layout/moldura-luzes.png') }})">
            <img src="{{ asset('assets/img/espetaculos/'.$espetaculo->imagem) }}" class="img-espetaculo">
            <div class="titulo">
                {{ $espetaculo->titulo }}
            </div>
        </a>
        @endforeach
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection