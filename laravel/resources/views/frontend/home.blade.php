@extends('frontend.layout.template')

@section('content')

<main class="home">

    <section class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <div class="titulo">{!! $banner->titulo !!}</div>
        </div>
        @endforeach
    </section>

    <section class="agenda-home">
        <div class="center">
            <article class="legenda">
                <h2 class="titulo-agenda">Agenda</h2>
                <p class="frase-agenda">{{ $home->frase_agenda }}</p>
                <a href="{{ route('agenda') }}" class="link-agenda">
                    ver agenda completa
                    <img src="{{ asset('assets/img/layout/seta-tortinha-direita.svg') }}" alt="" class="img-seta">
                </a>
            </article>

            <article class="agenda">
                @foreach($agendaHome as $evento)
                <div class="evento">
                    <a href="{{ route('espetaculos.show', $evento->espetaculo_slug) }}" title="{{ $evento->espetaculo_titulo }}" class="link-espetaculo">
                        <img src="{{ asset('assets/img/espetaculos/agenda/'.$evento->espetaculo_imagem) }}" alt="{{ $evento->espetaculo_titulo }}" class="img-espetaculo">
                    </a>
                    <div class="data">
                        <p class="dia">{{ utf8_encode(strftime("%d", strtotime($evento->data))) }}</p>
                        <p class="mes">{{ utf8_encode(strftime("%B", strtotime($evento->data))) }}</p>
                    </div>
                    <h4 class="titulo">{{ $evento->espetaculo_titulo }}</h4>
                    <p class="tipo">{{ $evento->espetaculo_tipo }}</p>
                    <p class="horario-local">
                        <img src="{{ asset('assets/img/layout/icone-relógio.svg') }}" alt="" class="img-relogio">
                        {{ $evento->horario }} • {{ $evento->local }}
                    </p>
                    @if($evento->endereco)
                    <p class="endereco">{{ $evento->bairro }} • {{ $evento->cidade_uf }}</p>
                    @endif
                    @if($evento->link)
                    <a href="{{ $evento->link }}" class="link-ingresso">
                        @if($evento->gratuito == 1)
                        <img src="{{ asset('assets/img/layout/icone-presente.svg') }}" alt="" class="img-gratuito">
                        @else
                        <img src="{{ asset('assets/img/layout/icone-dinheiro.svg') }}" alt="" class="img-pago">
                        @endif
                        {{ $evento->titulo_link_home }}
                    </a>
                    @endif
                </div>
                @endforeach
            </article>
        </div>
    </section>

    <section class="espetaculos-home">
        <div class="center">
            <article class="legenda">
                <p class="frase">Alguns dos nossos</p>
                <div class="titulo">
                    ESPETÁCULOS
                    <div class="estrelinhas">
                        <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                        <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                        <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                        <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                        <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                    </div>
                </div>
            </article>

            <article class="espetaculos">
                @foreach($espetaculosHome as $espetaculo)
                <a href="{{ route('espetaculos.show', $espetaculo->slug) }}" class="link-espetaculo" style="background-image: url({{ asset('assets/img/layout/moldura-luzes.png') }})">
                    <img src="{{ asset('assets/img/espetaculos/'.$espetaculo->imagem) }}" class="img-espetaculo">
                    <div class="titulo">
                        {{ $espetaculo->titulo }}
                    </div>
                </a>
                @endforeach
            </article>

            <a href="{{ route('espetaculos') }}" class="link-espetaculos">
                <div class="estrelinhas">
                    <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                    <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                    <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                    <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                    <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                </div>
                conheça todos os espetáculos da Cia. Truks
                <img src="{{ asset('assets/img/layout/seta-tortinha-direita-laranja.svg') }}" alt="" class="img-seta">
            </a>
        </div>
    </section>

    <section class="bg-galeria" style="background-image: url({{ asset('assets/img/home/'.$home->imagem_fotos) }})">
        <a href="{{ route('galeria-de-fotos') }}" class="link-galeria-fotos">
            <img src="{{ asset('assets/img/layout/icone-maquinaFotografica.svg') }}" alt="" class="img-maquina">
            <p class="titulo-link">
                VISITE NOSSA GALERIA DE FOTOS
                <img src="{{ asset('assets/img/layout/seta-tortinha-direita-branco.svg') }}" alt="" class="img-seta">
            </p>
        </a>
    </section>

    <section class="links-home">
        <div class="center">
            <a href="{{ $home->link1_link }}" class="link-um">
                <p class="titulo-link">{{ $home->link1_titulo }}</p>
                <img src="{{ asset('assets/img/layout/seta-dobrada-vermelhaLuzes-direita.png') }}" alt="" class="img-seta">
            </a>
            <a href="{{ $home->link2_link }}" class="link-dois">
                <p class="titulo-link">{{ $home->link2_titulo }}</p>
                <img src="{{ asset('assets/img/layout/seta-dobrada-vermelhaLuzes-direita.png') }}" alt="" class="img-seta">
            </a>
            <a href="{{ $home->link3_link }}" class="link-tres">
                <p class="titulo-link">{{ $home->link3_titulo }}</p>
                <img src="{{ asset('assets/img/layout/seta-dobrada-vermelhaLuzes-direita.png') }}" alt="" class="img-seta">
            </a>
        </div>
    </section>

    <section class="bg-galeria" style="background-image: url({{ asset('assets/img/home/'.$home->imagem_videos) }})">
        <a href="{{ route('videos') }}" class="link-galeria-videos">
            <img src="{{ asset('assets/img/layout/icone-videos.svg') }}" alt="" class="img-videos">
            <p class="titulo-link">
                VISITE NOSSA GALERIA DE VÍDEOS
                <img src="{{ asset('assets/img/layout/seta-tortinha-direita-branco.svg') }}" alt="" class="img-seta">
            </p>
        </a>
    </section>

    <section class="novidades">
        <div class="center">
            <a href="{{ route('noticias') }}" class="link-novidades">
                <img src="{{ asset('assets/img/layout/seta-reta-LuzesesBrilhoFundo-direita.png') }}" alt="" class="img-seta">
                <p class="titulo">NOVIDADES</p>
            </a>

            <article class="noticias">
                @foreach($noticias as $noticia)
                <div class="noticia">
                    <img src="{{ asset('assets/img/layout/pendulo-olofote.png') }}" alt="" class="img-pendulo">
                    <p class="titulo-noticia">{{ $noticia->titulo }}</p>
                    @if(isset($noticia->frase))
                    <p class="frase-noticia">{{ $noticia->frase }}</p>
                    @endif
                    <a href="{{ route('noticias.show', $noticia->slug) }}" class="link-ler-mais">
                        <img src="{{ asset('assets/img/layout/seta-reta-luzes-direita.png') }}" alt="" class="img-seta">
                        <span>LER MAIS</span>
                    </a>
                </div>
                @endforeach
            </article>
        </div>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection