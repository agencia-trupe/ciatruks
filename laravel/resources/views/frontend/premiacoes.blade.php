@extends('frontend.layout.template')

@section('content')

<main class="premiacoes">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'premiacoes')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="lista-premiacoes center">
        @foreach($premiacoes as $premiacao)
        <article class="premiacao">
            @if($premiacao->imagem)
            <img src="{{ asset('assets/img/premiacoes/'.$premiacao->imagem) }}" alt="" class="img-premiacao">
            @else
            <img src="{{ asset('assets/img/layout/trofeu-luzesBrilhoFundo.png') }}" alt="" class="img-icone">
            @endif
            <div class="dados">
                <h4 class="titulo">{{ $premiacao->titulo }}</h4>
                @if($premiacao->data)
                <p class="data">{{ utf8_encode(strftime("%B de %Y", strtotime($premiacao->data))) }}</p>
                @endif
                <div class="texto">{!! $premiacao->texto !!}</div>
            </div>
        </article>
        @endforeach
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection