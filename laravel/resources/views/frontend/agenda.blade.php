@extends('frontend.layout.template')

@section('content')

<main class="agenda">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'agenda')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="lista-agenda center">
        @foreach($agenda as $evento)
        <article class="evento">
            <a href="{{ route('espetaculos.show', $evento->espetaculo_slug) }}" class="link-img-espetaculo">
                <img src="{{ asset('assets/img/espetaculos/agenda/'.$evento->espetaculo_imagem) }}" alt="{{ $evento->espetaculo_titulo }}" class="img-espetaculo">
            </a>
            <div class="data">
                <p class="dia">{{ utf8_encode(strftime('%d', strtotime($evento->data))) }}</p>
                <p class="mes">{{ utf8_encode(strftime('%B', strtotime($evento->data))) }}</p>
            </div>
            <div class="informacoes">
                <a href="{{ route('espetaculos.show', $evento->espetaculo_slug) }}" class="link-espetaculo">{{ $evento->espetaculo_titulo }}</a>
                <p class="tipo">{{ $evento->espetaculo_tipo }}</p>
                <p class="horario">
                    <img src="{{ asset('assets/img/layout/icone-relógio-laranja.svg') }}" alt="" class="img-relogio">
                    {{ $evento->horario }} • {{ utf8_encode(strftime('%d de %B de %Y', strtotime($evento->data))) }}
                </p>
                <p class="local">
                    <img src="{{ asset('assets/img/layout/icone-localizacao.svg') }}" alt="" class="img-local">
                    {{ $evento->local }}
                </p>
                @if($evento->endereco)
                <p class="endereco">{{ $evento->endereco }}</p>
                <p class="bairro-cidade">{{ $evento->bairro }} • {{ $evento->cidade_uf }}</p>
                @endif
                @if($evento->telefone)
                @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
                <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
                @endif
                @if($evento->observacao)
                <p class="observacao">{{ $evento->observacao }}</p>
                @endif
                @if($evento->link)
                <a href="{{ $evento->link }}" class="link-ingresso">
                    @if($evento->gratuito == 1)
                    <div class="ingresso">
                        <img src="{{ asset('assets/img/layout/icone-presente.svg') }}" alt="" class="img-ingresso">
                    </div>
                    @else
                    <div class="ingresso">
                        <img src="{{ asset('assets/img/layout/icone-dinheiro.svg') }}" alt="" class="img-ingresso">
                    </div>
                    @endif
                    <p class="titulo-ingresso">{{ $evento->titulo_link }}</p>
                </a>
                @endif
            </div>
        </article>
        <div class="estrelinhas"></div>
        @endforeach
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection