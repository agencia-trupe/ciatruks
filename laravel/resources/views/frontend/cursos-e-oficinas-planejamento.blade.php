@extends('frontend.layout.template')

@section('content')

<main class="cursos-e-oficinas">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'cursos-e-oficinas')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="planejamento center">
        <article class="left">
            {!! $planejamento->texto !!}
        </article>
        @if($planejamento->texto_modulos)
        <article class="right">
            <div class="texto-modulos">{!! $planejamento->texto_modulos !!}</div>
        </article>
        @endif
    </section>

    <section class="voltar">
        <div class="center">
            <a href="{{ route('cursos-e-oficinas') }}" class="link-voltar center">
                <img src="{{ asset('assets/img/layout/seta-retaLuzesBrilhoFundo-esquerda.png') }}" alt="" class="img-seta">
                voltar
            </a>
        </div>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection