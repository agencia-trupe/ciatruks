@extends('frontend.layout.template')

@section('content')

<main class="espetaculos">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'espetaculos')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="espetaculo center">
        <article class="sobre">
            <div class="moldura" style="background-image: url({{ asset('assets/img/layout/moldura-luzes.png') }})">
                <img src="{{ asset('assets/img/espetaculos/'.$espetaculo->imagem) }}" class="img-espetaculo">
            </div>
            <div class="textos">
                <h1 class="titulo">{{ $espetaculo->titulo }}</h1>
                <div class="descricao">{!! $espetaculo->descricao !!}</div>
            </div>
        </article>
        <article class="dados">
            @if($espetaculo->texto)
            <div class="texto">{!! $espetaculo->texto !!}</div>
            @endif
            @if($espetaculo->ficha_tecnica)
            <div class="ficha-tecnica">{!! $espetaculo->ficha_tecnica !!}</div>
            @endif
        </article>
    </section>

    @if(count($videos) > 0)
    <section class="videos" style="background-image: url({{ asset('assets/img/layout/bg-videos.png') }})">
        <div class="center">
            @foreach($videos as $video)
            <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$video->video }}"></iframe>
            @endforeach
        </div>
    </section>
    @endif

    @if(count($imagens) > 0)
    <section class="galeria">
        <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
        <div class="center masonry-grid">
            @foreach($imagens as $imagem)
            <a href="{{ asset('assets/img/espetaculos/imagens/big/'.$imagem->imagem) }}" class="link-img-galeria fancybox" rel="espetaculos_imagens">
                <img src="{{ asset('assets/img/espetaculos/imagens/'.$imagem->imagem) }}" class="img-galeria" alt="">
            </a>
            @endforeach
        </div>
    </section>
    @endif

    @include('frontend.depoimentos-internas')

</main>

@endsection