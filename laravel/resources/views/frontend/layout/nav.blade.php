<div class="menu">

    <a href="{{ route('home') }}" class="link-nav first @if(Tools::routeIs('home*')) active @endif">HOME</a>
    <a href="{{ route('a-companhia') }}" class="link-nav second @if(Tools::routeIs('a-companhia*')) active @endif">A COMPANHIA</a>
    <a href="{{ route('agenda') }}" class="link-nav third @if(Tools::routeIs('agenda*')) active @endif">AGENDA</a>
    <a href="{{ route('espetaculos') }}" class="link-nav first @if(Tools::routeIs('espetaculos*')) active @endif">ESPETÁCULOS</a>
    <a href="{{ route('cursos-e-oficinas') }}" class="link-nav second @if(Tools::routeIs('cursos-e-oficinas*')) active @endif">CURSOS E OFICINAS</a>
    <a href="{{ route('projetos-especiais') }}" class="link-nav third @if(Tools::routeIs('projetos-especiais*')) active @endif">PROJETOS ESPECIAIS</a>
    <a href="{{ route('centro-de-estudos') }}" class="link-nav first @if(Tools::routeIs('centro-de-estudos*')) active @endif">CENTRO DE ESTUDOS</a>
    <a href="{{ route('captacao-de-patrocinios') }}" class="link-nav second captacao @if(Tools::routeIs('captacao-de-patrocinios*')) active @endif">CAPTAÇÃO DE PATROCÍNIOS</a>
    <a href="{{ route('contratacoes') }}" class="link-nav third @if(Tools::routeIs('contratacoes*')) active @endif">CONTRATAÇÕES</a>
    <a href="{{ route('galeria-de-fotos') }}" class="link-nav first @if(Tools::routeIs('galeria-de-fotos*')) active @endif">GALERIA DE FOTOS</a>
    <a href="{{ route('videos') }}" class="link-nav second @if(Tools::routeIs('videos*')) active @endif">VÍDEOS</a>
    <a href="{{ route('noticias') }}" class="link-nav third @if(Tools::routeIs('noticias*')) active @endif">NOTÍCIAS</a>
    <a href="{{ route('premiacoes') }}" class="link-nav first @if(Tools::routeIs('premiacoes*')) active @endif">PREMIAÇÕES</a>
    <a href="{{ route('criticas-e-materias') }}" class="link-nav second @if(Tools::routeIs('criticas-e-materias*')) active @endif">CRÍTICAS E MATÉRIAS</a>
    <a href="{{ route('contato') }}" class="link-nav third @if(Tools::routeIs('contato*')) active @endif">CONTATO</a>

</div>