<header>

    <a href="{{ route('home') }}" class="link-home">
        <img src="{{ asset('assets/img/layout/marca-cia-truks.svg') }}" alt="{{ $config->title }}" class="img-logo">
    </a>

    <button id="mobile-toggle" type="button" role="button" class="@if(Tools::routeIs('home')) btn-home @endif">
        <span class="lines"></span>
    </button>

    <nav>
        @include('frontend.layout.nav')
    </nav>

</header>