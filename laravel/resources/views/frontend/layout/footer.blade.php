<footer>

    <div class="center">

        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-cia-truks.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>

        <article class="menu">
            <a href="{{ route('home') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">HOME</a>
            <a href="{{ route('a-companhia') }}" class="link-footer @if(Tools::routeIs('a-companhia*')) active @endif">A COMPANHIA</a>
            <a href="{{ route('agenda') }}" class="link-footer @if(Tools::routeIs('agenda*')) active @endif">AGENDA</a>
            <a href="{{ route('espetaculos') }}" class="link-footer @if(Tools::routeIs('espetaculos*')) active @endif">ESPETÁCULOS</a>
            <a href="{{ route('cursos-e-oficinas') }}" class="link-footer @if(Tools::routeIs('cursos-e-oficinas*')) active @endif">CURSOS E OFICINAS</a>
            <a href="{{ route('projetos-especiais') }}" class="link-footer @if(Tools::routeIs('projetos-especiais*')) active @endif">PROJETOS ESPECIAIS</a>
            <a href="{{ route('centro-de-estudos') }}" class="link-footer @if(Tools::routeIs('centro-de-estudos*')) active @endif">CENTRO DE ESTUDOS</a>
            <a href="{{ route('captacao-de-patrocinios') }}" class="link-footer @if(Tools::routeIs('captacao-de-patrocinios*')) active @endif">CAPTAÇÃO DE PATROCÍNIOS</a>
            <a href="{{ route('contratacoes') }}" class="link-footer @if(Tools::routeIs('contratacoes*')) active @endif">CONTRATAÇÕES</a>
            <a href="{{ route('galeria-de-fotos') }}" class="link-footer @if(Tools::routeIs('galeria-de-fotos*')) active @endif">GALERIA DE FOTOS</a>
            <a href="{{ route('videos') }}" class="link-footer @if(Tools::routeIs('videos*')) active @endif">VÍDEOS</a>
            <a href="{{ route('noticias') }}" class="link-footer @if(Tools::routeIs('noticias*')) active @endif">NOTÍCIAS</a>
            <a href="{{ route('premiacoes') }}" class="link-footer @if(Tools::routeIs('premiacoes*')) active @endif">PREMIAÇÕES</a>
            <a href="{{ route('criticas-e-materias') }}" class="link-footer @if(Tools::routeIs('criticas-e-materias*')) active @endif">CRÍTICAS E MATÉRIAS</a>
            <a href="{{ route('contato') }}" class="link-footer @if(Tools::routeIs('contato*')) active @endif">CONTATO</a>
        </article>

        <article class="espetaculos">
            @foreach($espetaculos as $espetaculo)
            <a href="{{ route('espetaculos.show', $espetaculo->slug) }}" class="link-espetaculo @if(url()->current() == route('espetaculos.show', $espetaculo->slug)) active @endif">{{ $espetaculo->titulo }}</a>
            @endforeach
        </article>

        <article class="informacoes">
            @php
            $telefone = str_replace(" ", "", $contato->telefone);
            $whatsapp = str_replace(" ", "", $contato->whatsapp);
            @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
            <a href="https://api.whatsapp.com/send?phone=+55{{ $whatsapp }}" class="link-whatsapp" target="_blank">{{ $contato->whatsapp }}</a>

            <a href="{{ $contato->instagram }}" target="_blank" class="link-instagram">@ciatruks</a>
            <a href="mailto:{{ $contato->email }}" class="link-email">{{ $contato->email }}</a>

            <a href="{{ route('politica-de-privacidade') }}" class="link-politica @if(Tools::routeIs('contato*')) politica-de-privacidade @endif">Nossa Política de Privacidade</a>

            <div class="copyright">
                <p class="direitos"> © {{ date('Y') }} {{ config('app.name') }} • Todos os direitos reservados</p>
                <div class="links">
                    <a href="https://www.trupe.net" target="_blank" class="link-trupe">Criação de sites: </a>
                    <a href="https://www.trupe.net" target="_blank" class="link-trupe"> Trupe Agência Criativa</a>
                </div>
            </div>
        </article>

    </div>

</footer>