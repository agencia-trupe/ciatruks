@extends('frontend.layout.template')

@section('content')

<main class="depoimentos">

    <section class="pagina-depoimentos">
        <div class="center">
            <article class="left">
                <div class="caixa-titulo">
                    <img src="{{ asset('assets/img/layout/balaoDeFala-luzesBrilhoFundo.png') }}" alt="" class="img-titulo">
                    <p class="titulo">DEPOIMENTOS</p>
                    <a href="" class="link-depoimentos">ver todos os depoimentos</a>
                </div>
                <form action="{{ route('depoimentos.post') }}" method="POST" class="form-depoimento">
                    <h4 class="titulo">DEIXAR MEU DEPOIMENTO:</h4>
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <textarea name="mensagem" placeholder="depoimento" required>{{ old('mensagem') }}</textarea>
                    <button type="submit" class="btn-enviar">ENVIAR</button>

                    @if($errors->any())
                    <div class="flash flash-erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        <p>Depoimento enviado com sucesso!</p>
                    </div>
                    @endif
                </form>
            </article>

            <article class="lista-depoimentos">
                @foreach($depoimentos as $depoimento)
                <div class="depoimento">
                    <a href="" class="link-espetaculo" title="{{ $depoimento->titulo }}">
                        <img src="{{ asset('assets/img/espetaculos/depoimentos/'.$depoimento->imagem) }}" alt="{{ $depoimento->titulo }}" class="img-depoimento">
                    </a>
                    <div class="dados">
                        <p class="mensagem">{!! $depoimento->mensagem !!}</p>
                        <p class=" contato">{{ $depoimento->nome }} - {{ $depoimento->created_at }}</p>
                        <div class="estrelinhas">
                            <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                            <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                            <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                            <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                            <img src="{{ asset('assets/img/layout/estrelinha.png') }}" alt="" class="img-estrela">
                        </div>
                    </div>
                </div>
                @endforeach
            </article>
        </div>

        <a href="" class="link-ver-mais-depoimentos">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VER MAIS</p>
        </a>
    </section>

</main>

@endsection