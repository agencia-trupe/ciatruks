@extends('frontend.layout.template')

@section('content')

<main class="noticias">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'like', 'noticias')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="noticia center">
        <article class="imagem-data">
            <img src="{{ asset('assets/img/noticias/show/'.$noticia->imagem) }}" class="img-noticia">
            <div class="data">{{ utf8_encode(strftime("%d %h %Y", strtotime($noticia->data))) }}</div>
        </article>
        <article class="cx-titulo">
            <h1 class="titulo">{{ $noticia->titulo }}</h1>
        </article>
        <article class="dados-imagens">
            <div class="textos">
                <p class="frase">{{ $noticia->frase }}</p>
                {!! $noticia->texto !!}
            </div>
            <div class="imagens">
                @foreach($imagens as $imagem)
                <img src="{{ asset('assets/img/noticias/imagens/'.$imagem->imagem) }}" alt="" class="img-texto">
                @endforeach
            </div>
        </article>
    </section>

    <section class="voltar center">
        <a href="{{ route('noticias') }}" class="link-voltar-noticias">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VOLTAR</p>
        </a>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection