@extends('frontend.layout.template')

@section('content')

<main class="projetos-especiais">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'projetos-especiais')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    @foreach($projetos as $projeto)
    <section class="projeto">
        <div class="center">
            @if($projeto->texto)
            <article class="texto">{!! $projeto->texto !!}</article>
            @endif
            @if($projeto->video)
            <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$projeto->video }}"></iframe>
            @endif
        </div>
    </section>
    @endforeach

    @include('frontend.depoimentos-internas')

</main>

@endsection