@extends('frontend.layout.template')

@section('content')

<main class="criticas-e-materias">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'criticas-e-materias')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="materias">
        <div class="center masonry-grid-materias">
            @foreach($materias as $materia)

            @if($materia->tipo_id == 1)
            <a href="#" class="materia materia-imagens" data-materia-id="{{ $materia->id }}">
                <img src="{{ asset('assets/img/materias/'.$materia->imagem) }}" class="img-capa" alt="">
                <p class="data-titulo">
                    {{ utf8_encode(strftime("%B %Y", strtotime($materia->data))) }}
                    @if($materia->titulo)
                    • {{ $materia->titulo }}
                    @endif
                </p>
            </a>
            @endif

            @if($materia->tipo_id == 2)
            <a href="{{ $materia->link }}" target="_blank" class="materia materia-link">
                <img src="{{ asset('assets/img/materias/'.$materia->imagem) }}" class="img-capa" alt="">
                <p class="data-titulo">
                    {{ utf8_encode(strftime("%B %Y", strtotime($materia->data))) }}
                    @if($materia->titulo)
                    • {{ $materia->titulo }}
                    @endif
                </p>
            </a>
            @endif

            @endforeach
        </div>

        <a href="" class="link-ver-mais-materias">
            <img src="{{ asset('assets/img/layout/coracao-LuzesBrilhoFundo.png') }}" alt="" class="img-coracao">
            <p class="titulo">VER MAIS</p>
        </a>
    </section>

    @include('frontend.depoimentos-internas')

</main>

<div class="materias-show" style="display:none;">
    @foreach($materias as $materia)
    @foreach($materia->imagens as $imagem)
    <a href="{{ asset('assets/img/materias/imagens/'.$imagem->imagem) }}" class="fancybox" rel="materia-{{ $materia->id }}"></a>
    @endforeach
    @endforeach
</div>

@endsection