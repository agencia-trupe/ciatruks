@extends('frontend.layout.template')

@section('content')

<main class="contato">

    <section class="apresentacao center">
        @php $subtitulo = $subtitulos->where('rota', 'contato')->first(); @endphp
        <h1 class="titulo">{{ $subtitulo->pagina }}</h1>
        <h4 class="subtitulo">{{ $subtitulo->subtitulo }}</h4>
    </section>

    <section class="dados-contato center">
        <article class="dados">
            @php
            $telefone = str_replace(" ", "", $contato->telefone);
            $whatsapp = str_replace(" ", "", $contato->whatsapp);
            @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
            <a href="https://api.whatsapp.com/send?phone=+55{{ $whatsapp }}" class="link-whatsapp" target="_blank">{{ $contato->whatsapp }}</a>

            <a href="{{ $contato->instagram }}" target="_blank" class="link-instagram">@ciatruks</a>
            <a href="mailto:{{ $contato->email }}" class="link-email">{{ $contato->email }}</a>

            <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="" class="img-contato">
        </article>

        <form action="{{ route('contato.post') }}" method="POST" class="form-contato" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" placeholder="telefone" class="input-telefone" value="{{ old('telefone') }}">
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
            <button type="submit" class="btn-enviar">
                <img src="{{ asset('assets/img/layout/seta-reta-LuzesesBrilhoFundo-direita.png') }}" alt="" class="img-seta">
                <span>ENVIAR</span>
            </button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com sucesso!</p>
            </div>
            @endif
        </form>
    </section>

    @include('frontend.depoimentos-internas')

</main>

@endsection