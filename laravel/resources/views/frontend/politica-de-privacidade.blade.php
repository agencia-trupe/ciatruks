@extends('frontend.layout.template')

@section('content')

<main class="politica-de-privacidade">

    <section class="apresentacao center">
        <h1 class="titulo">POLÍTICA DE PRIVACIDADE</h1>
    </section>

    <section class="texto center">{!! $politica->texto !!}</section>

    @include('frontend.depoimentos-internas')

</main>

@endsection