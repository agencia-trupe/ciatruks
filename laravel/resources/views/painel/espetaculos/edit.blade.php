@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>ESPETÁCULOS |</small> Editar Espetáculo</h2>
</legend>

{!! Form::model($espetaculo, [
'route' => ['painel.espetaculos.update', $espetaculo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.espetaculos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection