@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Espetáculo: {{ $espetaculo->titulo }} |</small> Editar Vídeo</h2>
</legend>

{!! Form::model($video, [
'route' => ['painel.espetaculos.videos.update', $espetaculo->id, $video->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.espetaculos.videos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection