@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Espetáculo: {{ $espetaculo->titulo }} |</small> Adicionar Vídeo</h2>
</legend>

{!! Form::open(['route' => ['painel.espetaculos.videos.store', $espetaculo->id], 'files' => true]) !!}

@include('painel.espetaculos.videos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection