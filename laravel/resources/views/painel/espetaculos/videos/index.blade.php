@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.espetaculos.index') }}" title="Voltar para Espetáculos" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Espetáculos</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0"><small>Espetáculo: {{ $espetaculo->titulo }} |</small> Vídeos</h2>


    <a href="{{ route('painel.espetaculos.videos.create', $espetaculo->id) }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Vídeo
    </a>
</legend>


@if(!count($videos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="espetaculos_videos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Vídeo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($videos as $video)
            <tr id="{{ $video->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <iframe src="{{ 'https://www.youtube.com/embed/'.$video->video }}" frameborder="0" width="400px" height="220px"></iframe>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.espetaculos.videos.destroy', $espetaculo->id, $video->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.espetaculos.videos.edit', [$espetaculo->id, $video->id] ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection