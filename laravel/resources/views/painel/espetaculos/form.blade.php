@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($espetaculo->imagem)
    <img src="{{ url('assets/img/espetaculos/'.$espetaculo->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control input-text']) !!}
    <p style="color:red;margin:5px 0 0 0;font-style:italic;">Exibido na AGENDA. Exemplo: <strong>ESPETÁCULO INFANTIL</strong></p>
</div>

<div class="mb-3 col-12">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto (opcional)') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-master']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('ficha_tecnica', 'Ficha Técnica (opcional)') !!}
    {!! Form::textarea('ficha_tecnica', null, ['class' => 'form-control editor-master']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.espetaculos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>