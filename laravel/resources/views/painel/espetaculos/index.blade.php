@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">ESPETÁCULOS</h2>

    <a href="{{ route('painel.espetaculos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Espetáculo
    </a>
</legend>


@if(!count($espetaculos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="espetaculos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Imagem</th>
                <th scope="col">Título</th>
                <th scope="col">Tipo</th>
                <th scope="col">Vídeos</th>
                <th scope="col">Imagens</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($espetaculos as $espetaculo)
            <tr id="{{ $espetaculo->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/espetaculos/'.$espetaculo->imagem) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $espetaculo->titulo }}</td>
                <td>{{ $espetaculo->tipo }}</td>
                <td>
                    <a href="{{ route('painel.espetaculos.videos.index', $espetaculo->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-youtube me-2"></i>VÍDEOS
                    </a>
                </td>
                <td>
                    <a href="{{ route('painel.espetaculos.imagens.index', $espetaculo->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-images me-2"></i>IMAGENS
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.espetaculos.destroy', $espetaculo->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.espetaculos.edit', $espetaculo->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection