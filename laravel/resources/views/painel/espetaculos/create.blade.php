@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>ESPETÁCULOS |</small> Adicionar Espetáculo</h2>
</legend>

{!! Form::open(['route' => 'painel.espetaculos.store', 'files' => true]) !!}

@include('painel.espetaculos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection