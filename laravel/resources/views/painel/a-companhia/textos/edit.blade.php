@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>A COMPANHIA | Textos |</small> Editar Texto</h2>
</legend>

{!! Form::model($texto, [
'route' => ['painel.a-companhia.textos.update', $texto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.a-companhia.textos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection