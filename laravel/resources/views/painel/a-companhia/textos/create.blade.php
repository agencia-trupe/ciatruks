@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>A COMPANHIA | Textos |</small> Adicionar Texto</h2>
</legend>

{!! Form::open(['route' => 'painel.a-companhia.textos.store', 'files' => true]) !!}

@include('painel.a-companhia.textos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection