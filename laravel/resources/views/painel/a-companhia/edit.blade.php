@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">A COMPANHIA</h2>
</legend>

{!! Form::model($companhia, [
'route' => ['painel.a-companhia.update', $companhia->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.a-companhia.form', ['submitText' => 'Alterar'])
{!! Form::close() !!}

@endsection