@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CRÍTICAS E MATÉRIAS |</small> Editar Matéria</h2>
</legend>

{!! Form::model($materia, [
'route' => ['painel.materias.update', $materia->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.materias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection