@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('tipo_id', 'Tipo') !!}
    {!! Form::select('tipo_id', $tipos , old('tipo_id'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($materia->imagem)
    <img src="{{ url('assets/img/materias/'.$materia->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('data', 'Data') !!}
    @if($submitText == 'Alterar')
    {!! Form::date('data', old('data', $materia->data->format('Y-m-d')), ['class' => 'form-control']) !!}
    @else
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="mb-3 col-12">
    {!! Form::label('titulo', 'Título (opcional)') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>


<div class="mb-3 col-12 col-md-12">
    {!! Form::label('link', 'Link (quando selecionar o tipo LINK)') !!}
    {!! Form::text('link', null, ['class' => 'form-control input-text tipo-link']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.materias.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>