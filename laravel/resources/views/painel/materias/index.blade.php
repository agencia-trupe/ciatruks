@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">CRÍTICAS E MATÉRIAS</h2>

    <a href="{{ route('painel.materias.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Matéria
    </a>
</legend>


@if(!count($materias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="materias">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Imagem</th>
                <th scope="col">Data | Título</th>
                <th scope="col">Tipo | Conteúdo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($materias as $materia)
            <tr id="{{ $materia->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/materias/'.$materia->imagem) }}" style="width: auto; max-width:80px;" alt="">
                </td>
                <td>
                    {{ utf8_encode(ucwords(strftime("%b %Y", strtotime($materia->data)))) }}
                    @if($materia->titulo)
                    • {{ $materia->titulo }}
                    @endif
                </td>
                <td>
                    @if($materia->tipo == "Galeria")
                    <a href="{{ route('painel.materias.imagens.index', $materia->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-images me-2"></i>IMAGENS
                    </a>
                    @endif
                    @if($materia->tipo == "Link")
                    <a href="{{ $materia->link }}" target="_blank">
                        {{ $materia->link }}
                    </a>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.materias.destroy', $materia->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.materias.edit', $materia->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection