@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CRÍTICAS E MATÉRIAS |</small> Adicionar Matéria</h2>
</legend>

{!! Form::open(['route' => 'painel.materias.store', 'files' => true]) !!}

@include('painel.materias.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection