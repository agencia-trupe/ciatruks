@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOTÍCIAS |</small> Editar Notícia</h2>
</legend>

{!! Form::model($noticia, [
'route' => ['painel.noticias.update', $noticia->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.noticias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection