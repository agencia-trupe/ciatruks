@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Adicionar Usuário</h2>
</legend>

{!! Form::open(['route' => 'painel.usuarios.store']) !!}

@include('painel.usuarios.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection