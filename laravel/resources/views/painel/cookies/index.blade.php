@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">RELATÓRIO DE COOKIES</h2>
</legend>

@if(!count($cookies))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" data-table="aceite_de_cookies">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">IP</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($cookies as $cookie)
            <tr>
                <td data-order="{{ $cookie->created_at_order }}">{{ $cookie->created_at }}</td>
                <td>{{ $cookie->ip }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection