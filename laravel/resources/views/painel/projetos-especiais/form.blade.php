@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto (opcional)') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('video', 'Vídeo (opcional)') !!}
    {!! Form::text('video', null, ['class' => 'form-control input-text tipo-video']) !!}
    <p style="color:red;margin:5px 0 0 0;font-style:italic;">Incluir apenas o código do vídeo, a parte após "v=". Exemplo: https://www.youtube.com/watch?v=<strong>kUtybP_IDKg</strong></p>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.projetos-especiais.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>