@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PROJETOS ESPECIAIS |</small> Editar Projeto</h2>
</legend>

{!! Form::model($projeto, [
'route' => ['painel.projetos-especiais.update', $projeto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.projetos-especiais.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection