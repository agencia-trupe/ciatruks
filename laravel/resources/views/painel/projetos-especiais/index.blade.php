@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PROJETOS ESPECIAIS</h2>

    <a href="{{ route('painel.projetos-especiais.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Projeto
    </a>
</legend>


@if(!count($projetos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_especiais">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Texto (opcional)</th>
                <th scope="col">Vídeo (opcional)</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($projetos as $projeto)
            <tr id="{{ $projeto->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    @if($projeto->texto)
                    {!! mb_strimwidth($projeto->texto, 0, 300, "...") !!}
                    @else
                    Sem Texto
                    @endif
                </td>
                <td>
                    @if($projeto->video)
                    <iframe src="{{ 'https://www.youtube.com/embed/'.$projeto->video }}" frameborder="0" width="150px" height="80px"></iframe>
                    @else
                    Sem Vídeo
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.projetos-especiais.destroy', $projeto->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.projetos-especiais.edit', $projeto->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection