@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PROJETOS ESPECIAIS |</small> Adicionar Projeto</h2>
</legend>

{!! Form::open(['route' => 'painel.projetos-especiais.store', 'files' => true]) !!}

@include('painel.projetos-especiais.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection