@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>DEPOIMENTOS |</small> Editar Depoimento</h2>
</legend>

{!! Form::model($depoimento, [
'route' => ['painel.depoimentos.update', $depoimento->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.depoimentos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection