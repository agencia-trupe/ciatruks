@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">DEPOIMENTOS</h2>
</legend>

<div class="mb-3 col-12">
    <label>Data</label>
    <div class="well">{{ $depoimento->created_at }}</div>
</div>

<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $depoimento->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $depoimento->email }}">
            <i class="bi bi-clipboard"></i>
        </button>
        {{ $depoimento->email }}
    </div>
</div>

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $depoimento->mensagem !!}</div>
</div>

<div class="mb-3 col-12">
    <label>Aprovado</label>
    @if($depoimento->aprovado == 1)
    <div class="well"><i class="bi bi-check me-1"></i> Aprovado</div>
    @else
    <div class="well"><i class="bi bi-x me-1"></i> Não aprovado</div>
    @endif
</div>

<div class="mb-3 col-12">
    <label>Espetáculo</label>
    @if($espetaculo)
    <div class="well">{{ $espetaculo->titulo }}</div>
    @else
    <div class="well">Indefinido</div>
    @endif
</div>

<div class="d-flex align-items-center mt-4">
    <a href="{{ route('painel.depoimentos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop