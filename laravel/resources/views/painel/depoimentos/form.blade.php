@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control input-text', 'disabled' => 'disabled']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text', 'disabled' => 'disabled']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('mensagem', 'Mensagem') !!}
    {!! Form::textarea('mensagem', null, ['class' => 'form-control input-text', 'readonly']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('espetaculo_id', 'Espetáculo') !!}
    {!! Form::select('espetaculo_id', $espetaculos , old('espetaculo_id'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="mb-3 col-12">
    <div class="checkbox m-0 @if($depoimento->aprovado == 1) checked @endif">
        <label style="font-weight:bold">
            {!! Form::hidden('aprovado', 0) !!}
            {!! Form::checkbox('aprovado') !!}
            Aprovado
        </label>
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.depoimentos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>