@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="mb-4">
    <h2 class="m-0">DEPOIMENTOS</h2>
</legend>

@if(!count($depoimentos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="depoimentos">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Espetáculo</th>
                <th scope="col">Aprovado</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($depoimentos as $depoimento)
            <tr class="@if(!$depoimento->lido)alert alert-warning @endif" id="{{ $depoimento->id }}">
                <td data-order="{{ $depoimento->created_at_order }}">{{ $depoimento->created_at }}</td>
                <td>{{ $depoimento->nome }}</td>
                <td class="d-flex flex-row align-items-center">
                    <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $depoimento->email }}">
                        <i class="bi bi-clipboard"></i>
                    </button>
                    {{ $depoimento->email }}
                </td>
                <td>
                    @if($depoimento->espetaculo_id)
                    {{ $espetaculos->where('id', $depoimento->espetaculo_id)->first()->titulo }}
                    @else
                    Indefinido
                    @endif
                </td>
                <td>
                    @if($depoimento->aprovado == 1)
                    <i class="bi bi-check me-1"></i> Aprovado
                    @else
                    <i class="bi bi-x me-1"></i> Não aprovado
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.depoimentos.destroy', $depoimento->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.depoimentos.show', $depoimento->id ) }}" class="btn btn-success btn-sm">
                            <i class="bi bi-chat-text-fill me-2"></i>Ler Depoimento
                        </a>

                        <a href="{{ route('painel.depoimentos.edit', $depoimento->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>

                        <a href="{{ route('painel.depoimentos.toggle', $depoimento->id) }}" class="btn btn-sm {{ ($depoimento->lido ? 'btn-warning' : 'btn-success') }}">
                            @if($depoimento->lido)
                            <i class="bi bi-arrow-repeat"></i>
                            @else
                            <i class="bi bi-check2-circle"></i>
                            @endif
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif

@endsection