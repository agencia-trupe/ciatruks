@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Editar Vídeo</h2>
</legend>

{!! Form::model($video, [
'route' => ['painel.videos.update', $video->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.videos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection