@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Adicionar Vídeo</h2>
</legend>

{!! Form::open(['route' => ['painel.videos.store'], 'files' => true]) !!}

@include('painel.videos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection