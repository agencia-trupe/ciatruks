@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CENTRO DE ESTUDOS | Textos |</small> Adicionar Texto</h2>
</legend>

{!! Form::open(['route' => 'painel.centro-de-estudos.textos.store', 'files' => true]) !!}

@include('painel.centro-de-estudos.textos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection