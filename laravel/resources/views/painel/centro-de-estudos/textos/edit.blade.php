@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CENTRO DE ESTUDOS | Textos |</small> Editar Texto</h2>
</legend>

{!! Form::model($texto, [
'route' => ['painel.centro-de-estudos.textos.update', $texto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.centro-de-estudos.textos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection