@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>AGENDA |</small> Editar Evento</h2>
</legend>

{!! Form::model($agenda, [
'route' => ['painel.agenda.update', $agenda->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.agenda.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection