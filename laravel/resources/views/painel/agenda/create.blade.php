@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>AGENDA |</small> Adicionar Evento</h2>
</legend>

{!! Form::open(['route' => 'painel.agenda.store', 'files' => true]) !!}

@include('painel.agenda.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection