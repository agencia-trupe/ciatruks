@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('espetaculo_id', 'Espetáculo') !!}
    {!! Form::select('espetaculo_id', $espetaculos , old('espetaculo_id'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('data', 'Data') !!}
        @if($submitText == 'Alterar')
        {!! Form::date('data', old('data', $agenda->data->format('Y-m-d')), ['class' => 'form-control']) !!}
        @else
        {!! Form::date('data', null, ['class' => 'form-control']) !!}
        @endif
    </div>

    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('horario', 'Horário') !!}
        {!! Form::text('horario', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('local', 'Local') !!}
    {!! Form::text('local', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('endereco', 'Endereço (com número e complemento)') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('bairro', 'Bairro') !!}
        {!! Form::text('bairro', null, ['class' => 'form-control input-text']) !!}
    </div>

    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('cidade_uf', 'Cidade, UF') !!}
        {!! Form::text('cidade_uf', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('telefone', 'Telefone (opcional)') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control input-text input-telefone']) !!}
</div>

<div class="my-4 col-12">
    <div class="checkbox m-0 @if($submitText == 'Alterar') @if($agenda->gratuito == 1) checked @endif @endif">
        <label style="font-weight:bold">
            {!! Form::hidden('gratuito', 0) !!}
            {!! Form::checkbox('gratuito') !!}
            Gratuito
        </label>
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-12">
        {!! Form::label('link', 'Link (url)') !!}
        {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
    </div>

    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('titulo_link', 'Título do Link') !!}
        {!! Form::text('titulo_link', null, ['class' => 'form-control input-text']) !!}
    </div>

    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('titulo_link_home', 'Título menor do Link (HOME)') !!}
        {!! Form::text('titulo_link_home', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('observacao', 'Observação (opcional)') !!}
    {!! Form::text('observacao', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.agenda.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>