@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">AGENDA</h2>

    <a href="{{ route('painel.agenda.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Evento
    </a>
</legend>


@if(!count($agenda))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="agenda">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Horário</th>
                <th scope="col">Local</th>
                <th scope="col">Espetáculo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($agenda as $evento)
            <tr id="{{ $evento->id }}">
                <td>{{ strftime("%d/%m/%Y", strtotime($evento->data)) }}</td>
                <td>{{ $evento->horario }}</td>
                <td>{{ $evento->local }}</td>
                <td>
                    <p class="my-1"><img src="{{ asset('assets/img/espetaculos/depoimentos/'.$evento->espetaculo_imagem) }}" style="width: auto; max-width:60px; margin-right: 5px;" alt=""> {{ $evento->espetaculo_titulo }} | {{ $evento->espetaculo_tipo }}</p>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.agenda.destroy', $evento->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.agenda.edit', $evento->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

<h4 class="mt-5">Eventos que já aconteceram</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="agenda">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Horário</th>
                <th scope="col">Local</th>
                <th scope="col">Espetáculo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($anteriores as $anterior)
            <tr id="{{ $anterior->id }}">
                <td>{{ strftime("%d/%m/%Y", strtotime($anterior->data)) }}</td>
                <td>{{ $anterior->horario }}</td>
                <td>{{ $anterior->local }}</td>
                <td>
                    <p class="my-1"><img src="{{ asset('assets/img/espetaculos/depoimentos/'.$anterior->espetaculo_imagem) }}" style="width: auto; max-width:60px; margin-right: 5px;" alt=""> {{ $anterior->espetaculo_titulo }} | {{ $anterior->espetaculo_tipo }}</p>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.agenda.destroy', $anterior->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.agenda.edit', $anterior->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection