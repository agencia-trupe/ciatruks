@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CAPTAÇÃO DE PATROCÍNIOS | Textos |</small> Adicionar Texto</h2>
</legend>

{!! Form::open(['route' => 'painel.captacao-de-patrocinios.textos.store', 'files' => true]) !!}

@include('painel.captacao-de-patrocinios.textos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection