@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CAPTAÇÃO DE PATROCÍNIOS | Textos |</small> Editar Texto</h2>
</legend>

{!! Form::model($texto, [
'route' => ['painel.captacao-de-patrocinios.textos.update', $texto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.captacao-de-patrocinios.textos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection