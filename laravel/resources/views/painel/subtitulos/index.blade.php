@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">SUBTÍTULOS</h2>
</legend>


@if(!count($subtitulos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="subtitulos">
        <thead>
            <tr>
                <th scope="col">Página</th>
                <th scope="col">Subtítulo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($subtitulos as $subtitulo)
            <tr id="{{ $subtitulo->id }}">
                <td>{{ $subtitulo->pagina }}</td>
                <td>{{ $subtitulo->subtitulo }}</td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.subtitulos.edit', $subtitulo->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection