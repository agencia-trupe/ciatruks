@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>SUBTÍTULOS |</small> Editar Subtítulo da Página: {{ $subtitulo->pagina }}</h2>
</legend>

{!! Form::model($subtitulo, [
'route' => ['painel.subtitulos.update', $subtitulo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.subtitulos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection