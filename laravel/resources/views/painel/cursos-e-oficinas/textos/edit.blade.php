@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CURSOS E OFICINAS | Textos |</small> Editar Texto</h2>
</legend>

{!! Form::model($texto, [
'route' => ['painel.cursos-e-oficinas.textos.update', $texto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.cursos-e-oficinas.textos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection