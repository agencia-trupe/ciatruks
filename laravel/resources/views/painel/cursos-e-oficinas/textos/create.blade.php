@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CURSOS E OFICINAS | Textos |</small> Adicionar Texto</h2>
</legend>

{!! Form::open(['route' => 'painel.cursos-e-oficinas.textos.store', 'files' => true]) !!}

@include('painel.cursos-e-oficinas.textos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection