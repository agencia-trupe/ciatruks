@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CURSOS E OFICINAS | Planejamentos |</small> Editar Planejamento</h2>
</legend>

{!! Form::model($planejamento, [
'route' => ['painel.cursos-e-oficinas.planejamentos.update', $planejamento->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.cursos-e-oficinas.planejamentos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection