@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_modulos', 'Texto Módulos') !!}
    {!! Form::textarea('texto_modulos', null, ['class' => 'form-control editor-master']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.cursos-e-oficinas.planejamentos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>