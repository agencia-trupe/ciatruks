@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PREMIAÇÕES |</small> Editar Premiação</h2>
</legend>

{!! Form::model($premiacao, [
'route' => ['painel.premiacoes.update', $premiacao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.premiacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection