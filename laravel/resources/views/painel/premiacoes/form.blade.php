@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('data', 'Data') !!}
    @if($submitText == 'Alterar')
    {!! Form::date('data', old('data', $premiacao->data->format('Y-m-d')), ['class' => 'form-control']) !!}
    @else
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="mb-3 col-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($premiacao->imagem)
    <img src="{{ url('assets/img/premiacoes/'.$premiacao->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-clean']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.premiacoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>