@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PREMIAÇÕES |</small> Adicionar Premiação</h2>
</legend>

{!! Form::open(['route' => 'painel.premiacoes.store', 'files' => true]) !!}

@include('painel.premiacoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection