@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_agenda', 'Frase Agenda') !!}
    {!! Form::text('frase_agenda', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_fotos', 'Imagem (Galeria de Fotos)') !!}
    @if($submitText == 'Alterar')
    @if($home->imagem_fotos)
    <img src="{{ url('assets/img/home/'.$home->imagem_fotos) }}" style="display:block; margin-bottom: 10px; max-width: 70%;">
    @endif
    @endif
    {!! Form::file('imagem_fotos', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_videos', 'Imagem (Galeria de Vídeos)') !!}
    @if($submitText == 'Alterar')
    @if($home->imagem_videos)
    <img src="{{ url('assets/img/home/'.$home->imagem_videos) }}" style="display:block; margin-bottom: 10px; max-width: 70%;">
    @endif
    @endif
    {!! Form::file('imagem_videos', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link1_titulo', 'Item 1 - Título') !!}
        {!! Form::text('link1_titulo', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link1_link', 'Item 1 - Link') !!}
        {!! Form::text('link1_link', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link2_titulo', 'Item 2 - Título') !!}
        {!! Form::text('link2_titulo', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link2_link', 'Item 2 - Link') !!}
        {!! Form::text('link2_link', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link3_titulo', 'Item 3 - Título') !!}
        {!! Form::text('link3_titulo', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('link3_link', 'Item 3 - Link') !!}
        {!! Form::text('link3_link', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>