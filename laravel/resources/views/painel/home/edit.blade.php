@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">HOME</h2>
</legend>

{!! Form::model($home, [
'route' => ['painel.home.update', $home->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection