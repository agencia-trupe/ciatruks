@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('espetaculo_id', 'Espetáculo') !!}
    {!! Form::select('espetaculo_id', $espetaculos , old('espetaculo_id'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.home.espetaculos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>