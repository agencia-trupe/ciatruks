@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>HOME | ESPETÁCULOS |</small> Editar Espetáculo</h2>
</legend>

{!! Form::model($espetaculo, [
'route' => ['painel.home.espetaculos.update', $espetaculo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.espetaculos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection