<ul class="nav navbar-nav">

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.home*', 'painel.banners*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Home <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.home.index') }}" class="dropdown-item @if(Tools::routeIs('painel.home*')) active @endif">Home</a>
            </li>
            <li>
                <a href="{{ route('painel.banners.index') }}" class="dropdown-item @if(Tools::routeIs('painel.banners*')) active @endif">Banners</a>
            </li>
            <li>
                <a href="{{ route('painel.home.espetaculos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.home.espetaculos*')) active @endif">Espetáculos</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.subtitulos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.subtitulos*')) active @endif">Subtítulos</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.a-companhia*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            A Companhia <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.a-companhia.index') }}" class="dropdown-item @if(Tools::routeIs('painel.a-companhia.index')) active @endif">Vídeo de Abertura</a>
            </li>
            <li>
                <a href="{{ route('painel.a-companhia.textos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.a-companhia.textos*')) active @endif">Partes [Textos|Imagens]</a>
            </li>
            <li>
                <a href="{{ route('painel.a-companhia.galeria.index') }}" class="dropdown-item @if(Tools::routeIs('painel.a-companhia.galeria*')) active @endif">Galeria de Imagens</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.agenda.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.agenda*')) active @endif">Agenda</a>
    </li>

    <li>
        <a href="{{ route('painel.espetaculos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.espetaculos*')) active @endif">Espetáculos</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.cursos-e-oficinas*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Cursos e Oficinas <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.cursos-e-oficinas.textos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.cursos-e-oficinas.textos*')) active @endif">Partes [Textos|Imagens]</a>
            </li>
            <li>
                <a href="{{ route('painel.cursos-e-oficinas.planejamentos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.cursos-e-oficinas.planejamentos*')) active @endif">Planejamentos das Oficinas</a>
            </li>
            <li>
                <a href="{{ route('painel.cursos-e-oficinas.galeria.index') }}" class="dropdown-item @if(Tools::routeIs('painel.cursos-e-oficinas.galeria*')) active @endif">Galeria de Imagens</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.projetos-especiais.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.projetos-especiais*')) active @endif">Projetos Especiais</a>
    </li>

    <li>
        <a href="{{ route('painel.centro-de-estudos.textos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.centro-de-estudos.textos*')) active @endif">Centro de Estudos</a>
    </li>

    <li>
        <a href="{{ route('painel.captacao-de-patrocinios.textos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.captacao-de-patrocinios.textos*')) active @endif">Captação de Patrocínios</a>
    </li>

    <li>
        <a href="{{ route('painel.contratacoes.textos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.contratacoes.textos*')) active @endif">Contratações</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.galeria-de-fotos*', 'painel.videos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Galerias <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.galeria-de-fotos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.galeria-de-fotos*')) active @endif">Fotos</a>
            </li>
            <li>
                <a href="{{ route('painel.videos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.videos*')) active @endif">Vídeos</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.noticias.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.noticias*')) active @endif">Notícias</a>
    </li>

    <li>
        <a href="{{ route('painel.premiacoes.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.premiacoes*')) active @endif">Premiações</a>
    </li>

    <li>
        <a href="{{ route('painel.materias.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.materias*')) active @endif">Críticas e Matérias</a>
    </li>

    <li>
        <a href="{{ route('painel.depoimentos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.depoimentos*')) active @endif d-flex align-items-center">
            Depoimentos
            @if($depoimentosNaoLidos >= 1)
            <span class="label label-success ms-1">{{ $depoimentosNaoLidos }}</span>
            @endif
        </a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.contatos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('painel.contatos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('painel.contatos-recebidos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.contatos-recebidos*')) active @endif d-flex align-items-center">
                    Contatos recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>


</ul>