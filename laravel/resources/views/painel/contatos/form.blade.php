@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('whatsapp', 'Whatsapp') !!}
        {!! Form::text('whatsapp', null, ['class' => 'form-control input-text input-celular']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($contato->imagem)
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>