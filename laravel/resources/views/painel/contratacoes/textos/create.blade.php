@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTRATAÇÕES | Textos |</small> Adicionar Texto</h2>
</legend>

{!! Form::open(['route' => 'painel.contratacoes.textos.store', 'files' => true]) !!}

@include('painel.contratacoes.textos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection