@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.contratacoes.textos.index') }}" title="Voltar para Textos" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Textos</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0"><small>CONTRATAÇÕES | Textos |</small> Imagens</h2>

    {!! Form::open(['route' => ['painel.contratacoes.textos.imagens.store', $texto->id], 'files' => true, 'class' => 'pull-right']) !!}
    <div class="btn-group btn-group-sm">
        <span class="btn btn-success" style="position:relative;overflow:hidden">
            <i class="bi bi-plus-circle me-2 mb-1"></i>Adicionar Imagens
            <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
        </span>

        <a href="{{ route('painel.contratacoes.textos.imagens.clear', $texto->id) }}" class="btn btn-danger btn-sm btn-delete btn-delete-link btn-delete-multiple">
            <i class="bi bi-trash-fill me-2"></i>Limpar
        </a>
    </div>
    {!! Form::close() !!}
</legend>

<div class="progress progress-striped active">
    <div class="progress-bar" style="width: 0"></div>
</div>

<div class="alert alert-block alert-danger errors" style="display:none"></div>

<div class="d-flex flex-row align-items-center justify-content-between mt-2">
    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <i class="bi bi-arrows-move me-2"></i>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>
</div>

<div id="imagens" data-table="contratacoes_textos_imagens" style="width: 100%; display: flex; flex-wrap: wrap;">
    @if(!count($imagens))
    <div class="alert alert-warning no-images" style="width: 100%;" role="alert">Nenhuma imagem cadastrada.</div>
    @else

    @foreach($imagens as $imagem)
    @include('painel.contratacoes.textos.imagens.imagem')
    @endforeach

    @endif
</div>

@endsection