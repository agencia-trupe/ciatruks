@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTRATAÇÕES | Textos |</small> Editar Texto</h2>
</legend>

{!! Form::model($texto, [
'route' => ['painel.contratacoes.textos.update', $texto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contratacoes.textos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection