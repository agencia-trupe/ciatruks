<!DOCTYPE html>
<html>

<head>
    <title>[DEPOIMENTOS] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Nome:</span>
        <span style='font-size:14px;'>{{ $contato['nome'] }}</span>
    </p>
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>E-mail:</span>
        <span style='font-size:14px;'>{{ $contato['email'] }}</span>
    </p>
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Depoimento:</span>
        <span style='font-size:14px;'>{{ $contato['mensagem'] }}</span>
    </p>
</body>

</html>