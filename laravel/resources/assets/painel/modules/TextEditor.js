export default function TextEditor() {
    var editorClean = document.querySelectorAll(".editor-clean");
    var editorBasic = document.querySelectorAll(".editor-basic");
    var editorFull = document.querySelectorAll(".editor-full");
    var editorMaster = document.querySelectorAll(".editor-master");

    console.log(editorFull);

    if (editorClean != null) {
        editorClean.forEach((editor) => {
            ClassicEditor.create(editor, {
                toolbar: ["removeFormat"],
            })
                .then((editor) => {
                    console.log("Editor was initialized", editor);
                })
                .catch((error) => {
                    console.error(error.stack);
                });
        });
    }

    if (editorBasic != null) {
        editorBasic.forEach((editor) => {
            ClassicEditor.create(editor, {
                toolbar: [
                    "bold",
                    "italic",
                    "link",
                    "bulletedList",
                    "undo",
                    "redo",
                    "removeFormat",
                ],
            })
                .then((editor) => {
                    console.log("Editor was initialized", editor);
                })
                .catch((error) => {
                    console.error(error.stack);
                });
        });
    }

    if (editorFull != null) {
        editorFull.forEach((editor) => {
            ClassicEditor.create(editor, {
                toolbar: [
                    "bold",
                    "italic",
                    "link",
                    "numberedList",
                    "bulletedList",
                    "imageUpload",
                    "insertTable",
                    "undo",
                    "redo",
                    "removeFormat",
                ],
                simpleUpload: {
                    uploadUrl: window.location.origin + "/painel/image-upload",
                    withCredentials: false,
                },
            })
                .then((editor) => {
                    console.log("Editor was initialized", editor);
                })
                .catch((error) => {
                    console.error(error.stack);
                });
        });
    }

    if (editorMaster != null) {
        editorMaster.forEach((editor) => {
            ClassicEditor.create(editor, {
                toolbar: [
                    "heading",
                    "|",
                    "bold",
                    "italic",
                    "underline",
                    "|",
                    "link",
                    "numberedList",
                    "bulletedList",
                    "imageUpload",
                    "insertTable",
                    "|",
                    "undo",
                    "redo",
                    "removeFormat",
                ],
                heading: {
                    options: [
                        {
                            model: "paragraph",
                            title: "Parágrafo",
                            class: "ck-heading_paragraph",
                        },
                        {
                            model: "heading2",
                            view: "h2",
                            title: "Título",
                            class: "ck-heading_heading2",
                        },
                        {
                            model: "heading4",
                            view: "h4",
                            title: "Subtítulo",
                            class: "ck-heading_heading4",
                        },
                    ],
                },
                simpleUpload: {
                    uploadUrl: window.location.origin + "/painel/image-upload",
                    withCredentials: false,
                },
            })
                .then((editor) => {
                    console.log("Editor was initialized", editor);
                })
                .catch((error) => {
                    console.error(error.stack);
                });
        });
    }
}
