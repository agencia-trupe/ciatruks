import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";
import MobileToggle from "../js/MobileToggle";

Clipboard();
DataTables();
DatePicker();
DeleteButton();
ImagesUpload();
OrderImages();
OrderTable();
TextEditor();
MobileToggle();

$(document).ready(function () {
    $(".input-telefone").mask("00 0000 0000");
    $(".input-celular").mask("00 00000 0000");
});
