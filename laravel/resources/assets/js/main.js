import MobileToggle from "./MobileToggle";

MobileToggle();

$(window).on("load", function () {
    $(".loading").hide();
    $("main section").css({ visibility: "visible" });
});

$(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-ciatruks";

    // HOME - banners
    $(".banners")
        .on("cycle-initialized", function (e, o) {
            $(".banners").css("display", "block");
        })
        .cycle({
            slides: ".banner",
            fx: "fade",
            speed: 1500,
            timeout: 4000,
        });

    // FANCYBOX - todos
    $(".fancybox").fancybox({
        padding: 0,
        prevEffect: "fade",
        nextEffect: "fade",
        closeBtn: false,
        openEffect: "elastic",
        openSpeed: "150",
        closeEffect: "elastic",
        closeSpeed: "150",
        helpers: {
            title: {
                type: "outside",
                position: "top",
            },
            overlay: {
                css: {
                    background: "rgba(132, 134, 136, .88)",
                },
            },
        },
        fitToView: false,
        autoSize: false,
        beforeShow: function () {
            this.maxWidth = "90%";
            this.maxHeight = "90%";
        },
    });

    // GRID - a companhia, espetáculo, cursos e oficinas
    $(".masonry-grid").masonryGrid({
        columns: 4,
    });

    // GALERIA DE FOTOS - ver mais
    var itensGaleria = $("main.galeria-de-fotos .link-img-galeria");
    var spliceItensGaleria = 12; // 20-30 itens (no layout)
    if (itensGaleria.length <= spliceItensGaleria) {
        $(".link-ver-mais-fotos").hide();
    }
    var setDivGaleria = function () {
        var spliceItens = itensGaleria.splice(0, spliceItensGaleria);
        $(spliceItens).show();
        if (itensGaleria.length <= 0) {
            $(".link-ver-mais-fotos").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-fotos", function (e) {
        e.preventDefault();
        setDivGaleria();
    });
    $("main.galeria-de-fotos .link-img-galeria").hide();
    setDivGaleria();

    // GALERIA DE FOTOS - grid
    $(".masonry-grid-fotos").masonryGrid({
        columns: 3,
    });

    // ESPETÁCULOS
    if ($(window).width() > 800) {
        $("main.espetaculos .link-espetaculo:nth-child(3n)").css(
            "margin-right",
            "0"
        );
    } else {
        $("main.espetaculos .link-espetaculo:nth-child(2n)").css(
            "margin-right",
            "0"
        );
    }

    // VIDEOS - ver mais
    var itensVideos = $("main.videos iframe.video");
    var spliceItensVideos = 4; // 5 itens (no layout)
    if (itensVideos.length <= spliceItensVideos) {
        $(".link-ver-mais-videos").hide();
    }
    var setDivVideos = function () {
        var spliceItens = itensVideos.splice(0, spliceItensVideos);
        $("main.videos .lista-videos").append(spliceItens);
        $(spliceItens).show();
        if (itensVideos.length <= 0) {
            $(".link-ver-mais-videos").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-videos", function (e) {
        e.preventDefault();
        setDivVideos();
    });
    $("main.videos iframe.video").hide();
    setDivVideos();

    // NOTICIAS - ver mais
    var itensNoticias = $("main.noticias .link-noticia");
    var spliceItensNoticias = 5; // 7 itens (no layout)
    if (itensNoticias.length <= spliceItensNoticias) {
        $(".link-ver-mais-noticias").hide();
    }
    var setDivNoticias = function () {
        var spliceItens = itensNoticias.splice(0, spliceItensNoticias);
        $("main.noticias .lista-noticias").append(spliceItens);
        $(spliceItens).show();
        if (itensNoticias.length <= 0) {
            $(".link-ver-mais-noticias").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-noticias", function (e) {
        e.preventDefault();
        setDivNoticias();
    });
    $("main.noticias .link-noticia").hide();
    setDivNoticias();

    // CRITÍCAS E MATÉRIAS - ver mais
    var itensMaterias = $("main.criticas-e-materias .materia");
    var spliceItensMaterias = 6; // 9 itens (no layout)
    if (itensMaterias.length <= spliceItensMaterias) {
        $(".link-ver-mais-fotos").hide();
    }
    var setDivCriticasEMaterias = function () {
        var spliceItens = itensMaterias.splice(0, spliceItensMaterias);
        $(spliceItens).show();
        if (itensMaterias.length <= 0) {
            $(".link-ver-mais-materias").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-materias", function (e) {
        e.preventDefault();
        setDivCriticasEMaterias();
    });
    $("main.criticas-e-materias .materia").hide();
    setDivCriticasEMaterias();

    // CRITÍCAS E MATÉRIAS - grid
    $(".masonry-grid-materias").masonryGrid({
        columns: 3,
    });

    // CRÍTICAS E MATÉRIAS - fancybox
    $("main.criticas-e-materias .materia-imagens").click(function handler(e) {
        e.preventDefault();
        const id = $(this).data("materia-id");
        if (id) {
            $(`a[rel=materia-${id}]`)[0].click();
        }
    });

    // DEPOIMENTOS - ver mais
    var itensDepoimentos = $("main.depoimentos .lista-depoimentos .depoimento");
    var spliceItensDepoimentos = 5; // 15 itens (no layout)
    if (itensDepoimentos.length <= spliceItensDepoimentos) {
        $(".link-ver-mais-depoimentos").hide();
    }
    var setDivDepoimentos = function () {
        var spliceItens = itensDepoimentos.splice(0, spliceItensDepoimentos);
        $("main.depoimentos .lista-depoimentos").append(spliceItens);
        $(spliceItens).show();
        if (itensDepoimentos.length <= 0) {
            $(".link-ver-mais-depoimentos").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-depoimentos", function (e) {
        e.preventDefault();
        setDivDepoimentos();
    });
    $("main.depoimentos .lista-depoimentos .depoimento").hide();
    setDivDepoimentos();

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + urlPrevia + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
