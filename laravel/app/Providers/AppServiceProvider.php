<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Depoimento;
use App\Models\Espetaculo;
use App\Models\Subtitulo;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.*', function ($view) {
            $view->with('contato', Contato::first());
            $view->with('espetaculos', Espetaculo::ordenados()->get());
            $view->with('subtitulos', Subtitulo::all());
            $depoimentos = Depoimento::aprovados()
                ->join('espetaculos', 'espetaculos.id', '=', 'depoimentos.espetaculo_id')
                ->select('depoimentos.*', 'espetaculos.slug', 'espetaculos.imagem', 'espetaculos.titulo')
                ->inRandomOrder()->take(5)->get();
            $view->with('depoimentosInternas', $depoimentos);

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosNaoLidos', ContatoRecebido::naoLidos()->count());
            $view->with('depoimentosNaoAprovados', Depoimento::naoAprovados()->count());
            $view->with('depoimentosNaoLidos', Depoimento::naoLidos()->count());
        });
    }
}
