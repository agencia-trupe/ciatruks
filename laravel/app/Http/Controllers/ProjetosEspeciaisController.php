<?php

namespace App\Http\Controllers;

use App\Models\ProjetoEspecial;
use Illuminate\Http\Request;

class ProjetosEspeciaisController extends Controller
{
    public function index()
    {
        $projetos = ProjetoEspecial::ordenados()->get();

        return view('frontend.projetos-especiais', compact('projetos'));
    }
}
