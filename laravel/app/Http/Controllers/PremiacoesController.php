<?php

namespace App\Http\Controllers;

use App\Models\Premiacao;
use Illuminate\Http\Request;

class PremiacoesController extends Controller
{
    public function index()
    {
        $premiacoes = Premiacao::ordenados()->get();

        return view('frontend.premiacoes', compact('premiacoes'));
    }
}
