<?php

namespace App\Http\Controllers;

use App\Models\CentroDeEstudoTexto;
use App\Models\CentroDeEstudoTextoImagem;
use Illuminate\Http\Request;

class CentroDeEstudosController extends Controller
{
    public function index()
    {
        $textos = CentroDeEstudoTexto::ordenados()->get();
        $imagens = CentroDeEstudoTextoImagem::ordenados()->get();

        return view('frontend.centro-de-estudos', compact('textos', 'imagens'));
    }
}
