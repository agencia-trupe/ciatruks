<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CursosPlanejamentosRequest;
use App\Models\CursoPlanejamento;
use Illuminate\Http\Request;

class CursosPlanejamentosController extends Controller
{
    public function index()
    {
        $planejamentos = CursoPlanejamento::ordenados()->get();

        return view('painel.cursos-e-oficinas.planejamentos.index', compact('planejamentos'));
    }

    public function create()
    {
        return view('painel.cursos-e-oficinas.planejamentos.create');
    }

    public function store(CursosPlanejamentosRequest $request)
    {
        try {
            $input = $request->all();

            CursoPlanejamento::create($input);

            return redirect()->route('painel.cursos-e-oficinas.planejamentos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CursoPlanejamento $planejamento)
    {
        return view('painel.cursos-e-oficinas.planejamentos.edit', compact('planejamento'));
    }

    public function update(CursosPlanejamentosRequest $request, CursoPlanejamento $planejamento)
    {
        try {
            $input = $request->all();

            $planejamento->update($input);

            return redirect()->route('painel.cursos-e-oficinas.planejamentos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CursoPlanejamento $planejamento)
    {
        try {
            $planejamento->delete();

            return redirect()->route('painel.cursos-e-oficinas.planejamentos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
