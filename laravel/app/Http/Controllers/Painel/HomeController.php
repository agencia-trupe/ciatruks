<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeRequest;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();

        return view('painel.home.edit', compact('home'));
    }

    public function update(HomeRequest $request, Home $home)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_fotos'])) $input['imagem_fotos'] = Home::upload_imagem_fotos();
            if (isset($input['imagem_videos'])) $input['imagem_videos'] = Home::upload_imagem_videos();

            $home->update($input);

            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
