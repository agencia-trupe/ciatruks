<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContratacoesTextosImagensRequest;
use App\Models\ContratacaoTexto;
use App\Models\ContratacaoTextoImagem;
use Illuminate\Http\Request;

class ContratacoesTextosImagensController extends Controller
{
    public function index(ContratacaoTexto $texto)
    {
        $imagens = ContratacaoTextoImagem::texto($texto->id)->ordenados()->get();

        return view('painel.contratacoes.textos.imagens.index', compact('texto', 'imagens'));
    }

    public function show(ContratacaoTexto $texto, ContratacaoTextoImagem $imagem)
    {
        return $imagem;
    }

    public function store(ContratacoesTextosImagensRequest $request, ContratacaoTexto $texto)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ContratacaoTextoImagem::upload_imagem();
            $input['texto_id'] = $texto->id;

            $imagem = ContratacaoTextoImagem::create($input);

            $view = view('painel.contratacoes.textos.imagens.imagem', compact('texto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ContratacaoTexto $texto, ContratacaoTextoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.contratacoes.textos.imagens.index', $texto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(ContratacaoTexto $texto)
    {
        try {
            $texto->imagens()->delete();

            return redirect()->route('painel.contratacoes.textos.imagens.index', $texto->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
