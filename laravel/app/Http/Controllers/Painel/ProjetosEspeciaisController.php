<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosEspeciaisRequest;
use App\Models\ProjetoEspecial;
use Illuminate\Http\Request;

class ProjetosEspeciaisController extends Controller
{
    public function index()
    {
        $projetos = ProjetoEspecial::ordenados()->get();

        return view('painel.projetos-especiais.index', compact('projetos'));
    }

    public function create()
    {
        return view('painel.projetos-especiais.create');
    }

    public function store(ProjetosEspeciaisRequest $request)
    {
        try {
            $input = $request->all();

            ProjetoEspecial::create($input);

            return redirect()->route('painel.projetos-especiais.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ProjetoEspecial $projetos_especiai)
    {
        $projeto = $projetos_especiai;

        return view('painel.projetos-especiais.edit', compact('projeto'));
    }

    public function update(ProjetosEspeciaisRequest $request, ProjetoEspecial $projetos_especiai)
    {
        try {
            $input = $request->all();

            $projetos_especiai->update($input);

            return redirect()->route('painel.projetos-especiais.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ProjetoEspecial $projetos_especiai)
    {
        try {
            $projetos_especiai->delete();

            return redirect()->route('painel.projetos-especiais.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
