<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MateriasRequest;
use App\Models\Materia;
use App\Models\Tipo;
use Illuminate\Http\Request;

class MateriasController extends Controller
{
    public function index()
    {
        $materias = Materia::join('tipos', 'tipos.id', '=', 'materias.tipo_id')
            ->select('tipos.titulo as tipo', 'materias.*')
            ->orderBy('data', 'desc')->get();

        return view('painel.materias.index', compact('materias'));
    }

    public function create()
    {
        $tipos = Tipo::orderBy('id', 'asc')->pluck('titulo', 'id');

        return view('painel.materias.create', compact('tipos'));
    }

    public function store(MateriasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Materia::upload_imagem();

            Materia::create($input);

            return redirect()->route('painel.materias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Materia $materia)
    {
        $tipos = Tipo::orderBy('id', 'asc')->pluck('titulo', 'id');

        return view('painel.materias.edit', compact('materia', 'tipos'));
    }

    public function update(MateriasRequest $request, Materia $materia)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Materia::upload_imagem();

            $materia->update($input);

            return redirect()->route('painel.materias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Materia $materia)
    {
        try {
            $materia->delete();

            return redirect()->route('painel.materias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
