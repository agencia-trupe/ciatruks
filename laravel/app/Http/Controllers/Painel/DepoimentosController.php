<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Depoimento;
use App\Models\Espetaculo;
use Illuminate\Http\Request;

class DepoimentosController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::orderBy('created_at', 'DESC')->get();

        $espetaculos = Espetaculo::ordenados()->get();

        return view('painel.depoimentos.index', compact('depoimentos', 'espetaculos'));
    }

    public function show(Depoimento $depoimento)
    {
        $depoimento->update(['lido' => 1]);
        $espetaculo = Espetaculo::where('id', $depoimento->espetaculo_id)->first();

        return view('painel.depoimentos.show', compact('depoimento', 'espetaculo'));
    }

    public function edit(Depoimento $depoimento)
    {
        $espetaculos = Espetaculo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.depoimentos.edit', compact('depoimento', 'espetaculos'));
    }

    public function update(Request $request, Depoimento $depoimento)
    {
        try {
            $input['aprovado'] = $request->aprovado;
            $input['espetaculo_id'] = $request->espetaculo_id;
            $input['lido'] = 1;

            $depoimento->update($input);

            return redirect()->route('painel.depoimentos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Depoimento $depoimento)
    {
        try {
            $depoimento->delete();

            return redirect()->route('painel.depoimentos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $depoimento = Depoimento::where('id', $id)->first();
            $depoimento->update([
                'lido' => !$depoimento->lido
            ]);

            return redirect()->route('painel.depoimentos.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
