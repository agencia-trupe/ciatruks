<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EspetaculosVideosRequest;
use App\Models\Espetaculo;
use App\Models\EspetaculoVideo;
use Illuminate\Http\Request;

class EspetaculosVideosController extends Controller
{
    public function index(Espetaculo $espetaculo)
    {
        $videos = EspetaculoVideo::espetaculo($espetaculo->id)->ordenados()->get();

        return view('painel.espetaculos.videos.index', compact('espetaculo', 'videos'));
    }

    public function create(Espetaculo $espetaculo)
    {
        return view('painel.espetaculos.videos.create', compact('espetaculo'));
    }

    public function store(EspetaculosVideosRequest $request, Espetaculo $espetaculo)
    {
        try {
            $input = $request->all();
            $input['espetaculo_id'] = $espetaculo->id;

            EspetaculoVideo::create($input);

            return redirect()->route('painel.espetaculos.videos.index', $espetaculo->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Espetaculo $espetaculo, EspetaculoVideo $video)
    {
        return view('painel.espetaculos.videos.edit', compact('espetaculo', 'video'));
    }

    public function update(EspetaculosVideosRequest $request, Espetaculo $espetaculo, EspetaculoVideo $video)
    {
        try {
            $input = $request->all();
            $input['espetaculo_id'] = $espetaculo->id;

            $video->update($input);

            return redirect()->route('painel.espetaculos.videos.index', $espetaculo->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Espetaculo $espetaculo, EspetaculoVideo $video)
    {
        try {
            $video->delete();

            return redirect()->route('painel.espetaculos.videos.index', $espetaculo->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
