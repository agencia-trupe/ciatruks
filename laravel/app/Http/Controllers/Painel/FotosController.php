<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\FotosRequest;
use App\Models\Foto;
use Illuminate\Http\Request;

class FotosController extends Controller
{
    public function index()
    {
        $imagens = Foto::ordenados()->get();

        return view('painel.galeria-de-fotos.index', compact('imagens'));
    }

    public function show(Foto $galeria_de_foto)
    {
        return $galeria_de_foto;
    }

    public function store(FotosRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = Foto::upload_imagem();

            $imagem = Foto::create($input);

            $view = view('painel.galeria-de-fotos.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Foto $galeria_de_foto)
    {
        try {
            $galeria_de_foto->delete();

            return redirect()->route('painel.galeria-de-fotos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear()
    {
        try {
            Foto::whereNotNull('id')->delete();

            return redirect()->route('painel.galeria-de-fotos.index')->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
