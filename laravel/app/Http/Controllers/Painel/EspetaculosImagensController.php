<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EspetaculosImagensRequest;
use App\Models\Espetaculo;
use App\Models\EspetaculoImagem;
use Illuminate\Http\Request;

class EspetaculosImagensController extends Controller
{
    public function index(Espetaculo $espetaculo)
    {
        $imagens = EspetaculoImagem::espetaculo($espetaculo->id)->ordenados()->get();

        return view('painel.espetaculos.imagens.index', compact('espetaculo', 'imagens'));
    }

    public function show(Espetaculo $espetaculo, EspetaculoImagem $imagem)
    {
        return $imagem;
    }

    public function store(EspetaculosImagensRequest $request, Espetaculo $espetaculo)
    {
        try {
            $input = $request->all();
            $input['imagem'] = EspetaculoImagem::upload_imagem();
            $input['espetaculo_id'] = $espetaculo->id;

            $imagem = EspetaculoImagem::create($input);

            $view = view('painel.espetaculos.imagens.imagem', compact('espetaculo', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Espetaculo $espetaculo, EspetaculoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.espetaculos.imagens.index', $espetaculo->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(Espetaculo $espetaculo)
    {
        try {
            $espetaculo->imagens()->delete();

            return redirect()->route('painel.espetaculos.imagens.index', $espetaculo->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
