<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EspetaculosRequest;
use App\Models\Espetaculo;
use Illuminate\Http\Request;

class EspetaculosController extends Controller
{
    public function index()
    {
        $espetaculos = Espetaculo::ordenados()->get();

        return view('painel.espetaculos.index', compact('espetaculos'));
    }

    public function create()
    {
        return view('painel.espetaculos.create');
    }

    public function store(EspetaculosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Espetaculo::upload_imagem();

            Espetaculo::create($input);

            return redirect()->route('painel.espetaculos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Espetaculo $espetaculo)
    {
        return view('painel.espetaculos.edit', compact('espetaculo'));
    }

    public function update(EspetaculosRequest $request, Espetaculo $espetaculo)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Espetaculo::upload_imagem();

            $espetaculo->update($input);

            return redirect()->route('painel.espetaculos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Espetaculo $espetaculo)
    {
        try {
            $espetaculo->delete();

            return redirect()->route('painel.espetaculos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
