<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CursosTextosRequest;
use App\Models\CursoTexto;
use Illuminate\Http\Request;

class CursosTextosController extends Controller
{
    public function index()
    {
        $textos = CursoTexto::ordenados()->get();

        return view('painel.cursos-e-oficinas.textos.index', compact('textos'));
    }

    public function create()
    {
        return view('painel.cursos-e-oficinas.textos.create');
    }

    public function store(CursosTextosRequest $request)
    {
        try {
            $input = $request->all();

            CursoTexto::create($input);

            return redirect()->route('painel.cursos-e-oficinas.textos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CursoTexto $texto)
    {
        return view('painel.cursos-e-oficinas.textos.edit', compact('texto'));
    }

    public function update(CursosTextosRequest $request, CursoTexto $texto)
    {
        try {
            $input = $request->all();

            $texto->update($input);

            return redirect()->route('painel.cursos-e-oficinas.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CursoTexto $texto)
    {
        try {
            $texto->delete();

            return redirect()->route('painel.cursos-e-oficinas.textos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
