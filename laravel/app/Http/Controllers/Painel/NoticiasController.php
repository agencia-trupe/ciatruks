<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoticiasRequest;
use App\Models\Noticia;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticia::orderBy('data', 'desc')->get();

        return view('painel.noticias.index', compact('noticias'));
    }

    public function create()
    {
        return view('painel.noticias.create');
    }

    public function store(NoticiasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Noticia::upload_imagem();

            Noticia::create($input);

            return redirect()->route('painel.noticias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Noticia $noticia)
    {
        return view('painel.noticias.edit', compact('noticia'));
    }

    public function update(NoticiasRequest $request, Noticia $noticia)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Noticia::upload_imagem();

            $noticia->update($input);

            return redirect()->route('painel.noticias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Noticia $noticia)
    {
        try {
            $noticia->delete();

            return redirect()->route('painel.noticias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
