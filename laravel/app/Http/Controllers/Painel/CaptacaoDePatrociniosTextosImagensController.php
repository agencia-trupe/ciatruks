<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CaptacaoDePatrociniosTextosImagensRequest;
use App\Models\CaptacaoDePatrocinioTexto;
use App\Models\CaptacaoDePatrocinioTextoImagem;
use Illuminate\Http\Request;

class CaptacaoDePatrociniosTextosImagensController extends Controller
{
    public function index(CaptacaoDePatrocinioTexto $texto)
    {
        $imagens = CaptacaoDePatrocinioTextoImagem::texto($texto->id)->ordenados()->get();

        return view('painel.captacao-de-patrocinios.textos.imagens.index', compact('texto', 'imagens'));
    }

    public function show(CaptacaoDePatrocinioTexto $texto, CaptacaoDePatrocinioTextoImagem $imagem)
    {
        return $imagem;
    }

    public function store(CaptacaoDePatrociniosTextosImagensRequest $request, CaptacaoDePatrocinioTexto $texto)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CaptacaoDePatrocinioTextoImagem::upload_imagem();
            $input['texto_id'] = $texto->id;

            $imagem = CaptacaoDePatrocinioTextoImagem::create($input);

            $view = view('painel.captacao-de-patrocinios.textos.imagens.imagem', compact('texto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CaptacaoDePatrocinioTexto $texto, CaptacaoDePatrocinioTextoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.captacao-de-patrocinios.textos.imagens.index', $texto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(CaptacaoDePatrocinioTexto $texto)
    {
        try {
            $texto->imagens()->delete();

            return redirect()->route('painel.captacao-de-patrocinios.textos.imagens.index', $texto->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
