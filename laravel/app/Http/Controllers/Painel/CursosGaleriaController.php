<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CursosGaleriaRequest;
use App\Models\CursoGaleria;
use Illuminate\Http\Request;

class CursosGaleriaController extends Controller
{
    public function index()
    {
        $imagens = CursoGaleria::ordenados()->get();

        return view('painel.cursos-e-oficinas.galeria.index', compact('imagens'));
    }

    public function show(CursoGaleria $galerium)
    {
        return $galerium;
    }

    public function store(CursosGaleriaRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CursoGaleria::upload_imagem();

            $imagem = CursoGaleria::create($input);

            $view = view('painel.cursos-e-oficinas.galeria.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CursoGaleria $galerium)
    {
        try {
            $galerium->delete();

            return redirect()->route('painel.cursos-e-oficinas.galeria.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear()
    {
        try {
            CursoGaleria::whereNotNull('id')->delete();

            return redirect()->route('painel.cursos-e-oficinas.galeria.index')->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
