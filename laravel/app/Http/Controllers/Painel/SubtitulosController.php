<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubtitulosRequest;
use App\Models\Subtitulo;
use Illuminate\Http\Request;

class SubtitulosController extends Controller
{
    public function index()
    {
        $subtitulos = Subtitulo::orderBy('id', 'asc')->get();

        return view('painel.subtitulos.index', compact('subtitulos'));
    }

    public function edit(Subtitulo $subtitulo)
    {
        return view('painel.subtitulos.edit', compact('subtitulo'));
    }

    public function update(SubtitulosRequest $request, Subtitulo $subtitulo)
    {
        try {
            $input = $request->all();

            $subtitulo->update($input);

            return redirect()->route('painel.subtitulos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
