<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CentroDeEstudosTextosRequest;
use App\Models\CentroDeEstudoTexto;
use Illuminate\Http\Request;

class CentroDeEstudosTextosController extends Controller
{
    public function index()
    {
        $textos = CentroDeEstudoTexto::ordenados()->get();

        return view('painel.centro-de-estudos.textos.index', compact('textos'));
    }

    public function create()
    {
        return view('painel.centro-de-estudos.textos.create');
    }

    public function store(CentroDeEstudosTextosRequest $request)
    {
        try {
            $input = $request->all();

            CentroDeEstudoTexto::create($input);

            return redirect()->route('painel.centro-de-estudos.textos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CentroDeEstudoTexto $texto)
    {
        return view('painel.centro-de-estudos.textos.edit', compact('texto'));
    }

    public function update(CentroDeEstudosTextosRequest $request, CentroDeEstudoTexto $texto)
    {
        try {
            $input = $request->all();

            $texto->update($input);

            return redirect()->route('painel.centro-de-estudos.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CentroDeEstudoTexto $texto)
    {
        try {
            $texto->delete();

            return redirect()->route('painel.centro-de-estudos.textos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
