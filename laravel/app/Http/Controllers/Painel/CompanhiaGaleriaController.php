<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanhiaGaleriaRequest;
use App\Models\CompanhiaGaleria;
use Illuminate\Http\Request;

class CompanhiaGaleriaController extends Controller
{
    public function index()
    {
        $imagens = CompanhiaGaleria::ordenados()->get();

        return view('painel.a-companhia.galeria.index', compact('imagens'));
    }

    public function show(CompanhiaGaleria $galerium)
    {
        return $galerium;
    }

    public function store(CompanhiaGaleriaRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CompanhiaGaleria::upload_imagem();

            $imagem = CompanhiaGaleria::create($input);

            $view = view('painel.a-companhia.galeria.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CompanhiaGaleria $galerium)
    {
        try {
            $galerium->delete();

            return redirect()->route('painel.a-companhia.galeria.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear()
    {
        try {
            CompanhiaGaleria::whereNotNull('id')->delete();

            return redirect()->route('painel.a-companhia.galeria.index')->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
