<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CaptacaoDePatrociniosTextosRequest;
use App\Models\CaptacaoDePatrocinioTexto;
use Illuminate\Http\Request;

class CaptacaoDePatrociniosTextosController extends Controller
{
    public function index()
    {
        $textos = CaptacaoDePatrocinioTexto::ordenados()->get();

        return view('painel.captacao-de-patrocinios.textos.index', compact('textos'));
    }

    public function create()
    {
        return view('painel.captacao-de-patrocinios.textos.create');
    }

    public function store(CaptacaoDePatrociniosTextosRequest $request)
    {
        try {
            $input = $request->all();

            CaptacaoDePatrocinioTexto::create($input);

            return redirect()->route('painel.captacao-de-patrocinios.textos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CaptacaoDePatrocinioTexto $texto)
    {
        return view('painel.captacao-de-patrocinios.textos.edit', compact('texto'));
    }

    public function update(CaptacaoDePatrociniosTextosRequest $request, CaptacaoDePatrocinioTexto $texto)
    {
        try {
            $input = $request->all();

            $texto->update($input);

            return redirect()->route('painel.captacao-de-patrocinios.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CaptacaoDePatrocinioTexto $texto)
    {
        try {
            $texto->delete();

            return redirect()->route('painel.captacao-de-patrocinios.textos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
