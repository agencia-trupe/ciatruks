<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanhiaTextosImagensRequest;
use App\Models\CompanhiaTexto;
use App\Models\CompanhiaTextoImagem;
use Illuminate\Http\Request;

class CompanhiaTextosImagensController extends Controller
{
    public function index(CompanhiaTexto $texto)
    {
        $imagens = CompanhiaTextoImagem::texto($texto->id)->ordenados()->get();

        return view('painel.a-companhia.textos.imagens.index', compact('texto', 'imagens'));
    }

    public function show(CompanhiaTexto $texto, CompanhiaTextoImagem $imagem)
    {
        return $imagem;
    }

    public function store(CompanhiaTextosImagensRequest $request, CompanhiaTexto $texto)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CompanhiaTextoImagem::upload_imagem();
            $input['texto_id'] = $texto->id;

            $imagem = CompanhiaTextoImagem::create($input);

            $view = view('painel.a-companhia.textos.imagens.imagem', compact('texto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CompanhiaTexto $texto, CompanhiaTextoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.a-companhia.textos.imagens.index', $texto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(CompanhiaTexto $texto)
    {
        try {
            $texto->imagens()->delete();

            return redirect()->route('painel.a-companhia.textos.imagens.index', $texto->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
