<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MateriasImagensRequest;
use App\Models\Materia;
use App\Models\MateriaImagem;
use Illuminate\Http\Request;

class MateriasImagensController extends Controller
{
    public function index(Materia $materia)
    {
        $imagens = MateriaImagem::materia($materia->id)->ordenados()->get();

        return view('painel.materias.imagens.index', compact('materia', 'imagens'));
    }

    public function show(Materia $materia, MateriaImagem $imagen)
    {
        $imagem = $imagen;
        return $imagem;
    }

    public function store(MateriasImagensRequest $request, Materia $materia)
    {
        try {
            $input = $request->all();
            $input['imagem'] = MateriaImagem::upload_imagem();
            $input['materia_id'] = $materia->id;

            $imagem = MateriaImagem::create($input);

            $view = view('painel.materias.imagens.imagem', compact('materia', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Materia $materia, MateriaImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.materias.imagens.index', $materia->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(Materia $materia)
    {
        try {
            $materia->imagens()->delete();

            return redirect()->route('painel.materias.imagens.index', $materia->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
