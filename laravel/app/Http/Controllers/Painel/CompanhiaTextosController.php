<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanhiaTextosRequest;
use App\Models\CompanhiaTexto;
use Illuminate\Http\Request;

class CompanhiaTextosController extends Controller
{
    public function index()
    {
        $textos = CompanhiaTexto::ordenados()->get();

        return view('painel.a-companhia.textos.index', compact('textos'));
    }

    public function create()
    {
        return view('painel.a-companhia.textos.create');
    }

    public function store(CompanhiaTextosRequest $request)
    {
        try {
            $input = $request->all();

            CompanhiaTexto::create($input);

            return redirect()->route('painel.a-companhia.textos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CompanhiaTexto $texto)
    {
        return view('painel.a-companhia.textos.edit', compact('texto'));
    }

    public function update(CompanhiaTextosRequest $request, CompanhiaTexto $texto)
    {
        try {
            $input = $request->all();

            $texto->update($input);

            return redirect()->route('painel.a-companhia.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CompanhiaTexto $texto)
    {
        try {
            $texto->delete();

            return redirect()->route('painel.a-companhia.textos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
