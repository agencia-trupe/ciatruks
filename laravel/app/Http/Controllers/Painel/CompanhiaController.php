<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanhiaRequest;
use App\Models\Companhia;
use Illuminate\Http\Request;

class CompanhiaController extends Controller
{
    public function index()
    {
        $companhia = Companhia::first();

        return view('painel.a-companhia.edit', compact('companhia'));
    }

    public function update(CompanhiaRequest $request, Companhia $a_companhium)
    {
        try {
            $input = $request->all();

            $a_companhium->update($input);

            return redirect()->route('painel.a-companhia.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
