<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Espetaculo;
use App\Models\EspetaculoHome;
use Illuminate\Http\Request;

class EspetaculosHomeController extends Controller
{
    public function index()
    {
        $espetaculos = EspetaculoHome::join('espetaculos', 'espetaculos.id', '=', 'espetaculos_home.espetaculo_id')
            ->select('espetaculos_home.*', 'espetaculos.titulo', 'espetaculos.imagem')
            ->ordenados()->get();

        return view('painel.home.espetaculos.index', compact('espetaculos'));
    }

    public function create()
    {
        $espetaculos = Espetaculo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.home.espetaculos.create', compact('espetaculos'));
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();

            EspetaculoHome::create($input);

            return redirect()->route('painel.home.espetaculos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(EspetaculoHome $espetaculo)
    {
        $espetaculos = Espetaculo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.home.espetaculos.edit', compact('espetaculo', 'espetaculos'));
    }

    public function update(Request $request, EspetaculoHome $espetaculo)
    {
        try {
            $input = $request->all();

            $espetaculo->update($input);

            return redirect()->route('painel.home.espetaculos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EspetaculoHome $espetaculo)
    {
        try {
            $espetaculo->delete();

            return redirect()->route('painel.home.espetaculos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
