<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CentroDeEstudosTextosImagensRequest;
use App\Models\CentroDeEstudoTexto;
use App\Models\CentroDeEstudoTextoImagem;
use Illuminate\Http\Request;

class CentroDeEstudosTextosImagensController extends Controller
{
    public function index(CentroDeEstudoTexto $texto)
    {
        $imagens = CentroDeEstudoTextoImagem::texto($texto->id)->ordenados()->get();

        return view('painel.centro-de-estudos.textos.imagens.index', compact('texto', 'imagens'));
    }

    public function show(CentroDeEstudoTexto $texto, CentroDeEstudoTextoImagem $imagem)
    {
        return $imagem;
    }

    public function store(CentroDeEstudosTextosImagensRequest $request, CentroDeEstudoTexto $texto)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CentroDeEstudoTextoImagem::upload_imagem();
            $input['texto_id'] = $texto->id;

            $imagem = CentroDeEstudoTextoImagem::create($input);

            $view = view('painel.centro-de-estudos.textos.imagens.imagem', compact('texto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CentroDeEstudoTexto $texto, CentroDeEstudoTextoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.centro-de-estudos.textos.imagens.index', $texto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(CentroDeEstudoTexto $texto)
    {
        try {
            $texto->imagens()->delete();

            return redirect()->route('painel.centro-de-estudos.textos.imagens.index', $texto->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
