<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoticiasImagensRequest;
use App\Models\Noticia;
use App\Models\NoticiaImagem;
use Illuminate\Http\Request;

class NoticiasImagensController extends Controller
{
    public function index(Noticia $noticia)
    {
        $imagens = NoticiaImagem::noticia($noticia->id)->ordenados()->get();

        return view('painel.noticias.imagens.index', compact('noticia', 'imagens'));
    }

    public function show(Noticia $noticia, NoticiaImagem $imagem)
    {
        return $imagem;
    }

    public function store(NoticiasImagensRequest $request, Noticia $noticia)
    {
        try {
            $input = $request->all();
            $input['imagem'] = NoticiaImagem::upload_imagem();
            $input['noticia_id'] = $noticia->id;

            $imagem = NoticiaImagem::create($input);

            $view = view('painel.noticias.imagens.imagem', compact('noticia', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Noticia $noticia, NoticiaImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.noticias.imagens.index', $noticia->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(Noticia $noticia)
    {
        try {
            $noticia->imagens()->delete();

            return redirect()->route('painel.noticias.imagens.index', $noticia->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
