<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PremiacoesRequest;
use App\Models\Premiacao;
use Illuminate\Http\Request;

class PremiacoesController extends Controller
{
    public function index()
    {
        $premiacoes = Premiacao::ordenados()->get();

        return view('painel.premiacoes.index', compact('premiacoes'));
    }

    public function create()
    {
        return view('painel.premiacoes.create');
    }

    public function store(PremiacoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Premiacao::upload_imagem();

            Premiacao::create($input);

            return redirect()->route('painel.premiacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Premiacao $premiaco)
    {
        $premiacao = $premiaco;
        return view('painel.premiacoes.edit', compact('premiacao'));
    }

    public function update(PremiacoesRequest $request, Premiacao $premiaco)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Premiacao::upload_imagem();

            $premiaco->update($input);

            return redirect()->route('painel.premiacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Premiacao $premiaco)
    {
        try {
            $premiaco->delete();

            return redirect()->route('painel.premiacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
