<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContratacoesTextosRequest;
use App\Models\ContratacaoTexto;
use Illuminate\Http\Request;

class ContratacoesTextosController extends Controller
{
    public function index()
    {
        $textos = ContratacaoTexto::ordenados()->get();

        return view('painel.contratacoes.textos.index', compact('textos'));
    }

    public function create()
    {
        return view('painel.contratacoes.textos.create');
    }

    public function store(ContratacoesTextosRequest $request)
    {
        try {
            $input = $request->all();

            ContratacaoTexto::create($input);

            return redirect()->route('painel.contratacoes.textos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ContratacaoTexto $texto)
    {
        return view('painel.contratacoes.textos.edit', compact('texto'));
    }

    public function update(ContratacoesTextosRequest $request, ContratacaoTexto $texto)
    {
        try {
            $input = $request->all();

            $texto->update($input);

            return redirect()->route('painel.contratacoes.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ContratacaoTexto $texto)
    {
        try {
            $texto->delete();

            return redirect()->route('painel.contratacoes.textos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
