<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgendaRequest;
use App\Models\Agenda;
use App\Models\Espetaculo;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    public function index()
    {
        $dataAtual = date('Y-m-d');

        $agenda = Agenda::join('espetaculos', 'espetaculos.id', '=', 'agenda.espetaculo_id')
            ->select('agenda.*', 'espetaculos.titulo as espetaculo_titulo', 'espetaculos.imagem as espetaculo_imagem', 'espetaculos.tipo as espetaculo_tipo')
            ->where('data', '>=', $dataAtual)
            ->orderBy('agenda.data', 'desc')->get();

        $anteriores = Agenda::join('espetaculos', 'espetaculos.id', '=', 'agenda.espetaculo_id')
            ->select('agenda.*', 'espetaculos.titulo as espetaculo_titulo', 'espetaculos.imagem as espetaculo_imagem', 'espetaculos.tipo as espetaculo_tipo')
            ->where('data', '<', $dataAtual)
            ->orderBy('agenda.data', 'desc')->get();

        return view('painel.agenda.index', compact('agenda', 'anteriores'));
    }

    public function create()
    {
        $espetaculos = Espetaculo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.agenda.create', compact('espetaculos'));
    }

    public function store(AgendaRequest $request)
    {
        try {
            $input = $request->all();

            Agenda::create($input);

            return redirect()->route('painel.agenda.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Agenda $agenda)
    {
        $espetaculos = Espetaculo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.agenda.edit', compact('agenda', 'espetaculos'));
    }

    public function update(AgendaRequest $request, Agenda $agenda)
    {
        try {
            $input = $request->all();

            $agenda->update($input);

            return redirect()->route('painel.agenda.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Agenda $agenda)
    {
        try {
            $agenda->delete();

            return redirect()->route('painel.agenda.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
