<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CursosTextosImagensRequest;
use App\Models\CursoTexto;
use App\Models\CursoTextoImagem;
use Illuminate\Http\Request;

class CursosTextosImagensController extends Controller
{
    public function index(CursoTexto $texto)
    {
        $imagens = CursoTextoImagem::texto($texto->id)->ordenados()->get();

        return view('painel.cursos-e-oficinas.textos.imagens.index', compact('texto', 'imagens'));
    }

    public function show(CursoTexto $texto, CursoTextoImagem $imagem)
    {
        return $imagem;
    }

    public function store(CursosTextosImagensRequest $request, CursoTexto $texto)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CursoTextoImagem::upload_imagem();
            $input['texto_id'] = $texto->id;

            $imagem = CursoTextoImagem::create($input);

            $view = view('painel.cursos-e-oficinas.textos.imagens.imagem', compact('texto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CursoTexto $texto, CursoTextoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.cursos-e-oficinas.textos.imagens.index', $texto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(CursoTexto $texto)
    {
        try {
            $texto->imagens()->delete();

            return redirect()->route('painel.cursos-e-oficinas.textos.imagens.index', $texto->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
