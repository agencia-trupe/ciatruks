<?php

namespace App\Http\Controllers;

use App\Models\ContratacaoTexto;
use App\Models\ContratacaoTextoImagem;
use Illuminate\Http\Request;

class ContratacoesController extends Controller
{
    public function index()
    {
        $textos = ContratacaoTexto::ordenados()->get();
        $imagens = ContratacaoTextoImagem::ordenados()->get();

        return view('frontend.contratacoes', compact('textos', 'imagens'));
    }
}
