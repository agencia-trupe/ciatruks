<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepoimentosRequest;
use App\Models\Contato;
use App\Models\Depoimento;
use App\Notifications\DepoimentosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class DepoimentosController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::aprovados()
            ->join('espetaculos', 'espetaculos.id', '=', 'depoimentos.espetaculo_id')
            ->select('depoimentos.*', 'espetaculos.slug', 'espetaculos.imagem', 'espetaculos.titulo')
            ->orderBy('created_at', 'desc')->get();

        return view('frontend.depoimentos', compact('depoimentos'));
    }

    public function post(DepoimentosRequest $request, Depoimento $depoimento)
    {
        try {
            $data = $request->all();

            $depoimento->create($data);

            Notification::send(
                Contato::first(),
                new DepoimentosNotification($data)
            );

            return back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar depoimento: ' . $e->getMessage()]);
        }
    }
}
