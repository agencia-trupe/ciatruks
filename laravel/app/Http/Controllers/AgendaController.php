<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    public function index()
    {
        $dataAtual = date('Y-m-d');

        $agenda = Agenda::join('espetaculos', 'espetaculos.id', '=', 'agenda.espetaculo_id')
            ->select('agenda.*', 'espetaculos.titulo as espetaculo_titulo', 'espetaculos.imagem as espetaculo_imagem', 'espetaculos.tipo as espetaculo_tipo', 'espetaculos.slug as espetaculo_slug')
            ->where('data', '>=', $dataAtual)
            ->orderBy('agenda.data', 'asc')->get();

        return view('frontend.agenda', compact('agenda'));
    }
}
