<?php

namespace App\Http\Controllers;

use App\Models\CursoGaleria;
use App\Models\CursoPlanejamento;
use App\Models\CursoTexto;
use App\Models\CursoTextoImagem;
use Illuminate\Http\Request;

class CursosEOficinasController extends Controller
{
    public function index()
    {
        $textos = CursoTexto::ordenados()->get();
        $imagens = CursoTextoImagem::ordenados()->get();
        $planejamentos = CursoPlanejamento::ordenados()->get();
        $galeria = CursoGaleria::ordenados()->get();

        return view('frontend.cursos-e-oficinas', compact('textos', 'imagens', 'planejamentos', 'galeria'));
    }

    public function showPlanejamento($slug)
    {
        $planejamento = CursoPlanejamento::where('slug', $slug)->first();

        return view('frontend.cursos-e-oficinas-planejamento', compact('planejamento'));
    }
}
