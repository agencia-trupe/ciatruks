<?php

namespace App\Http\Controllers;

use App\Models\Espetaculo;
use App\Models\EspetaculoImagem;
use App\Models\EspetaculoVideo;
use Illuminate\Http\Request;

class EspetaculosController extends Controller
{
    public function index()
    {
        $espetaculos = Espetaculo::ordenados()->get();

        return view('frontend.espetaculos', compact('espetaculos'));
    }

    public function show($slug)
    {
        $espetaculo = Espetaculo::where('slug', $slug)->first();
        $videos = EspetaculoVideo::espetaculo($espetaculo->id)->ordenados()->get();
        $imagens = EspetaculoImagem::espetaculo($espetaculo->id)->ordenados()->get();
        
        return view('frontend.espetaculos-show', compact('espetaculo', 'videos', 'imagens'));
    }
}
