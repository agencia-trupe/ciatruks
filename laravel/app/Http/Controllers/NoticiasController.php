<?php

namespace App\Http\Controllers;

use App\Models\Noticia;
use App\Models\NoticiaImagem;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticia::orderBy('data', 'desc')->get();

        return view('frontend.noticias', compact('noticias'));
    }

    public function show($slug)
    {
        $noticia = Noticia::where('slug', $slug)->first();
        $imagens = NoticiaImagem::noticia($noticia->id)->ordenados()->get();

        return view('frontend.noticias-show', compact('noticia', 'imagens'));
    }
}
