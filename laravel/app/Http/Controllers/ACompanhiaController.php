<?php

namespace App\Http\Controllers;

use App\Models\Companhia;
use App\Models\CompanhiaGaleria;
use App\Models\CompanhiaTexto;
use App\Models\CompanhiaTextoImagem;
use Illuminate\Http\Request;

class ACompanhiaController extends Controller
{
    public function index()
    {
        $companhia = Companhia::first();
        $textos = CompanhiaTexto::ordenados()->get();
        $imagens = CompanhiaTextoImagem::ordenados()->get();
        $galeria = CompanhiaGaleria::ordenados()->get();

        return view('frontend.a-companhia', compact('companhia', 'textos', 'imagens', 'galeria'));
    }
}
