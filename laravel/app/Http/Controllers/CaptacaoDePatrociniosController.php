<?php

namespace App\Http\Controllers;

use App\Models\CaptacaoDePatrocinioTexto;
use App\Models\CaptacaoDePatrocinioTextoImagem;
use Illuminate\Http\Request;

class CaptacaoDePatrociniosController extends Controller
{
    public function index()
    {
        $textos = CaptacaoDePatrocinioTexto::ordenados()->get();
        $imagens = CaptacaoDePatrocinioTextoImagem::ordenados()->get();

        return view('frontend.captacao-de-patrocinios', compact('textos', 'imagens'));
    }
}
