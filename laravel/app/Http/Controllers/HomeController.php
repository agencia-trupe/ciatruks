<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Agenda;
use App\Models\Banner;
use App\Models\Depoimento;
use App\Models\EspetaculoHome;
use App\Models\Home;
use App\Models\Noticia;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $home = Home::first();

        $dataAtual = date('Y-m-d');

        $agendaHome = Agenda::join('espetaculos', 'espetaculos.id', '=', 'agenda.espetaculo_id')
            ->select('agenda.*', 'espetaculos.titulo as espetaculo_titulo', 'espetaculos.imagem as espetaculo_imagem', 'espetaculos.tipo as espetaculo_tipo', 'espetaculos.slug as espetaculo_slug')
            ->where('data', '>=', $dataAtual)
            ->orderBy('agenda.data', 'asc')->take(4)->get();
        
        $espetaculosHome = EspetaculoHome::ordenados()->join('espetaculos', 'espetaculos.id', '=', 'espetaculos_home.espetaculo_id')
            ->select('espetaculos_home.*', 'espetaculos.imagem', 'espetaculos.titulo', 'espetaculos.slug')
            ->take(6)->get();
        
        $noticias = Noticia::orderBy('data', 'desc')->take(2)->get();

        // dd($noticias);

        return view('frontend.home', compact('banners', 'home', 'agendaHome', 'espetaculosHome', 'noticias'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
