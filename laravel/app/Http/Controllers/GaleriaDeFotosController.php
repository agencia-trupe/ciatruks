<?php

namespace App\Http\Controllers;

use App\Models\Foto;
use Illuminate\Http\Request;

class GaleriaDeFotosController extends Controller
{
    public function index()
    {
        $galeria = Foto::ordenados()->get();

        return view('frontend.galeria-de-fotos', compact('galeria'));
    }
}
