<?php

namespace App\Http\Controllers;

use App\Models\Materia;
use Illuminate\Http\Request;

class CriticasEMateriasController extends Controller
{
    public function index()
    {
        $materias = Materia::ordenados()->get();

        return view('frontend.criticas-e-materias', compact('materias'));
    }
}
