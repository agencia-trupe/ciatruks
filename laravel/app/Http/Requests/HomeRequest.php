<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frase_agenda'  => 'required',
            'imagem_fotos'  => 'image',
            'imagem_videos' => 'image',
            'link1_titulo'  => 'required',
            'link1_link'    => 'required',
            'link2_titulo'  => 'required',
            'link2_link'    => 'required',
            'link3_titulo'  => 'required',
            'link3_link'    => 'required',
        ];
    }
}
