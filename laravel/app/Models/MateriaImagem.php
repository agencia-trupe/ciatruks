<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MateriaImagem extends Model
{
    use HasFactory;

    protected $table = 'materias_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeMateria($query, $id)
    {
        return $query->where('materia_id', $id);
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         'width'  => null,
        //         'height' => null,
        //         'upsize'  => true,
        //         'path'    => 'assets/img/materias/imagens/'
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => null,
            'upsize'  => true,
            'path'    => 'assets/img/materias/imagens/'
        ]);
        // }
    }
}
