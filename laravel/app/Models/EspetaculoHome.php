<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EspetaculoHome extends Model
{
    use HasFactory;

    protected $table = 'espetaculos_home';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeEspetaculo($query, $id)
    {
        return $query->where('espetaculo_id', $id);
    }
}
