<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_fotos()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem_fotos', [
        //         'width'  => 1980,
        //         'height' => null,
        //         'path'   => 'assets/img/home/'
        //     ]);
        // } else {
        return CropImage::make('imagem_fotos', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
        // }
    }

    public static function upload_imagem_videos()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem_videos', [
        //         'width'  => 1980,
        //         'height' => null,
        //         'path'   => 'assets/img/home/'
        //     ]);
        // } else {
        return CropImage::make('imagem_videos', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
        // }
    }
}
