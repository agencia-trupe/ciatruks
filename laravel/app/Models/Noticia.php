<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    protected $table = 'noticias';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function imagens()
    {
        return $this->hasMany('App\Models\NoticiaImagem', 'noticia_id')->ordenados();
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         [
        //             'width'  => 300,
        //             'height' => 200,
        //             'path'   => 'assets/img/noticias/'
        //         ],
        //         [
        //             'width'  => 800,
        //             'height' => null,
        //             'path'   => 'assets/img/noticias/show/'
        //         ]
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            [
                'width'  => 300,
                'height' => 200,
                'path'   => 'assets/img/noticias/'
            ],
            [
                'width'  => 800,
                'height' => null,
                'path'   => 'assets/img/noticias/show/'
            ]
        ]);
        // }
    }
}
