<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Espetaculo extends Model
{
    use HasFactory;
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    protected $table = 'espetaculos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\EspetaculoVideo', 'espetaculo_id')->ordenados();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\EspetaculoImagem', 'espetaculo_id')->ordenados();
    }

    public function home()
    {
        return $this->hasMany('App\Models\EspetaculoHome', 'espetaculo_id')->ordenados();
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         [
        //             'width'  => 350,
        //             'height' => 440,
        //             'path'   => 'assets/img/espetaculos/'
        //         ],
        //         [
        //             'width'  => 350,
        //             'height' => 350,
        //             'path'   => 'assets/img/espetaculos/agenda/'
        //         ],
        //         [
        //             'width'  => 120,
        //             'height' => 120,
        //             'path'   => 'assets/img/espetaculos/depoimentos/'
        //         ],
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            [
                'width'  => 350,
                'height' => 440,
                'path'   => 'assets/img/espetaculos/'
            ],
            [
                'width'  => 350,
                'height' => 350,
                'path'   => 'assets/img/espetaculos/agenda/'
            ],
            [
                'width'  => 120,
                'height' => 120,
                'path'   => 'assets/img/espetaculos/depoimentos/'
            ],
        ]);
        // }
    }
}
