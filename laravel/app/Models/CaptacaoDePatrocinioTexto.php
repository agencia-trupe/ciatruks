<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaptacaoDePatrocinioTexto extends Model
{
    use HasFactory;

    protected $table = 'captacao_de_patrocinios_textos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\CaptacaoDePatrocinioTextoImagem', 'texto_id')->ordenados();
    }
}
