<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    use HasFactory;

    protected $table = 'agenda';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function scopeEspetaculo($query, $id)
    {
        return $query->where('espetaculo_id', $id);
    }

    public function scopeGratuito($query)
    {
        return $query->where('gratuito', '=', 1);
    }
}
