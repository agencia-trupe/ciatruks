<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaImagem extends Model
{
    use HasFactory;

    protected $table = 'noticias_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeNoticia($query, $id)
    {
        return $query->where('noticia_id', $id);
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         'width'  => 400,
        //         'height' => null,
        //         'path'   => 'assets/img/noticias/imagens/'
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/noticias/imagens/'
        ]);
        // }
    }
}
