<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Premiacao extends Model
{
    use HasFactory;

    protected $table = 'premiacoes';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         'width'  => 300,
        //         'height' => null,
        //         'path'   => 'assets/img/premiacoes/'
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/premiacoes/'
        ]);
        // }
    }
}
