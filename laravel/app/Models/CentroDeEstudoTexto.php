<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CentroDeEstudoTexto extends Model
{
    use HasFactory;

    protected $table = 'centro_de_estudos_textos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\CentroDeEstudoTextoImagem', 'texto_id')->ordenados();
    }
}
