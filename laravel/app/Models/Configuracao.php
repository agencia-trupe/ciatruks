<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    use HasFactory;

    protected $table = 'configuracoes';

    protected $guarded = ['id'];

    public static function upload_imagem_de_compartilhamento()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem_de_compartilhamento', [
        //         'width'  => null,
        //         'height' => null,
        //         'path'   => 'assets/img/'
        //     ]);
        // } else {
        return CropImage::make('imagem_de_compartilhamento', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/'
        ]);
        // }
    }
}
