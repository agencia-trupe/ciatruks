<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    use HasFactory;

    protected $table = 'materias';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\MateriaImagem', 'materia_id')->ordenados();
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         'width'  => 380,
        //         'height' => null,
        //         'path'   => 'assets/img/materias/'
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            'width'  => 380,
            'height' => null,
            'path'   => 'assets/img/materias/'
        ]);
        // }
    }
}
