<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaptacaoDePatrocinioTextoImagem extends Model
{
    use HasFactory;

    protected $table = 'captacao_de_patrocinios_textos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeTexto($query, $id)
    {
        return $query->where('texto_id', $id);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/captacao-de-patrocinios/imagens/'
        ]);
    }
}
