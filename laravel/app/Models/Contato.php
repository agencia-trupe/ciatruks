<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contato extends Model
{
    use HasFactory;
    use Notifiable;

    protected $table = 'contatos';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         'width'  => 400,
        //         'height' => null,
        //         'path'   => 'assets/img/contato/'
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/contato/'
        ]);
        // }
    }
}
