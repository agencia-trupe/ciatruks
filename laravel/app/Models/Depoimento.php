<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Depoimento extends Model
{
    use HasFactory;

    protected $table = 'depoimentos';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }

    public function scopeAprovados($query)
    {
        return $query->where('aprovado', '=', 1);
    }

    public function scopeNaoAprovados($query)
    {
        return $query->where('aprovado', '!=', 1);
    }

    public function scopeEspetaculo($query, $id)
    {
        return $query->where('espetaculo_id', $id);
    }

    public function countNaoAprovados()
    {
        return $this->naoAprovados()->count();
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 1);
    }

    public function countNaoLidos()
    {
        return $this->naoLidos()->count();
    }
}
