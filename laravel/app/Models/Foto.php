<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    use HasFactory;

    protected $table = 'fotos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         [
        //             'width'  => 400,
        //             'height' => null,
        //             'path'   => 'assets/img/fotos/'
        //         ],
        //         [
        //             'width'  => null,
        //             'height' => null,
        //             'upsize'  => true,
        //             'path'   => 'assets/img/fotos/big/'
        //         ]
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            [
                'width'  => 400,
                'height' => null,
                'path'   => 'assets/img/fotos/'
            ],
            [
                'width'  => null,
                'height' => null,
                'upsize'  => true,
                'path'   => 'assets/img/fotos/big/'
            ]
        ]);
        // }
    }
}
