<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EspetaculoImagem extends Model
{
    use HasFactory;

    protected $table = 'espetaculos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeEspetaculo($query, $id)
    {
        return $query->where('espetaculo_id', $id);
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         [
        //             'width'  => 325,
        //             'height' => null,
        //             'path'   => 'assets/img/espetaculos/imagens/'
        //         ],
        //         [
        //             'width'  => null,
        //             'height' => null,
        //             'upsize'  => true,
        //             'path'   => 'assets/img/espetaculos/imagens/big/'
        //         ]
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            [
                'width'  => 325,
                'height' => null,
                'path'   => 'assets/img/espetaculos/imagens/'
            ],
            [
                'width'  => null,
                'height' => null,
                'upsize'  => true,
                'path'   => 'assets/img/espetaculos/imagens/big/'
            ]
        ]);
        // }
    }
}
