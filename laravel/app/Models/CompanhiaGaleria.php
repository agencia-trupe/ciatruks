<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanhiaGaleria extends Model
{
    use HasFactory;

    protected $table = 'companhia_galeria';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        // if (Configuracao::first()->tinify_key) {
        //     return CropImageTinify::make('imagem', [
        //         [
        //             'width'  => 325,
        //             'height' => null,
        //             'path'   => 'assets/img/a-companhia/galeria/'
        //         ],
        //         [
        //             'width'  => null,
        //             'height' => null,
        //             'upsize'  => true,
        //             'path'   => 'assets/img/a-companhia/galeria/big/'
        //         ]
        //     ]);
        // } else {
        return CropImage::make('imagem', [
            [
                'width'  => 325,
                'height' => null,
                'path'   => 'assets/img/a-companhia/galeria/'
            ],
            [
                'width'  => null,
                'height' => null,
                'upsize'  => true,
                'path'   => 'assets/img/a-companhia/galeria/big/'
            ]
        ]);
        // }
    }
}
