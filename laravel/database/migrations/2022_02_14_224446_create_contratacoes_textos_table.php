<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratacoesTextosTable extends Migration
{
    public function up()
    {
        Schema::create('contratacoes_textos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contratacoes_textos');
    }
}
