<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepoimentosTable extends Migration
{
    public function up()
    {
        Schema::create('depoimentos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('email');
            $table->text('mensagem');
            $table->boolean('lido')->default(0);
            $table->boolean('aprovado')->default(0);
            $table->foreignId('espetaculo_id')->nullable()->constrained('espetaculos')->cascadeOnUpdate()->nullOnDelete();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('depoimentos');
    }
}
