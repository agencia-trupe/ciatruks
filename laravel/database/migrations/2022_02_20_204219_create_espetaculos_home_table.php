<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspetaculosHomeTable extends Migration
{
    public function up()
    {
        Schema::create('espetaculos_home', function (Blueprint $table) {
            $table->id();
            $table->foreignId('espetaculo_id')->constrained('espetaculos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('espetaculos_home');
    }
}
