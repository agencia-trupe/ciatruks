<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContacaoHistoriasTextosTable extends Migration
{
    public function up()
    {
        Schema::create('contacao_historias_textos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contacao_historias_textos');
    }
}
