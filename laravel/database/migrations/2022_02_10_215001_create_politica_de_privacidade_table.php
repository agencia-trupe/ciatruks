<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoliticaDePrivacidadeTable extends Migration
{
    public function up()
    {
        Schema::create('politica_de_privacidade', function (Blueprint $table) {
            $table->id();
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('politica_de_privacidade');
    }
}
