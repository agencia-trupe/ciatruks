<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTextosImagensTable extends Migration
{
    public function up()
    {
        Schema::create('cursos_textos_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('texto_id')->constrained('cursos_textos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cursos_textos_imagens');
    }
}
