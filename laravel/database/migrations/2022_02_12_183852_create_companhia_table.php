<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanhiaTable extends Migration
{
    public function up()
    {
        Schema::create('companhia', function (Blueprint $table) {
            $table->id();
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companhia');
    }
}
