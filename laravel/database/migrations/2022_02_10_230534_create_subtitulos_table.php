<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubtitulosTable extends Migration
{
    public function up()
    {
        Schema::create('subtitulos', function (Blueprint $table) {
            $table->id();
            $table->string('pagina');
            $table->string('rota');
            $table->string('subtitulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('subtitulos');
    }
}
