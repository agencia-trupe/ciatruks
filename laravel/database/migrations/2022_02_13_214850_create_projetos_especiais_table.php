<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetosEspeciaisTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_especiais', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->text('texto')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('projetos_especiais');
    }
}
