<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanhiaGaleriaTable extends Migration
{
    public function up()
    {
        Schema::create('companhia_galeria', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companhia_galeria');
    }
}
