<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaTable extends Migration
{
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->id();
            $table->foreignId('espetaculo_id')->nullable()->constrained('espetaculos')->cascadeOnUpdate()->nullOnDelete();
            $table->date('data');
            $table->string('horario');
            $table->string('local');
            $table->string('endereco');
            $table->string('bairro');
            $table->string('cidade_uf');
            $table->string('telefone')->nullable();
            $table->boolean('gratuito')->default(0);
            $table->string('link');
            $table->string('titulo_link');
            $table->string('titulo_link_home');
            $table->string('observacao')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('agenda');
    }
}
