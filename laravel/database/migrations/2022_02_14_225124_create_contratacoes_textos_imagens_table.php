<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratacoesTextosImagensTable extends Migration
{
    public function up()
    {
        Schema::create('contratacoes_textos_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('texto_id')->constrained('contratacoes_textos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contratacoes_textos_imagens');
    }
}
