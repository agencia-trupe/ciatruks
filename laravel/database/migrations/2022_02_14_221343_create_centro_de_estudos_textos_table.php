<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroDeEstudosTextosTable extends Migration
{
    public function up()
    {
        Schema::create('centro_de_estudos_textos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('centro_de_estudos_textos');
    }
}
