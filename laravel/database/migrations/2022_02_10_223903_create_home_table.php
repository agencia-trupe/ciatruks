<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('frase_agenda');
            $table->string('imagem_fotos');
            $table->string('imagem_videos');
            $table->string('link1_titulo');
            $table->string('link1_link');
            $table->string('link2_titulo');
            $table->string('link2_link');
            $table->string('link3_titulo');
            $table->string('link3_link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('home');
    }
}
