<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspetaculosTable extends Migration
{
    public function up()
    {
        Schema::create('espetaculos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('slug');
            $table->string('titulo');
            $table->string('tipo');
            $table->text('descricao');
            $table->text('texto')->nullable();
            $table->text('ficha_tecnica')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('espetaculos');
    }
}
