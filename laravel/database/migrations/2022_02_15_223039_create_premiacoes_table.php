<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiacoesTable extends Migration
{
    public function up()
    {
        Schema::create('premiacoes', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->string('imagem')->nullable();
            $table->string('titulo');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('premiacoes');
    }
}
