<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspetaculosVideosTable extends Migration
{
    public function up()
    {
        Schema::create('espetaculos_videos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('espetaculo_id')->constrained('espetaculos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('espetaculos_videos');
    }
}
