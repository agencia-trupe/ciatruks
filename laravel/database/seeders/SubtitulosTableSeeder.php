<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubtitulosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('subtitulos')->insert([
            'id'        => 1,
            'pagina'    => 'A Companhia',
            'rota'      => 'a-companhia',
            'subtitulo' => 'O GRUPO: Cia Truks - Teatro de Bonecos',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 2,
            'pagina'    => 'Agenda',
            'rota'      => 'agenda',
            'subtitulo' => 'Acompanhe aqui todas as nossas apresentações e compareça!',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 3,
            'pagina'    => 'Espetáculos',
            'rota'      => 'espetaculos',
            'subtitulo' => 'Conheça cada detalhe de todos os nossos espetáculos',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 4,
            'pagina'    => 'Cursos e Oficinas',
            'rota'      => 'cursos-e-oficinas',
            'subtitulo' => 'Aprenda com a gente: Nossa metodologia de trabalho',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 5,
            'pagina'    => 'Projetos Especiais',
            'rota'      => 'projetos-especiais',
            'subtitulo' => 'Saiba mais sobre alguns projetos em que trabalhamos',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 6,
            'pagina'    => 'Centro de Estudos',
            'rota'      => 'centro-de-estudos',
            'subtitulo' => 'Centro de estudos e práticas do teatro de animação',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 7,
            'pagina'    => 'Captação de Patrocínios',
            'rota'      => 'captacao-de-patrocinios',
            'subtitulo' => 'Centro de estudos e práticas do teatro de animação',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 8,
            'pagina'    => 'Contratações',
            'rota'      => 'contratacoes',
            'subtitulo' => 'Informações gerais',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 9,
            'pagina'    => 'Galeria de Fotos',
            'rota'      => 'galeria-de-fotos',
            'subtitulo' => 'Confira momentos inesquecíveis da Cia Truks',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 10,
            'pagina'    => 'Vídeos',
            'rota'      => 'videos',
            'subtitulo' => 'Nossos registros animados',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 11,
            'pagina'    => 'Notícias',
            'rota'      => 'noticias',
            'subtitulo' => 'Acompanhe aqui todas as nossas novidades!',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 12,
            'pagina'    => 'Premiações',
            'rota'      => 'premiacoes',
            'subtitulo' => 'Reconhecimento e orgulho!',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 13,
            'pagina'    => 'Críticas e Matérias',
            'rota'      => 'criticas-e-materias',
            'subtitulo' => 'Onde fomos citados e comentados',
        ]);

        DB::table('subtitulos')->insert([
            'id'        => 14,
            'pagina'    => 'Contato',
            'rota'      => 'contato',
            'subtitulo' => 'Fale conosco',
        ]);
    }
}
