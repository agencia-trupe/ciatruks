<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'email'     => 'contato@truks.com.br',
            'telefone'  => '11 3865 8019',
            'whatsapp'  => '11 99952 8553',
            'instagram' => 'https://www.instagram.com/ciatruks/',
            'imagem'    => '',
        ]);
    }
}
