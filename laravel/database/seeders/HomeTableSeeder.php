<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'frase_agenda'  => 'Confira as datas dos próximos espetáculos e compareça!',
            'imagem_fotos'  => '',
            'imagem_videos' => '',
            'link1_titulo'  => 'CONHEÇA A COMPANHIA',
            'link1_link'    => '',
            'link2_titulo'  => 'ASSISTA A UM ESPETÁCULO',
            'link2_link'    => '',
            'link3_titulo'  => 'APRENDA COM A GENTE',
            'link3_link'    => '',
        ]);
    }
}
