<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(SubtitulosTableSeeder::class);
        $this->call(CompanhiaTableSeeder::class);
        $this->call(TiposTableSeeder::class);
    }
}
