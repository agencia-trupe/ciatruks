<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\AgendaController;
use App\Http\Controllers\Painel\BannersController;
use App\Http\Controllers\Painel\CaptacaoDePatrociniosTextosController;
use App\Http\Controllers\Painel\CaptacaoDePatrociniosTextosImagensController;
use App\Http\Controllers\Painel\CentroDeEstudosTextosController;
use App\Http\Controllers\Painel\CentroDeEstudosTextosImagensController;
use App\Http\Controllers\Painel\CompanhiaController;
use App\Http\Controllers\Painel\CompanhiaGaleriaController;
use App\Http\Controllers\Painel\CompanhiaTextosController;
use App\Http\Controllers\Painel\CompanhiaTextosImagensController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatosController;
use App\Http\Controllers\Painel\ContatosRecebidosController;
use App\Http\Controllers\Painel\ContratacoesTextosController;
use App\Http\Controllers\Painel\ContratacoesTextosImagensController;
use App\Http\Controllers\Painel\CursosGaleriaController;
use App\Http\Controllers\Painel\CursosPlanejamentosController;
use App\Http\Controllers\Painel\CursosTextosController;
use App\Http\Controllers\Painel\CursosTextosImagensController;
use App\Http\Controllers\Painel\DepoimentosController;
use App\Http\Controllers\Painel\EspetaculosController;
use App\Http\Controllers\Painel\EspetaculosHomeController;
use App\Http\Controllers\Painel\EspetaculosImagensController;
use App\Http\Controllers\Painel\EspetaculosVideosController;
use App\Http\Controllers\Painel\FotosController;
use App\Http\Controllers\Painel\HomeController;
use App\Http\Controllers\Painel\MateriasController;
use App\Http\Controllers\Painel\MateriasImagensController;
use App\Http\Controllers\Painel\NoticiasController;
use App\Http\Controllers\Painel\NoticiasImagensController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PoliticaDePrivacidadeController;
use App\Http\Controllers\Painel\PremiacoesController;
use App\Http\Controllers\Painel\ProjetosEspeciaisController;
use App\Http\Controllers\Painel\SubtitulosController;
use App\Http\Controllers\Painel\UsersController;
use App\Http\Controllers\Painel\VideosController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });

        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);
        Route::name('painel.')->group(function () {
            Route::resource('usuarios', UsersController::class)->except('show');
            Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
            Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
            Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
            Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
            Route::get('contatos-recebidos/{id}/toggle', [ContatosRecebidosController::class, 'toggle'])->name('contatos-recebidos.toggle');
            Route::resource('contatos-recebidos', ContatosRecebidosController::class)->only(['index', 'show', 'destroy']);
            Route::resource('home', HomeController::class)->only(['index', 'update']);
            Route::resource('banners', BannersController::class)->except('show');
            Route::resource('home/espetaculos', EspetaculosHomeController::class)->except('show')->names('home.espetaculos');
            Route::resource('subtitulos', SubtitulosController::class)->only(['index', 'edit', 'update']);
            Route::get('depoimentos/{id}/toggle', [DepoimentosController::class, 'toggle'])->name('depoimentos.toggle');
            Route::resource('depoimentos', DepoimentosController::class)->only(['index', 'show', 'edit', 'update', 'destroy']);
            Route::resource('agenda', AgendaController::class)->except('show');
            Route::resource('projetos-especiais', ProjetosEspeciaisController::class)->except('show');
            Route::get('galeria-de-fotos/clear', [FotosController::class, 'clear'])->name('galeria-de-fotos.clear');
            Route::resource('galeria-de-fotos', FotosController::class)->only(['index', 'show', 'store', 'destroy'])->names('galeria-de-fotos');
            Route::resource('videos', VideosController::class)->except('show');
            Route::resource('noticias', NoticiasController::class)->except('show');
            Route::get('noticias/{noticia}/imagens/clear', [NoticiasImagensController::class, 'clear'])->name('noticias.imagens.clear');
            Route::resource('noticias.imagens', NoticiasImagensController::class)->except('show')->names('noticias.imagens');
            Route::resource('premiacoes', PremiacoesController::class)->except('show');
            Route::resource('materias', MateriasController::class)->except('show');
            Route::get('materias/{materia}/imagens/clear', [MateriasImagensController::class, 'clear'])->name('materias.imagens.clear');
            Route::resource('materias.imagens', MateriasImagensController::class)->except('show')->names('materias.imagens');
            // A COMPANHIA
            Route::resource('a-companhia', CompanhiaController::class)->only(['index', 'update']);
            Route::resource('a-companhia/textos', CompanhiaTextosController::class)->except('show')->names('a-companhia.textos');
            Route::get('a-companhia/textos/{texto}/imagens/clear', [CompanhiaTextosImagensController::class, 'clear'])->name('a-companhia.textos.imagens.clear');
            Route::resource('a-companhia/textos.imagens', CompanhiaTextosImagensController::class)->except('show')->names('a-companhia.textos.imagens');
            Route::get('a-companhia/galeria/imagens/clear', [CompanhiaGaleriaController::class, 'clear'])->name('a-companhia.galeria.clear');
            Route::resource('a-companhia/galeria', CompanhiaGaleriaController::class)->only(['index', 'show', 'store', 'destroy'])->names('a-companhia.galeria');
            // ESPETÁCULOS
            Route::resource('espetaculos', EspetaculosController::class)->except('show');
            Route::resource('espetaculos.videos', EspetaculosVideosController::class)->except('show');
            Route::get('espetaculos/{espetaculo}/imagens/clear', [EspetaculosImagensController::class, 'clear'])->name('espetaculos.imagens.clear');
            Route::resource('espetaculos.imagens', EspetaculosImagensController::class)->only(['index', 'show', 'store', 'destroy']);
            // CURSOS E OFICINAS
            Route::resource('cursos-e-oficinas/textos', CursosTextosController::class)->except('show')->names('cursos-e-oficinas.textos');
            Route::get('cursos-e-oficinas/textos/{texto}/imagens/clear', [CursosTextosImagensController::class, 'clear'])->name('cursos-e-oficinas.textos.imagens.clear');
            Route::resource('cursos-e-oficinas/textos.imagens', CursosTextosImagensController::class)->except('show')->names('cursos-e-oficinas.textos.imagens');
            Route::resource('cursos-e-oficinas/planejamentos', CursosPlanejamentosController::class)->except('show')->names('cursos-e-oficinas.planejamentos');
            Route::get('cursos-e-oficinas/galeria/imagens/clear', [CursosGaleriaController::class, 'clear'])->name('cursos-e-oficinas.galeria.clear');
            Route::resource('cursos-e-oficinas/galeria', CursosGaleriaController::class)->only(['index', 'show', 'store', 'destroy'])->names('cursos-e-oficinas.galeria');
            // CENTRO DE ESTUDOS
            Route::resource('centro-de-estudos/textos', CentroDeEstudosTextosController::class)->except('show')->names('centro-de-estudos.textos');
            Route::get('centro-de-estudos/textos/{texto}/imagens/clear', [CentroDeEstudosTextosImagensController::class, 'clear'])->name('centro-de-estudos.textos.imagens.clear');
            Route::resource('centro-de-estudos/textos.imagens', CentroDeEstudosTextosImagensController::class)->except('show')->names('centro-de-estudos.textos.imagens');
            // CAPTAÇÃO DE PATROCÍNIOS
            Route::resource('captacao-de-patrocinios/textos', CaptacaoDePatrociniosTextosController::class)->except('show')->names('captacao-de-patrocinios.textos');
            Route::get('captacao-de-patrocinios/textos/{texto}/imagens/clear', [CaptacaoDePatrociniosTextosImagensController::class, 'clear'])->name('captacao-de-patrocinios.textos.imagens.clear');
            Route::resource('captacao-de-patrocinios/textos.imagens', CaptacaoDePatrociniosTextosImagensController::class)->except('show')->names('captacao-de-patrocinios.textos.imagens');
            // CONTRATAÇÕES
            Route::resource('contratacoes/textos', ContratacoesTextosController::class)->except('show')->names('contratacoes.textos');
            Route::get('contratacoes/textos/{texto}/imagens/clear', [ContratacoesTextosImagensController::class, 'clear'])->name('contratacoes.textos.imagens.clear');
            Route::resource('contratacoes/textos.imagens', ContratacoesTextosImagensController::class)->except('show')->names('contratacoes.textos.imagens');
        });
    });
});
