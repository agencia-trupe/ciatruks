<?php

use App\Http\Controllers\ACompanhiaController;
use App\Http\Controllers\AgendaController;
use App\Http\Controllers\CaptacaoDePatrociniosController;
use App\Http\Controllers\CentroDeEstudosController;
use App\Http\Controllers\ContacaoDeHistoriasController;
use App\Http\Controllers\ContatoController;
use App\Http\Controllers\ContratacoesController;
use App\Http\Controllers\CriticasEMateriasController;
use App\Http\Controllers\CursosEOficinasController;
use App\Http\Controllers\DepoimentosController;
use App\Http\Controllers\EspetaculosController;
use App\Http\Controllers\GaleriaDeFotosController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NoticiasController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\PremiacoesController;
use App\Http\Controllers\ProjetosEspeciaisController;
use App\Http\Controllers\VideosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('a-companhia', [ACompanhiaController::class, 'index'])->name('a-companhia');
Route::get('agenda', [AgendaController::class, 'index'])->name('agenda');
Route::get('espetaculos', [EspetaculosController::class, 'index'])->name('espetaculos');
Route::get('espetaculos/{slug}', [EspetaculosController::class, 'show'])->name('espetaculos.show');
Route::get('cursos-e-oficinas', [CursosEOficinasController::class, 'index'])->name('cursos-e-oficinas');
Route::get('cursos-e-oficinas/{slug}', [CursosEOficinasController::class, 'showPlanejamento'])->name('cursos-e-oficinas.planejamento');
Route::get('projetos-especiais', [ProjetosEspeciaisController::class, 'index'])->name('projetos-especiais');
Route::get('centro-de-estudos', [CentroDeEstudosController::class, 'index'])->name('centro-de-estudos');
Route::get('captacao-de-patrocinios', [CaptacaoDePatrociniosController::class, 'index'])->name('captacao-de-patrocinios');
Route::get('contratacoes', [ContratacoesController::class, 'index'])->name('contratacoes');
Route::get('galeria-de-fotos', [GaleriaDeFotosController::class, 'index'])->name('galeria-de-fotos');
Route::get('videos', [VideosController::class, 'index'])->name('videos');
Route::get('noticias', [NoticiasController::class, 'index'])->name('noticias');
Route::get('noticias/{slug}', [NoticiasController::class, 'show'])->name('noticias.show');
Route::get('premiacoes', [PremiacoesController::class, 'index'])->name('premiacoes');
Route::get('criticas-e-materias', [CriticasEMateriasController::class, 'index'])->name('criticas-e-materias');
Route::get('contato', [ContatoController::class, 'index'])->name('contato');
Route::post('contato', [ContatoController::class, 'post'])->name('contato.post');
Route::get('depoimentos', [DepoimentosController::class, 'index'])->name('depoimentos');
Route::post('depoimentos', [DepoimentosController::class, 'post'])->name('depoimentos.post');
Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

require __DIR__.'/auth.php';
