--
-- Table structure for table `aceite_de_cookies`
--

DROP TABLE IF EXISTS `aceite_de_cookies`;
CREATE TABLE `aceite_de_cookies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aceite_de_cookies`
--

LOCK TABLES `aceite_de_cookies` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
INSERT INTO `banners` VALUES (5,30,'img-9987_202203021618544r19JOyRZs.JPG','<p>Cia Truks</p>','2022-03-02 19:14:58','2022-03-02 19:18:56'),(6,29,'sonhatorio-4-cia-truks-foto-henrique-sitchin_20220302161742ZVCC5nUCkO.jpg','<p>Cia Truks</p>','2022-03-02 19:17:43','2022-03-02 19:17:43'),(10,28,'a-bruxinha-a-foto-alberto-rocha_2022030510033259GlzqlXc9.JPG','<p>CIA. TRUKS</p>','2022-03-05 13:03:33','2022-03-05 13:03:33'),(31,27,'isto-nao-e-um-cachimbo-1_20220313160915UgazGPClA0.jpg','<p>CIA TRUKS</p>','2022-03-13 19:09:17','2022-03-13 19:09:17'),(32,26,'big-bang-1_20220313161007QYzEcZlvcf.jpg','<p>CIA TRUKS</p>','2022-03-13 19:10:08','2022-03-13 19:10:08'),(37,24,'img-3466_202203131631390HteyKxY1Z.jpg','<p>CIA TRUKS</p>','2022-03-13 19:31:40','2022-03-13 19:31:40'),(38,23,'img-1622_202203131632408Yde3oU7U7.JPG','<p>cia truks</p>','2022-03-13 19:32:43','2022-03-13 19:32:43'),(39,22,'isso-e-coisa-de-crianca-5a-foto-alberto-rocha_20220313163321XlqMq144YJ.jpg','<p>CIA TRUKS</p>','2022-03-13 19:33:22','2022-03-13 19:33:22'),(41,21,'hist-7_20220313163643XmEWfJVGbr.JPG','<p>cia truks</p><p>&nbsp;</p>','2022-03-13 19:36:47','2022-03-13 19:36:47'),(42,20,'por-uma-estrela-10-foto-henrique-sitchin_20220313165020lONJtwUUDE.JPG','<p>CIA TRUKS</p>','2022-03-13 19:50:22','2022-03-13 19:50:22'),(43,25,'sonhatorio-alta-a-foto-alberto-greiber_20220313165052iBhAT4DO6m.jpg','<p>CIA TRUKS</p>','2022-03-13 19:50:54','2022-03-13 19:50:54'),(44,19,'sonhatorio-2-cia-truks-foto-henrique-sitchin_20220313165213F7d7gkpJEl.jpg','<p>CIA TRUKS</p>','2022-03-13 19:52:14','2022-03-13 19:52:14'),(45,18,'vovo-foto-1_20220313165351rvnYbkwH4e.jpg','<p>CIA TRUKS</p>','2022-03-13 19:53:52','2022-03-13 19:53:52'),(47,17,'zooilogico-3-foto-pietro-feliciano_20220313165531pjZTxr7zf8.jpg','<p>CIA TRUKS</p>','2022-03-13 19:55:32','2022-03-13 19:55:32'),(48,16,'por-uma-estrela-2-foto-henrique-sitchin_20220313165600uTIa8xjYvV.JPG','<p>CIA TRUKS</p>','2022-03-13 19:56:01','2022-03-13 19:56:01'),(49,14,'o-senhor-dos-sonhos-foto-thiago-cardinalli_20220313165632e2lKwOor6o.jpg','<p>CIA TRUKS</p>','2022-03-13 19:56:34','2022-03-13 19:56:34'),(50,13,'isso-e-coisa-de-crianca-1a-foto-alberto-rocha_202203131657142pA6tUhDkZ.jpg','<p>CIA TRUKS</p>','2022-03-13 19:57:15','2022-03-13 19:57:15'),(51,12,'historia-de-bar-15_202203131658167YTDOyQQV0.jpg','<p>CIA TRUKS</p>','2022-03-13 19:58:18','2022-03-13 19:58:18'),(52,10,'ltima-noticia-1-foto-henrique-sitchin_20220313165850PpmFcXbdIr.JPG','<p>CIA TRUKS</p>','2022-03-13 19:58:51','2022-03-13 19:58:51'),(53,15,'expedicao-pacifico-5-foto-alberto-rocha_20220313165931yPmQKoomQu.jpg','<p>CIA TRUKS</p>','2022-03-13 19:59:32','2022-03-13 19:59:32'),(54,9,'construtorio-foto-alberto-greiber-2_20220313170008UctKqoTKai.jpg','<p>CIA TRUKS</p>','2022-03-13 20:00:09','2022-03-13 20:00:09'),(55,8,'espectaculo-cidade-azul-cia-truks_20220313170031l2moQwMcmU.jpg','<p>CIA TRUKS</p>','2022-03-13 20:00:33','2022-03-13 20:00:33'),(57,7,'big-bang-2-foto-paulo-brasil_20220313170102oS9chmkTOS.jpg','<p>CIA TRUKS</p>','2022-03-13 20:01:03','2022-03-13 20:01:03'),(58,6,'acampatorio-2-foto-alberto-rocha-1_202203131701548ukYOq7GRO.jpg','<p>CIA TRUKS</p>','2022-03-13 20:01:55','2022-03-13 20:01:55'),(59,5,'a-bruxinha-foto-alberto-rocha-a_20220313170229QXXzbYPOW6.jpg','<p>CIA TRUKS</p>','2022-03-13 20:02:30','2022-03-13 20:02:30'),(60,11,'isto-nao-e-um-cachimbo-7_20220313170300A15LIJzmed.jpg','<p>CIA TRUKS</p>','2022-03-13 20:03:02','2022-03-13 20:03:02'),(62,4,'isto-nao-e-um-cachimbo-7_20220313170518RqX29ObhmX.jpg','<p>CIA TRUKS</p>','2022-03-13 20:05:19','2022-03-13 20:05:19'),(63,3,'espetaculo-cidade-azul-foto-henrique-sitchin_202203131709351IVRJ94pAt.jpg','<p>CIA TRUKS</p>','2022-03-13 20:09:36','2022-03-13 20:09:36'),(64,2,'o-senhor-dos-sonhos-foto-3-thiago-cardinali_20220313171040AU6uCrKwM8.jpg','<p>CIA TRUKS</p>','2022-03-13 20:10:42','2022-03-13 20:10:42'),(65,1,'vovo-3-foto-henrique-sitchin_20220313171151HAgOf8OrAu.jpg','<p>CIA TRUKS</p>','2022-03-13 20:11:53','2022-03-13 20:11:53');
UNLOCK TABLES;

--
-- Table structure for table `captacao_de_patrocinios_textos`
--

DROP TABLE IF EXISTS `captacao_de_patrocinios_textos`;
CREATE TABLE `captacao_de_patrocinios_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `captacao_de_patrocinios_textos`
--

LOCK TABLES `captacao_de_patrocinios_textos` WRITE;
INSERT INTO `captacao_de_patrocinios_textos` VALUES (5,0,'<h2>A Cia. Truks dispõe de diversos projetos já aprovados para a captação de patrocínios. São eles:</h2><p>&nbsp;</p><p>(lista que será editada depois)</p><p>&nbsp;</p><p>Para saber mais, fale conosco: <a href=\"https://www.trupe.net/previa-ciatruks/contato\">contato@truks.com.br</a></p>','2022-03-02 18:53:26','2022-03-04 21:02:03');
UNLOCK TABLES;

--
-- Table structure for table `captacao_de_patrocinios_textos_imagens`
--

DROP TABLE IF EXISTS `captacao_de_patrocinios_textos_imagens`;
CREATE TABLE `captacao_de_patrocinios_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacao_historias_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `contacao_historias_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `captacao_de_patrocinios_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `captacao_de_patrocinios_textos_imagens`
--

LOCK TABLES `captacao_de_patrocinios_textos_imagens` WRITE;
INSERT INTO `captacao_de_patrocinios_textos_imagens` VALUES (5,5,0,'big-bang-rc-18_20220302175915HuTHpaG5XH.JPG','2022-03-02 20:59:15','2022-03-02 20:59:15');
UNLOCK TABLES;

--
-- Table structure for table `centro_de_estudos_textos`
--

DROP TABLE IF EXISTS `centro_de_estudos_textos`;
CREATE TABLE `centro_de_estudos_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `centro_de_estudos_textos`
--

LOCK TABLES `centro_de_estudos_textos` WRITE;
INSERT INTO `centro_de_estudos_textos` VALUES (5,0,'<p>O&nbsp;<strong>Centro de Estudos e Práticas do Teatro de Animação</strong>&nbsp;esteve ativo na Biblioteca Municipal Monteiro Lobato, em São Paulo, de Janeiro de 2002 até o final de 2012. Foi um profícuo espaço de referências para o teatro feito por meio de bonecos, objetos e formas animadas, como também um polo de experimentos e buscas por novas e renovadas linguagens teatrais. O espaço dividiu-se entre a realização de uma intensa programação artística e um permanente espaço de formação profissional para todos os interessados, além de se fazer primordial para o fomento do trabalho da Cia. Truks que, a partir dos estudos, reflexões e experiências realizadas ali, estreou 8 novas montagens, desde o início destas ações. Não somente, o</p><p>&nbsp;</p><p>O Centro foi responsável pela criação de quase duas dezenas de novos grupos e espetáculos teatrais, e de pelo menos 100 novos profissionais. A partir da realização de inúmeras oficinas, e da troca de informações, experiências e procedimentos com vários grupos que ali operaram, o Centro trabalhou pela melhor formação dos profissionais do Teatro de Animação de São Paulo, oferecendo-lhes um amplo leque de possibilidades de ações, e, sobretudo, de instrumentalização para este fim. Ao mesmo tempo, também presenteou a comunidade com a sua rica programação artística que, aberta e gratuita, realizou uma ininterrupta agenda de apresentações de espetáculos para adultos e crianças, além de festivais de pequenos quadros e experiências cênicas. Promoveu, ainda, a realização de uma Oficina Permanente de Teatro de Animação, que trabalhou com afinco pela busca de linguagens cênicas inovadoras, e próprias de seus criadores. A Cia. Truks busca parcerias para a retomada deste importante trabalho!</p>','2022-03-02 18:50:19','2022-03-02 18:50:19');
UNLOCK TABLES;

--
-- Table structure for table `centro_de_estudos_textos_imagens`
--

DROP TABLE IF EXISTS `centro_de_estudos_textos_imagens`;
CREATE TABLE `centro_de_estudos_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `centro_de_estudos_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `centro_de_estudos_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `centro_de_estudos_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `centro_de_estudos_textos_imagens`
--

LOCK TABLES `centro_de_estudos_textos_imagens` WRITE;
INSERT INTO `centro_de_estudos_textos_imagens` VALUES (10,5,11,'oficina-da-cia-truks-2_20220313152424l4W1FaQLyN.jpg','2022-03-13 18:24:25','2022-03-13 18:24:25'),(12,5,7,'molinete-1_20220313162113oe7d35QIn5.jpg','2022-03-13 19:21:13','2022-03-13 19:21:13'),(13,5,8,'nucleo-de-desenvolvimento-de-novas-linguagens-030_20220313162248ekcbC9Imdr.jpg','2022-03-13 19:22:49','2022-03-13 19:22:49'),(15,5,4,'centro-de-estudos-436_202203131625519BzMXTKhPs.jpg','2022-03-13 19:25:52','2022-03-13 19:25:52'),(16,5,3,'centro-de-estudos-107_20220313162628F4RzpkDmHc.jpg','2022-03-13 19:26:28','2022-03-13 19:26:28'),(17,5,6,'dsc03656_20220318135406t6hxlWCfvS.JPG','2022-03-18 16:54:06','2022-03-18 16:54:06'),(18,5,1,'popol-vuh-1_202203181354143RbjUY7J5s.jpg','2022-03-18 16:54:14','2022-03-18 16:54:14'),(19,5,2,'oficina-teatro-de-objetos_202203181354260pt1Z0XDPL.jpg','2022-03-18 16:54:26','2022-03-18 16:54:26'),(20,5,10,'oficina-com-nadam-guerra-rj_20220318135437sMRUvIYt3l.JPG','2022-03-18 16:54:37','2022-03-18 16:54:37'),(21,5,9,'oficina-com-ana-liberman-chile_20220318135446TOANTjfuYf.JPG','2022-03-18 16:54:46','2022-03-18 16:54:46');
UNLOCK TABLES;

--
-- Table structure for table `companhia`
--

DROP TABLE IF EXISTS `companhia`;
CREATE TABLE `companhia` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia`
--

LOCK TABLES `companhia` WRITE;
INSERT INTO `companhia` VALUES (1,'vqrtENCYZOc',NULL,'2022-03-05 15:05:10');
UNLOCK TABLES;

--
-- Table structure for table `companhia_galeria`
--

DROP TABLE IF EXISTS `companhia_galeria`;
CREATE TABLE `companhia_galeria` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_galeria`
--

LOCK TABLES `companhia_galeria` WRITE;
INSERT INTO `companhia_galeria` VALUES (14,0,'a-bruxinha-a-foto-alberto-rocha_20220302175026JtVq2MZE26.JPG','2022-03-02 20:50:26','2022-03-02 20:50:26'),(15,0,'cidade-azul-3-foto-henrique-sitchin_20220302175026HuOm8MrLLr.jpg','2022-03-02 20:50:26','2022-03-02 20:50:26'),(16,0,'isto-nao-e-um-cachimbo-1_20220302175026qQ4bdm7Ijv.jpg','2022-03-02 20:50:27','2022-03-02 20:50:27'),(17,0,'o-senhor-dos-sonhos-3-foto-henrique-sitchin_202203021750266rXtWw4zGa.jpg','2022-03-02 20:50:27','2022-03-02 20:50:27'),(18,0,'e-se-as-historias-fossem-diferentes-3-foto-pietro-feliciano_20220302175027tcn0muAuOy.JPG','2022-03-02 20:50:28','2022-03-02 20:50:28'),(19,0,'o-senhor-dos-sonhos-2-foto-henrique-sitchin_20220302175027OiyvDGtbDM.jpg','2022-03-02 20:50:28','2022-03-02 20:50:28');
UNLOCK TABLES;

--
-- Table structure for table `companhia_textos`
--

DROP TABLE IF EXISTS `companhia_textos`;
CREATE TABLE `companhia_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_textos`
--

LOCK TABLES `companhia_textos` WRITE;
INSERT INTO `companhia_textos` VALUES (6,0,'<p><strong>A CIA TRUKS</strong></p><p>&nbsp;</p><p>A Cia. Truks foi criada em 1990, e desde então apresenta seus espetáculos em teatros, escolas, empresas, instituições ou espaços alternativos de todo o Brasil. Não somente, já se apresentou em mais de uma dezena de países, representando o Brasil em diversos festivais internacionais. O grupo é referência nacional na arte do Teatro de Animação, bem como um dos principais expoentes do bom teatro para crianças no Brasil. Recebeu mais de 40 indicações, entre prêmios e menções honrosas. Truks, ainda, coordenou o CENTRO DE ESTUDOS E PRÁTICAS DO TEATRO DE ANIMAÇÃO DE SÃO PAULO, espaço de referências desta linguagem artística, em projeto da Prefeitura de São Paulo, entre 2002 e 2012.</p><p>&nbsp;</p><p>O grupo notabilizou-se, ao longo dos anos, por seu extremo profissionalismo, marcado por cada uma de suas mais de 9.000 apresentações já realizadas, além de uma constante e obsessiva busca pela perfeição técnica, pela vida e máxima expressividade de seus bonecos. Dotou o seu trabalho e pesquisa cênica de uma característica singular, ao desenvolver uma técnica particular de animação de figuras, inspirada na centenária arte japonesa do Bunraku, em que três atores, simultaneamente, animam o mesmo boneco, conferindo-lhe movimentos humanos precisos, que encantam e surpreendem plateias de todas as idades. Não somente, desenvolveu uma linguagem própria de ressignificação e animação de objetos do cotidiano, que se transformam em divertidas e poéticas personagens de espetáculos extremamente inteligentes e criativos.</p><p>&nbsp;</p><p>Em suas peças, a cia. desenvolve dramaturgias ricas que envolvem e encantam pais e filhos. De forma inteligente e sensível, por meio de histórias bonitas e profundas, trata de temas importantes, referentes às vidas de todos nós, ao tempo em que oferece, às crianças, visões de mundo instigantes, inovadoras, e repletas de fantasia e poesia.</p>','2022-03-02 17:21:27','2022-03-02 17:21:27');
UNLOCK TABLES;

--
-- Table structure for table `companhia_textos_imagens`
--

DROP TABLE IF EXISTS `companhia_textos_imagens`;
CREATE TABLE `companhia_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companhia_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `companhia_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `companhia_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companhia_textos_imagens`
--

LOCK TABLES `companhia_textos_imagens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
CREATE TABLE `configuracoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_g` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigo_gtm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pixel_facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinify_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
INSERT INTO `configuracoes` VALUES (1,'Cia Truks - Teatro de Bonecos','Companhia de Teatro de Bonecos internacionalmente reconhecida, apresenta espetáculos autorais infantis e adultos, realiza formação de profissionais em manipulação de bonecos e é referência do tema no Brasil.','teatro de bonecos, manipulação de bonecos, companhia de teatro, espetáculos infantis e adultos,','img-compartilhamento-ciatruks_20220331153413ZpgWB9t8c5.png',NULL,NULL,NULL,NULL,'0q62ghR9brVkCvLN3CytcdNMCvLBgxRs',NULL,'2022-03-31 18:34:13');
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
CREATE TABLE `contatos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
INSERT INTO `contatos` VALUES (1,'contato@truks.com.br','11 3865 8019','11 99952 8553','https://www.instagram.com/ciatruks/','',NULL,'2022-03-01 00:17:02');
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
CREATE TABLE `contatos_recebidos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `contratacoes_textos`
--

DROP TABLE IF EXISTS `contratacoes_textos`;
CREATE TABLE `contratacoes_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contratacoes_textos`
--

LOCK TABLES `contratacoes_textos` WRITE;
INSERT INTO `contratacoes_textos` VALUES (3,0,'<h2>AS CONTRATAÇÕES DOS ESPETÁCULOS DEVEM SER FEITAS PELO TELEFONE (WHATSAPP): &nbsp;(11) 99952.8553, OU PELO E-MAIL: <a href=\"https://www.trupe.net/previa-ciatruks/contato\">contato@truks.com.br</a><br>&nbsp;</h2><p><strong>• As apresentações dos espetáculos da Cia. Truks poderão ser agendadas nos dias e horários mais convenientes para os interessados, mesmo não estando a peça desejada em cartaz ou temporada convencional nos teatros. Os espetáculos do grupo são chamados de \"peças de repertório\" e são, todos, ensaiados e apresentados regularmente, ao longo de todo o ano.</strong><br><br><strong>• Os espetáculos da Cia. Truks podem ser apresentados em qualquer tipo de espaço físico, como salas de aula, salões, quadras de esportes ou afins, desde que ao abrigo do sol, para maior conforto do público. As peças podem, portanto, ser encenadas dentro da escola ou de instituições, clubes, empresas e etc..&nbsp;</strong><br><br><strong>• As montagens dos cenários dos espetáculos do grupo necessitam, em média, de áreas livres de no mínimo 6m. de largura por 4m. de profundidade.</strong><br><br><strong>• Não há limite mínimo de público por sessão, e o limite máximo deverá ser estipulado em função do espaço físico disponível para a boa acomodação dos espectadores.</strong><br><br><strong>• A duração média dos espetáculos é de 50 minutos. O tempo médio de montagem dos cenários é de 2h00, e a desmontagem é feita em 40 minutos.</strong><br><br><strong>• A Cia. dispõe de toda a infraestrutura necessária para a realização dos espetáculos: cenários, aparelhagem de som, iluminação teatral e outros, necessitando, nos locais de apresentação, apenas de uma entrada de energia de 110V. e outra de 220V.</strong></p><p>&nbsp;</p><p><strong>SE VOCÊ QUER SER UM REPRESENTANTE DA CIA TRUKS, VAMOS CONVERSAR!&nbsp;</strong></p><p><strong>NOS ENVIE UM E-MAIL NO ENDEREÇO ABAIXO:</strong><br><a href=\"https://www.trupe.net/previa-ciatruks/contato\"><strong>contato@truks.com.br</strong></a></p><p>&nbsp;</p>','2022-03-02 18:56:06','2022-03-18 16:57:13');
UNLOCK TABLES;

--
-- Table structure for table `contratacoes_textos_imagens`
--

DROP TABLE IF EXISTS `contratacoes_textos_imagens`;
CREATE TABLE `contratacoes_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contratacoes_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `contratacoes_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `contratacoes_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contratacoes_textos_imagens`
--

LOCK TABLES `contratacoes_textos_imagens` WRITE;
INSERT INTO `contratacoes_textos_imagens` VALUES (5,3,0,'img-7878_20220318135825ogS2KHbsOv.JPG','2022-03-18 16:58:25','2022-03-18 16:58:25'),(6,3,0,'img-8146_20220318135834pORgLGnHh2.JPG','2022-03-18 16:58:34','2022-03-18 16:58:34'),(7,3,0,'img-20160825-144606723_20220318135843C8PfA3GjV5.jpg','2022-03-18 16:58:44','2022-03-18 16:58:44'),(8,3,0,'img-20160903-wa0035_20220318135852BjsOfF8Xk1.jpg','2022-03-18 16:58:53','2022-03-18 16:58:53'),(9,3,0,'img-20160604-173904721-hdr_20220318135908Lxm6NmbRgh.jpg','2022-03-18 16:59:09','2022-03-18 16:59:09');
UNLOCK TABLES;

--
-- Table structure for table `cursos_galeria`
--

DROP TABLE IF EXISTS `cursos_galeria`;
CREATE TABLE `cursos_galeria` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_galeria`
--

LOCK TABLES `cursos_galeria` WRITE;
INSERT INTO `cursos_galeria` VALUES (16,1,'dsc00533_20220302175420b1QQOEOqGv.JPG','2022-03-02 20:54:20','2022-03-02 20:54:20'),(17,13,'oficina-com-a-cia-truks-foto-aguinaldo-rodrigues_20220302175420PwVsbFPzHt.jpg','2022-03-02 20:54:21','2022-03-02 20:54:21'),(18,3,'img-3311_20220302175420U8fW6Bh0XF.JPG','2022-03-02 20:54:21','2022-03-02 20:54:21'),(19,7,'oficina-com-a-cia-truks-foto-2-aguinaldo-rodrigues_20220302175421gK12YuKadm.jpg','2022-03-02 20:54:22','2022-03-02 20:54:22'),(20,12,'oficina-com-a-cia-truks-2-foto-aguinaldo-rodrigues_20220302175421WbMvY4nlMO.jpg','2022-03-02 20:54:22','2022-03-02 20:54:22'),(21,17,'img-20160903-152548917_20220302175420ZVjKi4DN0A.jpg','2022-03-02 20:54:22','2022-03-02 20:54:22'),(22,18,'img-20160903-164526961_20220302175420LPTtp6yOHk.jpg','2022-03-02 20:54:23','2022-03-02 20:54:23'),(23,19,'img-20160903-164532566_20220302175420VxOb2bs17N.jpg','2022-03-02 20:54:23','2022-03-02 20:54:23'),(24,15,'oficina-com-henrique-2_20220302175422s7xyA5JqHG.jpg','2022-03-02 20:54:23','2022-03-02 20:54:23'),(25,11,'oficina-com-a-cia-truks_20220302175422OT1rfOrrta.JPG','2022-03-02 20:54:23','2022-03-02 20:54:23'),(26,16,'oficina-da-cia-truks-2_20220302175423LT0zm7okbl.jpg','2022-03-02 20:54:24','2022-03-02 20:54:24'),(28,4,'oficina-de-animacao-036_20220302175424Vwtrr4dA0C.jpg','2022-03-02 20:54:24','2022-03-02 20:54:24'),(29,21,'oficina-invencao-de-historias-com-objetos-cia-truks-3_20220302175424klhDl0m9LY.JPG','2022-03-02 20:54:25','2022-03-02 20:54:25'),(30,20,'oficina-invencao-de-historias-com-objetos-cia-truks-2_202203021754240p1tuLOFtW.JPG','2022-03-02 20:54:25','2022-03-02 20:54:25'),(31,2,'oficina-permanente-de-teatro-de-animacao-1_20220302175425YpXpkk2dOi.JPG','2022-03-02 20:54:26','2022-03-02 20:54:26'),(32,22,'oficina-o-teatro-de-objetos-da-cia-truks-1_20220302175424tPgVKxNzm8.JPG','2022-03-02 20:54:26','2022-03-02 20:54:26'),(33,9,'oficina-com-henrique_20220302175423DkYDUMqiyD.png','2022-03-02 20:54:26','2022-03-02 20:54:26'),(34,8,'oficina-permanente-de-teatro-de-animacao-2_20220302175426peUeKfbIxd.JPG','2022-03-02 20:54:26','2022-03-02 20:54:26'),(35,5,'oficina-de-animacao-028_20220302175424IyYoRhGE8f.jpg','2022-03-02 20:54:26','2022-03-02 20:54:26'),(36,10,'oficina-truks-2_20220302175426wirEBBYebA.jpg','2022-03-02 20:54:26','2022-03-02 20:54:26'),(37,14,'oficina-o-teatro-de-objetos-da-cia-truks_20220302175425OqpbzXHT0h.jpg','2022-03-02 20:54:27','2022-03-02 20:54:27'),(38,6,'oficina-permanente-de-teatro-de-animacao-049_20220302175426h4Zct9G8De.jpg','2022-03-02 20:54:28','2022-03-02 20:54:28');
UNLOCK TABLES;

--
-- Table structure for table `cursos_planejamentos`
--

DROP TABLE IF EXISTS `cursos_planejamentos`;
CREATE TABLE `cursos_planejamentos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_modulos` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_planejamentos`
--

LOCK TABLES `cursos_planejamentos` WRITE;
INSERT INTO `cursos_planejamentos` VALUES (4,0,'oficina-dramaturgia-para-o-teatro-de-animacao','OFICINA – DRAMATURGIA PARA O TEATRO DE ANIMAÇÃO','<p>O Teatro de Animação é essencialmente caracterizado pela presença de um objeto “intermediário” entre o ator e a plateia. Aqui, o boneco, ou uma “forma animada”, será instrumento de comunicação do ator com o público.</p><p>Definimos este como “o teatro da matéria”. A “coisa” irá substituir o ator de carne e osso em seu discurso dramático. Poderá ser algo que recebe vida, através da ação do ator, ou então algo elevado à condição de forte símbolo na cena teatral.</p><p>Nessa característica específica, e extremamente interessante ao universo das artes cênicas, que nos centramos ao longo da oficina. Não se trata, portanto, de um curso que segue os caminhos convencionais do estudo da dramaturgia universal, mas, sim, se concentra nas possibilidades expressivas (e desafiadoras) das imagens, dos objetos, das figuras, dos bonecos e dos materiais, como agentes da concepção dramatúrgica.</p><p>Almejamos, assim, oferecer aos participantes não somente um amplo entendimento das possibilidades expressivas do Teatro de Animação enquanto linguagem cênica específica, extremamente rica em imagens e significados, como, sobretudo, aparelhá-los para um início de processo de criação de um espetáculo a partir do estudo aprofundado das ricas possibilidades de desenvolvimento de uma dramaturgia particular a essa forma de arte.</p>','<h2><strong>A OFICINA SERÁ DIVIDIDA EM 3 MÓDULOS:</strong></h2><p>1) ESTUDOS DAS CARACTERÍSTICAS ESPECÍFICAS DO TEATRO DE ANIMAÇÃO</p><p>- Características que definem o Teatro de Animação tal qual o teatro da matéria – da coisa, do objeto, da figura, da forma e do boneco animado.<br>- Características que diferenciam o Teatro de Animação do “Teatro de Atores de Carne e Osso”.<br>- Possibilidades cênicas do Teatro de Animação – Técnicas e formas de expressão.<br>- O Teatro de Animação enquanto Poesia da Matéria.&nbsp;<br>- O Teatro de Animação enquanto proposta de vanguarda nas artes cênicas.</p><p>2) EXPERIMENTOS PARA A CRIAÇÃO DA IMAGEM DISPARADORA 1 – DA IDEIA PARA A MATÉRIA E DA MATÉRIA PARA A IDEIA.</p><p>- Exercícios diversos de aproximação do Teatro de Animação com o universo da poesia, como instrumento da criação de ideias e imagens “disparadoras” do processo de criação. Como temas ou simples e desconexas ideias podem se transformar em roteiros completos de um espetáculo teatral.<br>- Exercícios de investigação de coisas e materiais. A construção de metáforas visuais com imagens, objetos e ou experimentações. Como uma imagem pode ser a disparadora do desenvolvimento de um roteiro teatral.<br>- Estudos da Dramaturgia Convencional: a construção do personagem e as estruturas da narrativa – a elaboração dos conflitos e soluções cênicas.</p><p>3) DESENVOLVIMENTO DE PROJETOS PARTICULARES DE TRABALHO – A CONSTRUÇÃO E APROPRIAÇÃO DE UMA POÉTICA VERDADEIRA AO ARTISTA CRIADOR.</p>','2022-03-02 19:52:11','2022-03-02 19:52:11'),(5,0,'oficina-tecnicas-de-animacao-de-bonecos-formas-e-objetos','OFICINA TÉCNICAS DE ANIMAÇÃO DE BONECOS, FORMAS E OBJETOS','<p>Nesta oficina, os participantes iniciarão o estudo sobre os processos de animação de bonecos, formas e objetos, e a consequente transferência de energia, vida e interpretação do ator animador para as suas figuras.&nbsp;</p><p>Serão estudadas diversas técnicas e recursos de treinamento para se atingir o estado de perfeição e de “quase magia” que se almeja atribuir aos bonecos e formas animadas. Não somente, serão trabalhadas as dificuldades decorrentes da união do trabalho técnico de animação, com a exigência de preparo emocional e intenção teatral do ator que opera a figura.&nbsp;</p>','<p><strong>ENTRE OS EXERCÍCIOS PRATICADOS TEREMOS:</strong><br><br>- Investigação do poder expressivo das mãos<br>- Investigação da transferência de energia vital, do cérebro e do coração, para as mãos do ator<br>- Construção da vida no corpo externo ao corpo do ator / transferência de energia para o objeto<br>- A criação do foco propulsor da ação dramática<br>- O objeto em posição de “descanso”, ou “nulidade cênica” e em “posição expressiva”<br>- A criação e o direcionamento do “olhar da coisa”<br>- Exercícios de movimentação do objeto em coerência físico mecânica<br>- Precisão e síntese de gestos<br>- Possibilidades do ator manipulador enquanto ser em estado de anulação ou de participação na cena.<br>- Animação de diversas técnicas de bonecos<br>- O envolvimento emocional do ator com o exercício da animação.</p>','2022-03-02 19:53:54','2022-03-02 19:53:54');
UNLOCK TABLES;

--
-- Table structure for table `cursos_textos`
--

DROP TABLE IF EXISTS `cursos_textos`;
CREATE TABLE `cursos_textos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_textos`
--

LOCK TABLES `cursos_textos` WRITE;
INSERT INTO `cursos_textos` VALUES (4,0,'<p>A Cia Truks ministra cursos e oficinas sobre a arte do Teatro de Animação em todo o país. Não somente, presta assessoria a grupos e profissionais, também de todo o Brasil, em suas montagens e ou pesquisas cênicas.</p><p>As oficinas têm como objetivo principal oferecer aos participantes não somente o entendimento teórico, como, sobretudo, promover a prática de diversos conceitos particulares desta forma de arte. Assim, são estudadas e experimentadas, entre outros temas, a criação e o desenvolvimento de uma dramaturgia específica para o Teatro de Animação, as infinitas possibilidades expressivas dos bonecos, formas e objetos, a partir do instante em que \"recebem a vida\" através da ação de seus \"atores animadores\", ou que são alçados à condição de símbolos fortes em cena.</p><p>Os cursos podem ser ministrados em todo o país, para instituições, escolas, empresas ou grupos de interessados, observando-se o melhor formato para cada iniciativa, conforme as possibilidades e ou necessidades dos participantes.</p><p>Para saber mais, fale conosco! <a href=\"https://www.trupe.net/previa-ciatruks/contato\">truks@uol.com.br</a><br>&nbsp;</p>','2022-03-02 18:44:08','2022-03-02 18:44:37');
UNLOCK TABLES;

--
-- Table structure for table `cursos_textos_imagens`
--

DROP TABLE IF EXISTS `cursos_textos_imagens`;
CREATE TABLE `cursos_textos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cursos_textos_imagens_texto_id_foreign` (`texto_id`),
  CONSTRAINT `cursos_textos_imagens_texto_id_foreign` FOREIGN KEY (`texto_id`) REFERENCES `cursos_textos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos_textos_imagens`
--

LOCK TABLES `cursos_textos_imagens` WRITE;
INSERT INTO `cursos_textos_imagens` VALUES (28,4,0,'dsc00533_20220302175406pHHfnMa7zL.JPG','2022-03-02 20:54:06','2022-03-02 20:54:06'),(29,4,0,'oficina-com-a-cia-truks-foto-aguinaldo-rodrigues_20220302175406q6ByLHB8s3.jpg','2022-03-02 20:54:06','2022-03-02 20:54:06'),(30,4,0,'oficina-o-teatro-de-objetos-da-cia-truks-1_20220302175406YuOkueMYao.JPG','2022-03-02 20:54:06','2022-03-02 20:54:06'),(31,4,0,'oficina-com-henrique_202203021754065N26dvyMuP.png','2022-03-02 20:54:06','2022-03-02 20:54:06'),(32,4,0,'oficina-de-animacao-028_20220302175406P6KGiRVldg.jpg','2022-03-02 20:54:07','2022-03-02 20:54:07');
UNLOCK TABLES;

--
-- Table structure for table `espetaculos`
--

DROP TABLE IF EXISTS `espetaculos`;
CREATE TABLE `espetaculos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci,
  `ficha_tecnica` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos`
--

LOCK TABLES `espetaculos` WRITE;
INSERT INTO `espetaculos` VALUES (9,1,'a-bruxinha-foto-alberto-rocha-a_202203131440597OhR2VwK09.jpg','a-bruxinha','A BRUXINHA','Teatro para todas as idades','<p>Baseada na obra da premiadíssima autora e ilustradora EVA FURNARI, “A BRUXINHA” foi o primeiro trabalho da Cia. Truks. A peça estreou em 1991 e foi, sem dúvida, um dos grandes fenômenos do teatro para crianças em nosso país, não apenas pela grande repercussão que teve em toda a mídia, ao apresentar novas formas de animação de bonecos nunca antes vistas em São Paulo, mas sobretudo pelo encantamento que provocava (e continua provocando) no público. A montagem, doce, cativante e delicada, é um convite à imaginação e ao desenvolvimento da criatividade. A Bruxinha deixa os livros para ganhar corpo - e alma - de boneco, e levar para os palcos toda a sua graça, simpatia e inusitado senso de humor, vivendo suas aventurinhas, manias e caprichos. A personagem, uma simpatissíssima bruxa - com varinha de fada, nos oferece uma das mais lindas magias: transforma o nosso mundo em um lugar mais divertido e amoroso onde estar e viver.</p>','<h2>A IMPRENSA COMENTA</h2><p>&nbsp;</p><p>“A técnica é apreciadíssima por crianças e adultos, mesmo sob olhar diverso. As primeiras se divertem com a encenação, que lembra suas próprias brincadeiras. Os segundos se regalam com a técnica, ao mesmo tempo em que descobrem que podem partilhar o entretenimento com a plateia mais jovem”.</p><h4><strong>LÚCIA SERRONE – JORNAL DO BRASIL – RJ - 30/08/1996</strong></h4><p>&nbsp;</p><p>“A BRUXINHA é um teatro de bonecos eficiente. E bonito. As crianças pequenas ficam concentradas e encantadas ao assistir ao espetáculo... Os bonecos dão um show de movimentos. Um telescópio e um planeta voadores dançam no ar, sob uma luz brilhante. Os manipuladores sustentam o espetáculo e, às vezes, contracenam de forma divertida com os bonecos...”.</p><h4><strong>MÔNICA RODRIGUES DA COSTA – FOLHA DE SÃO PAULO - 27/02/1994</strong></h4><p>&nbsp;</p><p>“Acredite: sem utilizar uma só palavra, consegue prender a atenção das crianças durante todo o tempo. A história, narrada através de gestos e música mostra a Bruxinha tentando evitar que sua varinha caia em mãos erradas... O resultado é belíssimo. A certa altura dois pares de sapatos dançam um charmoso tango. Não é à toa que a criançada se encanta com tamanha fantasia”.</p><h4><strong>ANA CLÁUDIA FONSECA – REVISTA VEJA SP - 23/02/1994</strong></h4><p>&nbsp;</p>','<h2>FICHA TÉCNICA</h2><h4>Criação:</h4><p>EVA FURNARI,<br>CLÁUDIO SALTINI,<br>EDUARDO AMOS,<br>HENRIQUE SITCHIN e<br>VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>Direção Original:</h4><p>EDUARDO AMOS</p><p>&nbsp;</p><h4>Criação e confecção dos bonecos:</h4><p>VERÔNICA GERCHMAN,<br>EVA FURNARI,<br>CLÁUDIO SALTINI,<br>EDUARDO AMOS e<br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:</h4><p>AGUINALDO RODRIGUES<br>BIANCA MUNIZ<br>DRIELY PALÁCIO<br>JOÃO SANTIAGO<br>THAÍS ROSSI</p><p>&nbsp;</p><h4>Operação de Som e Iluminação:</h4><p>THAÍS ROSSI</p><p>&nbsp;</p><h4>Direção (a partir de 2010):</h4><p>HENRIQUE SITCHIN</p>','2022-03-02 17:25:55','2022-03-13 17:41:01'),(10,2,'o-senhor-dos-sonhos-foto-thiago-cardinalli_20220313144213py5kVgM5UI.jpg','o-senhor-dos-sonhos','O SENHOR DOS SONHOS','Teatro para todas as idades','<p>A poética peça conta a bonita história do menino Lucas que, totalmente mergulhado em seus sonhos, fantasias e invenções mirabolantes, procura, sem muito sucesso, adaptar-se às regras sociais. Em momentos lúdicos e engraçados, ou em outros de delicada poesia, revelam-se em cena toda a riqueza e sabedoria do universo infantil. O garoto, simbolizando todas as crianças, materializa o anseio delas pela liberdade de viverem os seus sonhos, no entanto, com a tão necessária compreensão, ajuda e cumplicidade dos adultos. Por fim, revela-se que o espetáculo traz à tona, na verdade, as memórias do velho Senhor Lucas, que acabaria por tornar-se um escritor de grande sucesso.</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"Alguém, por favor, diz para o Lucas que não é ele que está errado. Lucas, quem está errado é o mundo! Continue sonhando, porque sonhadores são essenciais nesta vida tão sem brilho e sem cores. Obrigada Lucas por nos mostrar, em forma de risadas, pequenos sustos e lágrimas, que os sonhadores não são uma raça em extinção\".</p><h4><strong>PATRÍCIA VIALE - 11/10/2016</strong></h4><p>&nbsp;</p><p>\"...Ganhei o Domingo quando vi o teatro de bonecos apresentando \"O Senhor dos Sonhos\", na Praça João Pessoa, em Araçatuba. Foi o momento poético do meu Domingo. Enquanto crianças se divertiam e riam com as pantomimas do garoto Lucas, os adultos refletiam e choravam as limitações humanas... Por fim, todos se emocionaram... Se você, leitor, consegue levitar um pouco do chão, admira-se com a beleza de uma flor, e é enluarado, é da tribo do Lucas...\"<strong>&nbsp;</strong></p><h4><strong>HÉLIO CONSOLARO- FOLHA DA REGIÃO - ARAÇATUBA - 15/02/2005</strong></h4><p>&nbsp;</p><p>\" A platéia vai do riso às lágrimas. Várias são as manifestações de surpresa, conivência ou estupefação. Não é uma super produção de Disney ou Spielberg. Em \"O Senhor dos Sonhos\", a Cia. Truks, de maneira simples e inteligente, consegue mexer com o imaginário infantil, tratar de questões sociais e educacionais muito importantes e, ao mesmo tempo, tão bem agradar aos adultos...\"</p><h4><strong>LUCIANA BENVENGO - REVISTA DA LAPA - 01/03/2000</strong></h4><p>&nbsp;</p><p>\"... O grupo demonstrou uma técnica impecável, e uma assombrosa variedade de modos de manipulação, que encantaram o público de Pamplona...\"</p><h4><strong>ALBERTO GARAYOA - O DIÁRIO DE NAVARRA - ESPANHA - 13/12/1999</strong></h4><p>&nbsp;</p><p>\"... A Cia. Truks repete a dose de talento e competência que faz valer a pena prestigiar seus espetáculos... Os manipuladores estão cada vez mais preparados e afinados na difícil arte de anular-se em cena em favor da missão de dar vida a objetos inanimados... A peça se encerra com uma cena de muita emoção e grande impacto...\"</p><h4><strong>DIB CARNEIRO NETO - O ESTADO DE SÃO PAULO - 18/06/1999</strong></h4><p>&nbsp;</p><p>\"... Lucas é um garoto tão bacana, que você vai querer virar amigo dele. Alguns pais vão querer levá-lo para casa. Ele tem o poder de criar sonhos, e dar forma a eles pelo simples pensamento! Apesar de ser boneco, ele parece gente, de tão bem feito que é...\"</p><h4><strong>MANOEL OCHOA - REVISTA ZÁ - Número 31 - 01/06/1999</strong></h4><p>&nbsp;</p>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>TEXTO:&nbsp;</h4><p>HENRIQUE SITCHIN e VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>DIREÇÃO:</h4><p>&nbsp;HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>CONCEPÇÃO E CONSTRUÇÃO DE BONECOS E CENÁRIOS:&nbsp;</h4><p>VERÔNICA GERCHMAN, SANDRA GRASSO, VALÉRIA PERUSSO E HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>TRILHA SONORA E ILUMINAÇÃO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:</h4><p><br>AGUINALDO RODRIGUES<br>BIANCA MUNIZ<br>DRIELY PALÁCIO,<br>MARCELA ARCE<br>JOÃO SANTIAGO<br>THAÍS ROSSI</p><p>&nbsp;</p><h4>OPERAÇÃO DE SOM E ILUMINAÇÃO:&nbsp;</h4><p>STEFANY ARAÚJO</p>','2022-03-02 17:31:41','2022-03-13 17:42:15'),(11,3,'espetaculo-vovo-cia-truks-foto-alberto-rocha-3_20220302143824rDOdrZGizs.jpg','vovo','VOVÔ','Teatro para todas as idades','<p>Com uma linguagem lúdica, porém cheia de poesia, e acessível às crianças, fazermos uma singela e emocionante homenagem aos imigrantes que chegaram ao nosso país em meados do século passado. Não contamos uma história de reis, rainhas, príncipes e princesas. Aqui os heróis não enfrentam bruxas e dragões... No entanto, contamos como foi a dura jornada de vida dos imigrantes, e reforçamos para as crianças o seu incrível heroísmo. A peça faz, de nosso vovô, um ícone de tantos heróis que, sim, como príncipes corajosos, enfrentaram as guerras, e as tantas e profundas transformações sociais do último século, com um objetivo maior: protegerem as suas famílias, de forma que nós, hoje, possamos estar aqui para relembrarmos e contarmos suas belíssimas histórias. Esta é uma delas!</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"...O espetáculo Vovô nos encantou com um enredo que resgata a oralidade, a memória, os laços de afetividade e a continuidade da vida em outras vidas, a história do próprio tempo. Tecnicamente, o grupo está de parabéns mas, felizmente, a técnica apenas se escondeu atrás da fantasia, do sonho e do encantamento. Esses paulistas são manipuladores diretos de nossa fantasia, e como é bom se deixar manipular por eles...\"</p><h4><strong>JÓRIA LIMA - NO SITE \"SENHORA K\", DE BELO HORIZONTE - MG - 13/06/2003</strong></h4><p>&nbsp;</p><p>\"... Com simplicidade, delicadeza e a devida dose de poesia, capaz de envolver o público adulto ou infantil, a Cia. Truks mostra que, para se contar uma boa história, basta deixar correr solta a fantasia. Além de bonecos e atores, efeitos como desenhos fosforescentes que servem para resumir a história valorizam a capacidade de síntese de um espetáculo já sintético e muito bem amarrado...\"</p><h4><strong>CLARA ARREGUY - O ESTADO DE MINAS - 03/06/2003</strong></h4><p>&nbsp;</p><p>\"... Desde 1990, a Cia. Truks tem encantado gente de todas as idades com sua arte bonequeira. A diversos e cativantes espetáculos, junta-se agora mais um, Vovô. A peça reverencia imigrantes europeus estabelecidos no Brasil... Os movimentos dos bonecos são tão precisos, realistas e humanos, que o público quase se esquece da existência de pessoas atrás deles...\"</p><h4><strong>ANA PAULA BUCHALLA - REVISTA VEJA SP - 03/09/2002</strong></h4><p>&nbsp;</p><p>\"... Vovô acerta no humor, na precisão técnica dos recursos de animação e na inteligência do texto sobre uma família que vem da Europa e ajuda a fazer a história do Brasil... A peça privilegia temas fundamentais para a formação psicológica das crianças, como a força inevitável da passagem do tempo e a necessidade de se transmitir histórias de geração a geração... Há no final um resumo das cenas, que acontece em outra linguagem, complementar à que foi apresentada - isso significa mostrar ao público mirim as variações possíveis dentro das linguagens artísticas. Isso é de uma riqueza sem valor estimado para a formação de público... O final, bem, o final é muito bem arquitetado. É ver para crer. A Truks mais uma vez não quis ser fácil. Emociona profundamente, só que de forma diferente da esperada. A morte está ali, naquele final surpreendente, mas em forma de vida, porque afinal, não existe uma sem a outra, e as crianças precisam lidar com isso para tornarem-se seres humanos completos. Vá aprender sobre vida e morte com Vovô, da Cia. Truks...\"</p><h4><strong>DIB CARNEIRO NETO - O ESTADO DE SÃO PAULO - 30/08/2002</strong></h4><p>&nbsp;</p><p>\"...No Festival de Teatro de Bonecos de Canela, os grupos internacionais surpreendem quando mostram técnicas e produções inovadoras. Mas os espetáculos brasileiros os superam, de longe, em emoção. Prova disso foi a peça Vovô, da Cia. Truks, de São Paulo. Os brasileiros fizeram bonito, ao nos fazer chorar de emoção..\"</p><p><strong>JORNAL ZERO HORA - RS - 09/06/2000</strong></p>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>TEXTO e DIREÇÃO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>CONFECÇÃO DE BONECOS, CENÁRIOS E OBJETOS DE CENA:&nbsp;</h4><p>HENRIQUE SITCHIN, VERÔNICA GERCHMAN, MARLI HATUM, CLAUDEMIR SANTANA, CASSIA DOMINGUES E SANDRA LESSA</p><p>&nbsp;</p><h4>FIGURINOS DOS BONECOS:&nbsp;</h4><p>VALÉRIA PERUSSO E VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>TRILHA SONORA E ILUMINAÇÃO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:&nbsp;</h4><p>AGUINALDO RODRIGUES, BIANCA MUNIZ, DRIELY PALÁCIO, HENRIQUE SITCHIN E THAÍS ROSSI</p><p>&nbsp;</p><h4>OPERAÇÃO DE SOM E ILUMINAÇÃO:&nbsp;</h4><p>JOÃO SANTIAGO</p>','2022-03-02 17:38:25','2022-03-13 16:54:48'),(12,4,'zoo-ilogico-2-foto-pietro-feliciano_20220305123219lNTZnydQBs.jpg','zoo-ilogico','ZOO-ILÓGICO','Teatro para todas as idades','<p>A peça traz para os palcos um incrível estímulo à criatividade! A partir do uso, e da transformação de simples objetos do cotidiano, desfilam pela cena mais de uma dezena de divertidas e inusitadas criaturas animadas: uma galinha feita com um bule e um espanador de pó, uma tartaruga composta por uma saladeira e uma colher, um gato simbolizado por uma peneira, entre tantas outras. Tudo começa quando dois amigos resolvem fazer um piquenique no Zoológico. &nbsp;Ao encontrarem as portas do parque fechadas, não se intimidarão em criar, com muita criatividade e um certo non-sense, o seu zoológico particular, em que bichos serão feitos de pratos, panos, garrafas, &nbsp;talheres e tudo o mais que estiver ao alcance de suas mãos. As nada comuns criaturas viverão situações cômicas ou poéticas. Estará criado o Zôo-ilógico, possível na imaginação de todos. E aberto, sempre!</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>“Zôo-Ilógico é um primor. Um programa para toda a família, aliando fantasia e técnica de forma atraente e prazerosa. O uso do chamado “teatro de objetos”, em que coisas inanimadas ganham vida pelas mãos dos atores manipuladores é harmonioso com a história, ingênuo sem ser antigo, hilário sem ser grosseiro e, de quebra, com momentos de pura poesia e romantismo. As transformações que se dão à frente da platéia são de uma criatividade incrível. O non sense de algumas criaturas-objetos intriga e estimula a imaginação. Adultos se empolgam de uma forma pouco vista nas platéias de teatro infantil e crianças vivem 50 minutos de puro encantamento.”</p><h4><strong>DIB CARNEIRO NETO – JORNAL O ESTADO DE SÃO PAULO - 09/04/2004</strong></h4><p>&nbsp;</p><p>“Bule, espanador e saco de lixo viram galinha, elefante e foca no teatro de bonecos Zôo-Ilógico. O público vê os personagens-bichos sendo feitos e pode aproveitar as idéias em casa. Os amigos nãao falam quase nada, mas fazem caras muito engraçadas e se comunicam por gestos. Até um bebê de nove meses batia palmas e dançava no colo da mãe”.</p><h4><strong>JORNAL “FOLHINHA DE SÃO PAULO” - 20/03/2004</strong></h4><p>&nbsp;</p><p>“A montagem é um dos melhores espetáculos infantis da cidade. Na trama, a intenção dos personagens era fazer um piquenique no zoológico. Ao encontrar o local fechado, resta-lhes soltar a imaginação. Sem palavras, e com um sincronismo espantoso, os atores enfeitiçam a platéia ao transformar pratos, bacias, talheres e panelas em animais encantadores.”</p><h4><strong>REVISTA VEJA SÃO PAULO - 01/03/2004</strong></h4><p>&nbsp;</p><p>“O grupo manipula objetos com mestria. Não são apenas os objetos que ganham vida nas cenas, pois os manipuladores são muito expressivos. À vontade com a arte de interpretar, eles brincam com a plateia e iludem a todos, sem afetação ou estardalhaços.”</p><h4><strong>MÔNICA RODRIGUES DA COSTA – JORNAL FOLHA DE SÃO PAULO - 01/03/2004</strong></h4>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>IDEIA ORIGINAL:</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ROTEIRO, CRIAÇÃO E PRODUÇÃO GERAL:&nbsp;</h4><p>HENRIQUE SITCHIN, CLÁUDIO SALTINI E VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>DIREÇÃO: &nbsp;</h4><p>VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>ELENCO:&nbsp;</h4><p>GABRIEL SITCHIN E ROGÉRIO UCHOAS</p>','2022-03-02 17:54:13','2022-03-13 16:56:14'),(13,5,'e-se-as-historias-fossem-diferentes-9-foto-pietro-s-feliciano_20220302145643T3maXBNDze.JPG','e-se-as-historias-fossem-diferentes','E SE AS HISTÓRIAS FOSSEM DIFERENTES?','Espetáculo para todas as idades','<p>Aqui a Cia Truks utiliza uma técnica absolutamente inusitada para fazer do espetáculo uma divertida brincadeira: trata-se da utilização de um interessante recurso técnico que mistura bonecos com desenhos que são produzidos ao vivo, em cena.&nbsp; Um ator tem, à sua frente, uma versátil mesa de trabalho, equipada com lápis de cor, tintas e papéis de cores e texturas variadas. Conforme a história se desenrola, ele monta cenários, desenha personagens e situações que, por meio de uma câmara de vídeo, colocada sobre a mesa, são lançadas, por um projetor, em um telão onde, por sua vez, surgem figuras – bonecos de sombras, a interagir com as imagens projetadas. É contada uma bonita e tocante história que fala da tão necessária aceitação das diferenças.</p>','<h2>A Imprensa comenta</h2><p>&nbsp;</p><p>\"Com uma diversidade de vozes impressionante, Sitchin faz crianças e adultos rirem...<br>...A história ironiza valores com os quais os seres humanos estão permanentemente preocupados: o de corresponder aos padrões sociais que lhes são exigidos\". Avaliação: ótimo.</p><h4><strong>MÕNICA RODRIGUES DA COSTA - Jornal Folha de SP - 20/06/2008</strong></h4><p>&nbsp;</p><p>\"Impressiona a velocidade com que Sitchin muda de charges sem deixar a plateia confusa. De forma muito criativa, a peça explora as possibilidades do vídeo. Munido de lápis coloridos e papéis, o hábil narrador desenha seus companheiros de cena. Uma câmera capta as imagens e transmite a irresistível brincadeira em um telão.\"</p><h4><strong>HELENA GALANTE - Revista Veja - SP - 14/06/2008</strong></h4>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>TEXTO, DIREÇÃO E CENOGRAFIA:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:</h4><p><br>HENRIQUE SITCHIN e GABRIEL SITCHIN</p><p>&nbsp;</p><h4>CRIAÇÃO E ANIMAÇÃO DAS FIGURAS EM SOMBRAS:<br>&nbsp;</h4><p>VALTER VALVERDE</p><p>&nbsp;</p><h4>ILUMINAÇÃO E TRILHA SONORA:<br>&nbsp;</h4><p>HENRIQUE SITCHIN</p>','2022-03-02 17:56:44','2022-03-13 16:58:15'),(14,6,'sem-titulo-1_20220302145927Udn9LSaZyG.jpg','sonhatorio','SONHATÓRIO','Teatro para todas as idades','<p>É hora do almoço no Sanatório Boa Cabeça.&nbsp; Sentam-se à mesa três supostos loucos, para a refeição. Porém, não há nada para comer ou beber. Sedentos e famintos, os amigos partem em inventam uma deliciosa “viagem” em busca de comida, que os levará para áridos desertos, para o fundo do mar e para longínquos planetas. Incríveis personagens e cenários, feitos de guardanapos, bacias, copos, garrafas pet, sacolas plásticas, talheres e pratos os acompanharão por suas aventuras. Nossos amigos atestam que “brincar ainda é o melhor remédio” para tudo. Eles transformam o que seria um sanatório em um... <i><strong>SONHATÓRIO</strong>!</i></p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"Há quase dez anos, a peça “Zôo-Ilógico” tornou-se um marco do teatro de objetos. Ali a brincadeira de criança se materializou de forma poética e genial... Em “Sonhatório” a trupe repete o feito: Na cozinha do Sanatório Boa Cabeça três internos driblam a fome fazendo pinguins com garrafas térmicas e águas-vivas com sacos de lixo. É brilhante, de fazer chorar, como a maioria das montagens de Sitchin - que agora dirige o filho e dois outros excelentes jovens...\"</p><h4><strong>FERNANDA ARAÚJO - JORNAL O ESTADO DE SÃO PAULO - 04/07/2012</strong></h4><p>&nbsp;</p><p>\"Henrique Sitchin, diretor e autor do espetáculo atinge o objetivo de encontrar a poesia nas pequenas coisas. Com um elenco talentoso, formado por Rafael Senatore, Gabriel Sitchin e Hugo Reis, a montagem reforça o poder da imaginação e a plateia rapidamente embarca na viagem...\"</p><h4><strong>TATIANE ROSSET - REVISTA VEJA SÃO PAULO - 14/08/2012</strong></h4><p>&nbsp;</p><p>\"À medida que a peça avança, os três manipuladores vão exibindo mais e mais habilidades, mais e mais possibilidades de se brincar com os apetrechos triviais, transformando-os em maravilhas inimagináveis. ??Outro detalhe louvável é que, em meio a tantas brincadeiras com objetos que podem vir a provocar acidentes domésticos, há espaço justamente para esse tipo de alerta à plateia mirim, mas sem tom de ralhar ou de dar bronca. Por exemplo, quando dois dos atores vão brincar de luta, com facas à mão, interrompem a cena logo no início e trocam as facas por colheres. A plateia entende o recado, capta a mensagem. ??Não quero estragar a surpresa da cena final, mas não posso deixar de dizer que é tocante, muito emocionante. Em seu Sonhatório, a Truks nos prescreve, do alto de sua sabedoria lúdica, qual o melhor remédio para uma vida saudável. Não deixe de ir buscar sua receita. Com a família toda.\"</p><h4><strong>DIB CARNEIRO NETO - REVISTA CRESCER - 03/08/2012</strong></h4>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>Criação:</h4><p><br>HENRIQUE SITCHIN e GABRIEL SITCHIN</p><p>&nbsp;</p><h4>Texto e Direção:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:</h4><p><br>GABRIEL SITCHIN, THAÍS ROSSI E ROGERIO UCHOAS</p><p>&nbsp;</p><h4>Criação e Confecção de Bonecos e Figuras:</h4><p><br>HENRIQUE SITCHIN, GABRIEL SITCHIN, RAFAEL SENATORE, HUGO REIS, PIETRO SITCHIN, KARINA PRALL E CAMILA OLIVEIRA</p><p>&nbsp;</p><h4>Trilha Sonora:</h4><p><br>RAFAEL SENATORE E FRANCISCO MUNIZ</p><p>&nbsp;</p><h4>Iluminação e Cenografia:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Operação de Som e Iluminação:</h4><p><br>BIANCA MUNIZ</p><p>&nbsp;</p>','2022-03-02 17:59:28','2022-03-13 16:58:59'),(15,7,'construtorio-2-foto-alberto-greiber_20220313140135CqvYggOCyW.jpg','construtorio','CONSTRUTÓRIO','Teatro para todas as idades','<p>Três operários ficam impedidos de sair da obra em que trabalham, ao final do expediente, por conta de uma tempestade que não os deixa voltarem para as suas casas. “Presos” que estão, no entanto, libertam a fantasia para criar as mais incríveis aventuras: transformam ferramentas, e demais objetos da obra, em instrumentos de suas incríveis brincadeiras. Assim recontam, com muita criatividade, alguns contos de fadas, parodiam, com muito bom humor, filmes clássicos, criam e interagem com bichos e criaturas formadas pelas mais inusitadas coisas. Revelam-se, por fim, operários de uma obra muito especial. São, sobretudo, poetas, construtores de sonhos...</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"‘Construtório’, do autor e diretor Henrique Sitchin, é um desfile eufórico de criatividade, levando a plateia ao delírio...A história é a de três operários que querem voltar para casa, no fim do expediente, mas a chuva lá fora cai violentamente, impedindo-os de sair do trabalho. Portanto, passam a noite ali, brincando com os mesmos objetos com que lidaram o dia inteiro. Alicates, brocas, mangueiras, canos, sacos plásticos, torneiras e muitas ferramentas viram peixinhos, papagaios, cobras e tantas outras invencionices lúdicas... Logo a plateia entra em êxtase com tanta criatividade brotando de objetos triviais. Adultos embarcam nessa viagem tanto quanto as crianças. Observei que desta vez a direção de Henrique Sitchin optou por iniciar o espetáculo já no ápice de euforia e alto astral – e isso se mantém durante toda a duração da peça, de tirar o fôlego. A plateia fica ligada e acesa desde o primeiro minuto – e, o que é raro no teatro infantil, surgem muitas palmas em várias cenas abertas, comprovando a excitação do público diante de tantos estímulos propostos pela Truks. É ver para crer. Não fique de fora dessa alegria. De quebra, como sempre costuma acontecer nas peças dessa companhia, o final traz surpresas e uma dose extra de criatividade e encantamento.\"</p><h4><strong>DIB CARNEIRO NETO - REVISTA CRESCER - 01/06/2006</strong></h4><p>&nbsp;</p><p>“A hora mais esperada por muitos trabalhadores é o fim do expediente. Numa obra, três operários respiram aliviados por poderem ir para casa, mas um dilúvio cai assim que eles põem os pés na rua. Impedido de enfrentá-lo o trio solta a imaginação e faz o tempo passar com brincadeiras e histórias que levam os pequenos às gargalhadas. Em um dos pontos altos as crianças tentam adivinhar o que os três estão criando com material de banheiro. Tubos, conexões e até assentos sanitários dão vida a astronautas, elefantes e borboletas. A peça incentiva a fantasia até o último minuto, quando as garotada é convidada a continuar a brincadeira em casa.”</p><h4><strong>MARIANA OLIVEIRA – REVISTA VEJA SP - 01/06/2006</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Texto e direção:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:</h4><p><br>GABRIEL SITCHIN, HENRIQUE SITCHIN &nbsp;e<br>ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>Criação e Confecção de Cenografia<br>e Formas Animadas:</h4><p><br>DALMIR ROGÉRIO PEREIRA,<br>JOSÉ VALDIR ALBUQUERQUE e<br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Trilha Sonora:</h4><p><br>RAFAEL SENATORE, IONE DIAS DE AGUIAR e CHICO MUNIZ</p><p>&nbsp;</p><h4>Iluminação:</h4><p><br>HENRIQUE SITCHIN</p>','2022-03-02 18:03:00','2022-03-13 17:01:36'),(16,8,'acampatorio-2-foto-alberto-rocha-1_2022031314033657n7nijZxb.jpg','acampatorio','ACAMPATÓRIO','Teatro para todas as idades','<p>Três criativos amigos partem para uma empolgante aventura: vão acampar em terras desconhecidas! Entram em cena com as suas mochilas superequipadas e logo abrem as portas para a imaginação. Passarão o dia vivendo as mais malucas situações, tais como uma frustrada pescaria, um engraçado piquenique, com direito a uma empolgante luta contra um exército de formigas, arrepiantes histórias “de terror” em volta da fogueira, entre outras passagens repletas de diversão, bom humor e até mesmo ilusionismo. Viverão, por fim, um destes dias que fazem a vida valer a pena: ao lado dos amigos queridos, com quem gostaríamos que o tempo parasse, ou que se repetisse por toda uma vida...</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"O nonsense e o real se misturam e brincam com a gente. As crianças na plateia deliram: não param nas cadeiras, não querem perder nem um lance, gargalham. Os adultos assistem encantados, em poucos minutos entregues ao deleite de uma boa história narrada praticamente sem falas e, de alguma forma, voltam para um lugar que nunca deveriam ter deixado: a imaginação.&nbsp;Em Acampatório, os atores usam o que se chama de “teatro de objetos”. A Truks prefere chamam “teatro com objetos”, o que faz todo sentido. Sentido? Ah, esta é uma brincadeira importante: o que era passa a ser sempre outra coisa. A partir de um cantil e duas canecas, vemos uma vaca; três pés de patos e snorkels aparecem-nos como três cantores; dois cachos de bananas se transforam em pés de caranguejo e a brincadeira é sempre tão esperada e excitante que minha filha Clarice, de 3 anos, vira para mim e pergunta: “Mãe, e agora, o que vai virar isso?”, assim que avista um objeto novo chegando.<br>A gente pode rir muito, mas muito mesmo. Rir de chorar. Os atores são cômicos e as cenas transformadas dão sempre uma mistura de “óooooooooh” com “não acredito!”. Dá uma vontade de correr para casa e brincar. Brincar, lembra?...&nbsp;A Truks não tem medo de jogar emoção nas crianças. Não se importa se ela chora ou se inclina a cabecinha de lado, sentindo a beleza da compaixão com um personagem (é sempre a mais comum reação da minha filha). Porque ela aposta que cada um de nós sempre pode ser mais e que a tristeza muitas vezes nos dá a mão nesta virada. As pessoas “ainda não foram terminadas”, como diz Guimarães Rosa. Sempre podem se transformar. Feito um teatro com objetos. Feito poesia. Feito ser humano.\"</p><h4><strong>CRISTIANE ROGÉRIO - SITE ESCONDERIJOS DO TEMPO - 08/09/2015</strong></h4><p>&nbsp;</p><p>\"E o trio de encantadores fazedores de poesia do repertório da Cia. Truks está de volta. Depois de ‘Construtório’ e ‘Sonhatório’, agora é a vez de ‘Acampatório’. Os três personagens arrebatam o público com carisma e sedução, nessa que já virou talvez a série mais bem-sucedida de teatro de objetos que já ocupou nossos palcos em São Paulo...&nbsp;Henrique Sitchin, na direção, nunca abre mão de uma trilha sonora com canções e ritmos conhecidos, pontuando as ações ora com temas de uma alegria frenética ora com melodias sentimentais mais intimistas. Ele também nunca deixa o virtuosismo e a técnica da manipulação de objetos, inegáveis em suas produções, predominarem sobre o encantamento e o fascínio de uma boa história. A técnica está ali, mas a serviço da emoção, da fabulação, do estímulo à imaginação. Mais uma vez ele nos surpreende ao usar a ingenuidade de três amigos brincalhões para nos lembrar da importância de brincar e não deixar morrer a criança que mora dentro de nós...Palmas, palmas e mais palmas para a Cia. Truks. Seja ‘Construtório’, ‘Sonhatório’ ou ‘Acampatório’, o que essa veterana trupe nos oferece é sempre ‘encantatório’\".</p><h4><strong>DIB CARNEIRO NETO - REVISTA CRESCER - 28/08/2015</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>TEXTO:</h4><p><br>HENRIQUE SITCHIN,<br>GABRIEL SITCHIN,<br>RAFAEL SENATORE<br>ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>DIREÇÃO:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:</h4><p><br>GABRIEL SITCHIN,<br>THAÍS ROSSI e<br>ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>OPERAÇÃO DE SOM E ILUMINAÇÃO:</h4><p><br>THIAGO UCHOAS</p><p>&nbsp;</p><h4>CENOGRAFIA E CRIATURAS ANIMADAS:</h4><p><br>DALMIR ROGÉRIO PEREIRA<br>GABRIEL SITCHIN<br>HENRIQUE SITCHIN<br>RAFAEL SENATORE<br>ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>TRILHA SONORA:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>SOMBRAS:</h4><p><br>BRUNO SAGGESE</p><p>&nbsp;</p><h4>DESENHOS:</h4><p><br>HELIO SENATORE</p>','2022-03-02 18:04:41','2022-03-13 17:03:36'),(17,9,'ltima-noticia-1-foto-henrique-sitchin_20220308140521p7xaQKBNfY.JPG','ultima-noticia','ÚLTIMA NOTÍCIA','Teatro para todas as idades','<p>Um misterioso detetive procura por uma estranha figura, fugida, vejam só, das páginas dos jornais! Trata-se de um \"menino de jornal\", personagem antes preso ao periódico, que decide deixar a realidade para viver as mais incríveis aventuras. É assim que ele é engolido por uma baleia em alto mar, perde-se no mundo dos dinossauros, voa com seu balão para o espaço sideral, até por fim salvar uma indefesa princesa de um temível dragão. Estarão abertas as portas para a poesia. Estará no ar a última notícia: crianças brincam no mundo dos sonhos!</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"Com uma longa trajetória de grandes sucessos, verdadeiras delicadezas cênicas para crianças de 0 a 90 anos, a Cia. Truks de Animação está nos proporcionando mais uma pérola. O nome da peça é ‘Última Notícia’... &nbsp;No formato dramatúrgico de “jornada do herói”, o espetáculo conta a história de um menino que mergulha literalmente nas páginas de um jornal e sai pelo mundo, em grandes aventuras, enquanto seu pai, como um detetive obstinado, segue suas pistas (como as pisadas no chão) para encontrar o filho a todo custo. E o menino não dá moleza: um dia está no espaço sideral, em outro está no fundo do mar (e dentro da barriga de uma baleia), em outro vai para o tempo dos dinossauros, e assim por diante. Metáforas não faltam o tempo todo, pois a Truks sempre nos oferece várias camadas de interpretação de seus enredos aparentemente simples e lineares.&nbsp;</p><p>Há sutilezas muito bem pensadas, como quando o dragão filhote grita pelo pai, pouco antes de o menino protagonista também gritar: Papai! O dragãozinho prepara o clima para o desfecho entre pai e filho, numa jogada esperta da dramaturgia. É lindo também quando o menino inverte de posição com o pai, vestindo a mesma roupa de detetive. Há nesse recurso simples um mundo imenso de significados e sugestões, sobretudo sobre a passagem do tempo e as alternâncias de papel que a gente encara pela vida afora.</p><p>Incrível é observar o quanto os atores-manipuladores da Truks estão descobrindo a cada novo espetáculo as infinitas possibilidades de interpretar enquanto manipulam tecnicamente os bonecos (aqui, todos feitos de papel jornal, para combinar com o enredo). Há um limite preciso e precioso entre essas duas tarefas, para que as interpretações dos manipuladores sejam potentes e criativas, mas não a ponto de quebrarem a magia e o encantamento de dar vida aos bonecos. É necessário cuidar minuciosamente dessa dosagem – e o grupo consegue com louvor. O diretor Sitchin nunca deixa de criar soluções encantadoramente simples para determinadas situações do enredo. A poesia de sua direção nunca perde a mão. Desta vez, é impossível não se emocionar com a singeleza de pedir à plateia que ajude a soprar bem forte, para que o balão do menino possa subir. É um momento interativo de grande poesia, o que é raro em outros encenadores, que acham que interação é gritaria\".</p><h4><strong>DIB CARNEIRO NETO - REVISTA CRESCER - 11/07/2014</strong></h4><p>&nbsp;</p><p>“Com bonecos, folhas de jornal, luzes e sombras, os atores-manipuladores da Cia. Truks apresentam uma singela e emocionante história. O clima de suspense marca o começo da encenação. Como numa brincadeira de esconde-esconde, um detetive e um menino (feito de papel) são transportados para um mundo de sonhos. Lá, o garoto se perde entre dinossauros, salva uma princesa do dragão e chega a ser engolido por uma baleia. Cenas poéticas envolvem as crianças, como quando o rapazinho consegue voar de balão com a ajuda do sopro da plateia. Se no desenrolar da trama os pequenos ficam vidrados nas incríveis aventuras, é no desfecho, revelado por uma manchete de jornal, que os pais são laçados de vez — alguns chegam às lágrimas”.</p><h4><strong>BRUNA RIBEIRO – REVISTA VEJA SP - 14/06/2014</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Concepção Geral, Texto e Direção:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:</h4><p><br>AGUINALDO RODRIGUES<br>BIANCA MUNIZ<br>DRIELY PALÁCIO<br>THAÍS ROSSI<br>JOÃO SANTIAGO</p><p>&nbsp;</p><h4>Criação e Confecção de Cenografia<br>Adereços e Bonecos:</h4><p><br>DALMIR ROGÉRIO PEREIRA, HELDER PARRA, LUCIANA SEMENSATTO,<br>PRISCILA CASTRO, ANGÉLICA PRIOSTE, JOÃO SANTIAGO,<br>JORGE MIYASHIRO e HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Trilha Sonora:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Iluminação:</h4><p><br>JOÃO SANTIAGO, HELDER PARRA E HENRIQUE SITCHIN</p><h4>&nbsp;</h4><h4>Operação de som e Iluminação:</h4><p><br>STEFANY ARAÚJO</p>','2022-03-02 18:07:37','2022-03-13 17:05:41'),(18,10,'expedicao-pacifico-4-foto-alberto-rocha_20220308141054TlvFLBWnSa.jpg','expedicao-pacifico','EXPEDIÇÃO PACÍFICO','Teatro para todas as idades','<p>Aqui a Cia. Truks trata de fazer, da grande Ilha de lixo que cresce a cada dia em algum ponto do Oceano Pacífico, cuja extensão já é maior do que vários estados do Brasil juntos, uma sutil alegoria de situações do nosso cotidiano. Na Grande Ilha de Lixo do Pacífico é que vão parar toda a sorte de detritos e rejeitos da nossa humanidade. Pois bem, conduzindo uma carroça de lixo repleta de descartes, vemos, nas figuras de dois “carroceiros”, gente tão simples e humilde, o ser humano marginalizado pela sociedade, tratado à deriva pelos asfaltos, tal como o lixo que navega pelo oceano. Neste espetáculo, no entanto, os tratamos como verdadeiros poetas. Aqui, os dois criativos catadores de lixo são capazes de construir, de quase nada – apenas sacolas e lonas plásticas, um mundo fantástico, de diversão, bom humor e muitos sonhos. Na medida em que recolhem o lixo da grande cidade, o transformam em verdadeira poesia visual, criando belíssimas imagens, divertidas criaturas e incríveis cenários para onde são capazes de viajar, e onde viverão as suas aventuras e brincadeiras.</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"...É simplesmente incrível o que eles fazem com os saquinhos e outros plásticos. Nos emocionamos, damos risada, entendemos do que eles estão falando com a atuação de poucas palavras e muitas expressões. A peça é uma denúncia ou um alerta sobre o lixo em nossos oceanos a partir da perspectiva de dois catadores. Não tem panfletagem, tem sensibilidade. Clarice, minha filha de 4 anos, emocionou-se com uma belíssima cena de balé sem, talvez, entender como aquela delicadeza encenada se contrasta com a dura realidade de uma sociedade que vive sem pensar em consequências. Sob a direção de Henrique Sitchin, os atores Gabriel Sitchin e Rogério Uchoas nos colocam à frente do problema ao mesmo tempo em que parece somente que estamos assistindo a dois grande amigos, muito criativos, brincar. Que se olham nos olhos e nos mostram a escolha que fizeram, de qual lado estão nessa vida\".</p><h4><strong>CRISTIANE ROGÉRIO - SITE ESCONDERIJOS DO TEMPO - 08/09/2016</strong></h4><p>&nbsp;</p><p>\"...A cada novo espetáculo, o grupo se supera na arte de transformar objetos em personagens, ou seja, coisas inertes em figuras animadas. É uma&nbsp;<a href=\"http://revistacrescer.globo.com/Os-primeiros-1000-dias-do-seu-filho/noticia/2015/02/3-dicas-para-estimular-criatividade-do-seu-filho.html\">festa da imaginação</a>. E o que mais é o teatro senão essa farra sem freios que conjuga o verbo ‘imaginar’ em todos os tempos e formas verbais possíveis e impossíveis? Desta vez, a equipe de criadores do espetáculo – Henrique Sitchin, Gabriel Sitchin e Rogério Uchoas – escolheu&nbsp;<a href=\"http://revistacrescer.globo.com/Brincar-e-preciso/noticia/2016/07/5-respostas-sobre-importancia-das-brincadeiras.html\">‘brincar’</a>&nbsp;quase que exclusivamente – a propósito do tema da peça – com aqueles saquinhos plásticos de supermercado, comumente usados para depósito de lixo caseiro. Você não vai acreditar nas maravilhas em que eles se transformam, pelas mãos dos dois atores-manipuladores, Gabriel e Rogério...\"</p><p>\"...Há uma dramaturgia muito bem pensada e encadeada, dando conta de toda a técnica de manipulação para que não vire um mero exibicionismo da forma, sem cuidados com o conteúdo. O grande mérito da Cia. Truks sempre foi saber contar boas histórias, aliando técnica e emoção. Aqui, conseguem mais uma vez – e com mérito...\"</p><p>\"...Expedição Pacífico’, em suma, trata de um assunto muito sério e responsável – a sustentabilidade, a ecologia, o cuidado com o planeta – de uma forma leve, criativa e inteligente.&nbsp; O assunto não cai nunca no tom panfletário...\"</p><h4><strong>DIB CARNEIRO NETO - 02/09/2016</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Texto:&nbsp;</h4><p>HENRIQUE SITCHIN, GABRIEL SITCHIN e ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>Direção:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:&nbsp;</h4><p>GABRIEL SITCHIN e ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>Operação de som e iluminação:&nbsp;</h4><p>BIANCA MUNIZ</p><p>&nbsp;</p><h4>Criação e Confecção de Cenografia e Formas Animadas:</h4><p><br>DALMIR ROGÉRIO PEREIRA, GABRIEL SITCHIN, JOSÉ VALDIR ALBUQUERQUE, HENRIQUE SITCHIN E ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>Trilha Sonora e Iluminação:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><p>&nbsp;</p>','2022-03-02 18:09:39','2022-03-13 17:07:28'),(19,11,'isso-e-coisa-de-crianca-2-foto-lazerum_20220313141141s5La8sWGvg.jpg','isso-e-coisa-de-crianca','ISSO É COISA DE CRIANÇA','Teatro para todas as idades','<p>Após a realização de dezenas de oficinas para crianças de Centros de Acolhimento da Prefeitura de São Paulo, o espetáculo foi concebido com o objetivo de materializar, nos palcos, as ideias criadas por elas, em seus exercícios. Exibimos, nessa peça, um desfile da mais alta criatividade que, ousamos dizer, não seria criada por adultos. Estão em cena o bule que produz o suco da imaginação, um ballet de peixes sapatos, uma revoada de gaiolas livres como as almas dos pássaros que ali viveram, a gravata que é uma cobra a enforcar o papai todos os dias, o menino que pulava até as nuvens, entre outras propostas das crianças, em bonitas cenas, cheias de humor e muita poesia.</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>O espetáculo é bem tocante, pois nasceu a partir de cenas sugeridas por crianças, em oficinas prévias realizadas pelo grupo. Grande sacada é exibir, no final, cenas gravadas durante essas oficinas, mostrando ao público os momentos exatos em que cada história surgiu. Impossível não se encantar e não se emocionar.</p><h4><strong>DIB CARNEIRO NETO - 27/03/2018</strong></h4><p>&nbsp;</p><p>Que histórias podem ser contadas com um pé de sapato, uma bandeja, duas raquetes e algumas caixas de papelão? No novo espetáculo da companhia Truks, são diversas as narrativas curtas e envolventes que surgem dos objetos manipulados por um quarteto de atores que se divertem (e nos divertem) muito em cena. Delicioso convite a imaginar, a peça traz a linguagem da simplicidade, a mesma falada pelas crianças e pelo brincar.</p><h4><strong>GABRIELA ROMEU - 03/03/2018</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>DIREÇÃO E COORDENAÇÃO GERAL DO PROJETO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:&nbsp;</h4><p>AGUINALDO RODRIGUES, DRIELY PALÁCIO, GABRIEL SITCHIN E ROGÉRIO UCHOAS</p><p>&nbsp;</p><h4>ROTEIRO E CENOGRAFIA:&nbsp;</h4><p>AGUINALDO RODRIGUES, DRIELY PALÁCIO, GABRIEL SITCHIN, HENRIQUE SITCHIN E ROGÉRIO UCHOAS</p><h4><br>TRILHA SONORA:&nbsp;</h4><p>HENRIQUE SITCHIN E AGUINALDO RODRIGUES</p><h4><br>ILUMINAÇÃO:&nbsp;</h4><p>CAMILA JORDÃO</p><h4><br>OPERAÇÃO DE SOM E ILUMINAÇÃO:&nbsp;</h4><p>JOÃO SANTIAGO</p>','2022-03-02 18:14:04','2022-03-18 16:52:44'),(20,12,'img-1524_20220302151916XBnsZmdugx.JPG','pipo-e-fifi','PIPO E FIFI','Teatro para todas as idades','<p>O espetáculo faz uma fidelíssima transição, para os palcos, da obra literária de mesmo nome, da premiada autora CAROLINE ARCARI. A peça é uma importante ferramenta de proteção para os pequenos, explicando às crianças, a partir dos 3&nbsp;anos de idade, conceitos básicos sobre o corpo, sentimentos, convivência e trocas afetivas. De forma simples e descomplicada, ensina a diferenciar toques de amor de toques abusivos, apontando para as crianças caminhos para a proteção contra abusos sexuais.</p>','<h2>VOCÊ SABIA QUE 2 EM CADA 5 CRIANÇAS SOFRE, OU JÁ SOFREU ALGUM TIPO DE ABUSO SEXUAL NO BRASIL?&nbsp;</h2><p>&nbsp;</p><p>Sim, infelizmente as estatísticas são alarmantes e é urgente que cada um de nós faça a sua parte para mudar essa realidade tão triste. Com esse espetáculo fazemos um alerta - é preciso, para aqueles que verdadeiramente amam as crianças, &nbsp;entender os sinais, e impedir a violência sexual contra as crianças se consumar ou continuar. E principalmente, oferecemos um bom \"escudo\" para que as crianças aprendam como, e saibam que podem, SIM, se defender dos abusos!</p>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>TEXTO:&nbsp;</h4><p>CAROLINE ARCARI</p><p>&nbsp;</p><h4>ADAPTAÇÃO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>DIREÇÃO:&nbsp;</h4><p>GABRIEL SITCHIN E HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:&nbsp;</h4><p>AGUINALDO RODRIGUES, BIANCA MUNIZ, THAÍS ROSSI E DRIELY PALÁCIO</p>','2022-03-02 18:19:21','2022-03-13 17:23:37'),(21,13,'historia-de-bar-1_20220313143022oNtXg4iMog.jpg','historia-de-bar','HISTÓRIA DE BAR','Teatro para adolescentes e adultos','<p><strong>ESPETÁCULO PARA ADOLESCENTES E ADULTOS</strong></p><p>&nbsp;</p><p>Atrás do balcão um simples BARMAN. Sobre ele, terríveis acusações! Teria sido o responsável pela série de crimes hediondos que assolam a cidade? Seria ele o temido assassino, a decapitar, sem piedade, as suas indefesas vítimas? Pois então, é chegada a hora desse homem contar a sua história! Utilizando-se apenas e tão somente dos objetos existentes no bar, ele contará uma história \"macarrônica\", ao melhor estilo dos antigos filmes noir. Uma sucessão de ágeis e engraçadas passagens cênicas nos fazem mergulhar no mundo da noite paulistana, entre as ruas, os becos e as boates da cidade fria e calculista. Os bandidos, afiados facões, maços de cigarro ou garrafas de cachaça, desafiam os policiais, um espremedor de frutas, o acendedor automático de fogões (hoje com munição de sobra), o frasco de pimenta, entre tantos outros objetos que, nas mãos do hábil ator, se transformam nas mais divertidas criaturas.</p>','<p>História de Bar se utiliza de uma técnica bastante singular. Trata-se do “teatro de objetos”. Em lugar do ator de carne e osso, são protagonistas da cena personagens feitos de toda a sorte de objetos de nosso cotidiano. O maço de cigarros é Tino... Nico Tino, famoso matador de aluguel, que consume suas vítimas por dentro, corroendo-as sem que percebam... A doce e muito cheirosa enfermeira é AR... Arlete, um bastão de Bom Ar. O garçom é um pegador de gelo, o temível carcereiro, um espremedor de frutas, enquanto o bandido que nada sabe é literalmente um... laranja!</p><p>O jogo é absolutamente encantador. A cada passagem do espetáculo uma inteligente surpresa estará à espreita. O exercício criativo da montagem acaba por retratar o mundo de uma nova maneira, e produz uma experiência no mínimo original, e muito inusitada e divertida.</p>','<h2>FICHA TéCNICA</h2><p>&nbsp;</p><h4>IDÉIA ORIGINAL:&nbsp;</h4><p>JOSÉ VALDIR ALBUQUERQUE</p><p>&nbsp;</p><h4>TEXTO E DRAMATURGIA:&nbsp;</h4><p>HENRIQUE SITCHIN E JOSÉ VALDIR ALBUQUERQUE</p><p>&nbsp;</p><h4>ATOR SOLO:&nbsp;</h4><p>JOSÉ VALDIR ALBUQUERQUE</p><p>&nbsp;</p><h4>DIREÇÃO:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>OPERAÇÃO DE SOM E ILUMINAÇÃO:&nbsp;</h4><p>ADRYELA RODRIGUES</p><p>&nbsp;</p>','2022-03-02 18:21:25','2022-03-13 17:47:57'),(22,14,'big-bang-2-foto-henrique-sitchin_20220302152254fMvu7IBUUr.jpg','big-bang','BIG BANG','Teatro para adolescentes e adultos','<p><strong>ESPETÁCULO PARA ADOLESCENTES E ADULTOS</strong></p><p>&nbsp;</p><p>Neste divertido espetáculo, destinado ao público jovem e adulto, a Cia Truks faz as suas leituras próprias, onde vale reinventar ou comentar a história da humanidade de forma crítica, mas extremamente bem humorada e sagaz. Aqui o grupo retrata, a seu modo (e ao modo dos bonecos!), a aventura humana sobre a terra, em um espetáculo rico em técnicas, imagens instigantes, humor e poesia. De uma caravela portuguesa, em lugar de um canhão, sairá um enorme tubo de aspirador de pó, a sugerir, muito mais do que uma embarcação, o tentáculo de um monstro. Um astronauta tenta conquistar a lua utilizando-se de gangorras e outros artefatos inúteis; Seres humanos viram sacos de lixo em disputa pelos detritos de uma grande cidade; Um homem é literalmente engolido por um aparelho televisor, entre tantas outras cenas igualmente impactantes.</p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>“... A Cia. Truks brinda o público paulistano, não é de hoje, com verdadeiras pérolas de encantamento cênico, à base de manipulação de bonecos. Agora apresenta Big Bang, uma espécie de história da evolução do mundo, sob a ótica muito particular e sensível da trupe. Os bonecos são cativantes, a iluminação cuidadosa, o casamento com a trilha sonora é harmonioso. Mais uma boa montagem para emocionar e fazer rir.”</p><h4><strong>DIB CARNEIRO NETO – O ESTADO DE SÃO PAULO - 23/06/2006</strong></h4><p>&nbsp;</p><p>“... O espetáculo, muito bem humorado, brinca com ideologias, conceitos e sátiras como a do impagável astronauta que conquista o espaço saltando de uma gangorra. A Cia. tem encantado as crianças com a maneira lúdica e inteligente de contar suas histórias. Agora chegou a vez de jovens e adultos. Deixe-se levar pela imaginação e... divirta-se!”</p><h4><strong>APOENAN RODRIGUES – REVISTA DA INDÚSTRIA - 01/06/2006</strong></h4><p>&nbsp;</p><p>“... A nova peça da Cia Truks é um programa divertido e conveniente para toda a família, atrai adultos e jovens com uma singela releitura da história da humanidade, a partir da explosão que teria originado o universo... Situações como a vida na pré história, a chegada do homem à Lua e o excesso de trabalho no mundo contemporâneo estão entre as poéticas e apuradas cenas do espetáculo...”</p><h4><strong>ANA PAULA BUCHALLA – REVISTA VEJA SP - 31/05/2006</strong></h4><p>&nbsp;</p><p>“... Durante anos a Cia Truks aprimorou a linguagem do teatro de bonecos e de animação. Acumulou prêmios, todos voltados para o público infantil. Agora, para comemorar os 15 anos do grupo, cria Big Bang, uma lúdica e crítica versão da gênese da humanidade. O aniversário é da trupe, mas o presente certamente é do público...”</p><h4><strong>BETH NÉSPOLI – O ESTADO DE SÃO PAULO - 11/05/2006</strong></h4>','<h2>FICHA TÉCNICA</h2><h4>&nbsp;</h4><h4>Texto, Concepção Geral e Dramaturgia:&nbsp;</h4><p>HENRIQUE SITCHIN E VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>Direção:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:&nbsp;</h4><p>AGUINALDO RODRIGUES, BIANCA MUNIZ, DRIELY PALÁCIO, GUILHERME CONRADI, STEFANY ARAÚJO e THAÍS ROSSI</p><p>&nbsp;</p><h4>Operação de som e iluminação:&nbsp;</h4><p>JOÃO SANTIAGO</p><p>&nbsp;</p><h4>Coordenação de Confecção de Cenografia, Bonecos e Formas Animadas:&nbsp;</h4><p>VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>Figurinos dos bonecos:&nbsp;</h4><p>VALÉRIA PERUSSO</p><p>&nbsp;</p><h4>Trilha Sonora:&nbsp;</h4><p>HENRIQUE SITCHIN</p>','2022-03-02 18:22:56','2022-03-13 17:48:34'),(23,15,'isto-nao-e-um-cachimbo-6_20220313152943oB3AagnX8M.jpg','isso-nao-e-um-cachimbo','ISSO NÃO É UM CACHIMBO','Espetáculo para adultos','<p><strong>ESPETÁCULO PARA ADULTOS</strong></p><p>Livremente inspirado na obra do pintor belga Renè Magritte, a peça \"dá vida\" às imagens desse grande mestre da arte surrealista. Já em uma das mais célebres obra do artista, intitulada “A Traição das Imagens”, vemos o desenho de um cachimbo com a legenda: “Isto Não é Um Cachimbo”. O artista nos propõe uma deliciosa brincadeira, ao apresentar uma imagem que imediatamente identificamos como sendo a de um cachimbo e, em fração de segundos, termos que negar a informação. Pois então, se um cachimbo não é um cachimbo, o que mais pode ser? Este é o jogo também do Teatro de Animação. As coisas podem não ser apenas aquilo que \"sabemos\" delas. Neste teatro, objetos carregam-se de muitos outros significados e simbologias possíveis. É assim que, por meio do sempre rigoroso apuro técnico da Cia Truks, figuras intrigantes saem das telas de Magritte para ganhar o palco, em cenas de forte impacto visual e conceitual: um velho homem, cujo peito é uma gaiola, despede-se da vida; Uma camisola ganha vida e reflete as dores da alma de sua dona, uma família de mortos vivos na varanda de casa, simbolizados por caixões, estão a esperar sabe-se lá o que, um homem às voltas com a ideia de tirar a própria vida, resolve trocar de cabeça, entre outras passagens deste espetáculo repleto de belas imagens e muita poesia.</p>',NULL,'<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Direção Geral e Coordenação dos Estudos de Dramaturgia:&nbsp;</h4><p>Henrique Sitchin</p><p>&nbsp;</p><h4>Direção de Animação:&nbsp;</h4><p>Verônica Gerchman</p><p>&nbsp;</p><h4>Elenco:&nbsp;</h4><p>Aguinaldo Rodrigues, Driely Palácio, Thaís Rossi, Rogério Uchoas e Gabriel Sitchin</p><p>&nbsp;</p><h4>Operação de som e iluminação:&nbsp;</h4><p>João Santiago</p><p>&nbsp;</p><p><br>&nbsp;</p>','2022-03-02 18:24:30','2022-03-13 18:29:44'),(24,16,'logo-fique-vivo-6_202203021528350fEvMs6ijP.jpg','fique-vivo','FIQUE VIVO','Teatro Jovem','<p>Em um eletrizante jogo-espetáculo, três misteriosos agentes&nbsp;da polícia científica&nbsp;invadem uma sala de aula e convocam os alunos para auxiliá-los em&nbsp;investigações urgentes. Eles trazem a notícia de três mortes sem aparentes explicações. A tarefa de todos os presentes será investigar cada um dos casos, e cada etapa cumprida do jogo/teatro revelará novas informações sobre os fatos ocorridos.&nbsp;Toda a experiência terá como fundo a necessidade de trabalho conjunto e consequente conscientização sobre questões voltadas à segurança no trânsito. A Cia. Truks, aqui, se utiliza dos jogos de escapada, e de toda a sua experiência no teatro, para&nbsp;criar uma atmosfera inédita, e que faz do jovem o protagonista máximo da experiência cênica.</p>',NULL,'<p>CRIAÇÃO E TEXTO: HENRIQUE SITCHIN E KARINA PRALL<br>ELENCO: HENRIQUE SITCHIN, GABRIEL SITCHIN E ROGÉRIO UCHOAS<br>PRODUÇÃO: MCD PRODUÇÕES</p>','2022-03-02 18:28:35','2022-03-02 18:28:35'),(25,17,'por-uma-estrela-2-foto-henrique-sitchin_20220302153140hwIhESQlW6.JPG','por-uma-estrela','POR UMA ESTRELA','Espetáculo para idosos','<p><strong>ESPETÁCULO PARA IDOSOS</strong></p><p>Neste<i><strong> </strong></i>espetáculo carregado de lirismo<i>,<strong> </strong></i>a Cia Truks conta uma bonita e tocante história de amor, destas capazes de emocionar todo aquele que tiver “um coração humano”. É a história de um casal de crianças que cresce junto em uma pequena aldeia de pescadores, que aprendem a amar um ao outro, mas que, de repente, são separados pelas contingências da vida. Veremos então o menino sofrer pela saudade e pela dor da separação. Uma longa vida irá passar até que ele se torne um homem idoso quando, então, decidirá ir atrás do seu amor, mostrando que a vida está sempre “começando”, e que cada dia é um novo dia. &nbsp;Aqui é feita uma bela homenagem à melhor idade, tempo em que se alcança o poder sobre a consciência, e a verdadeira sabedoria...</p>','<h2>A Imprensa comenta</h2><p>&nbsp;</p><p>\"É uma peça calminha, com ritmo lento, temática delicada, enfim, um verdadeiro oásis no mundo atual, por demais agitado e tecnológico. A história é bem romântica, com uma bela trilha sonora. Sitchin mais uma vez demonstra uma incrível coragem, pois não abre mão de sua linguagem sensível e de sua louvável meta como encenador: atingir o coração da plateia, menos do que a razão. Os manipuladores dos bonecos são tecnicamente muito bem preparados, mas nota-se que vão além da pura técnica e realmente sentem cada gesto que imprimem nos personagens. Quando os bonecos vão nadar no fundo do mar (panos transparentes), é perfeita a movimentação de braços e pernas, como se os bonecos estivessem mesmo mergulhando em águas profundas. Também é muito linda a solução para a passagem do tempo, utilizando o balanço do mar.\"</p><h4><strong>DIB CARNEIRO NETO - REVISTA CRESCER - 01/06/2006</strong></h4><p>&nbsp;</p><p>\"Com 22 anos de estrada, a Cia. Truks ganhou reconhecimento pelo ótimo trabalho na animação de bonecos e objetos. O grupo é responsável por êxitos como Zoo-Ilógico, A Bruxinha e Sonhatório — essa última volta ao cartaz no Sesc Vila Mariana no domingo (4). Em Por uma Estrela, o novo espetáculo exibido no Sesc Pompeia, a trupe retoma o bem-sucedido formato. Apesar da destreza dos manipuladores, o destaque aqui fica para a tocante história sem diálogos assinada pelo também diretor Henrique Sitchin\".</p><h4><strong>TATIANE ROSSET - REVISTA VEJA SP - 01/06/2006</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Texto:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Baseado em ideia original de:</h4><p><br>LUCIANA SEMENSATTO</p><p>&nbsp;</p><h4>Direção:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:</h4><p><br>AGUINALDO RODRIGUES<br>BIANCA MUNIZ<br>DRIELY PALÁCIO<br>JOÃO SANTIAGO<br>GUILHERME CONRADI<br>STEFANY ARAÚJO</p><p>&nbsp;</p><h4>Técnico de Som e Iluminação:</h4><p><br>JOÃO SANTIAGO</p><p>&nbsp;</p><h4>Coordenação de Confecção de Cenografia,<br>Bonecos e Formas Animadas:</h4><p><br>DALMIR ROGÉRIO PEREIRA</p><p>&nbsp;</p><h4>Confecção de Cenografia,<br>Bonecos e Formas Animadas:</h4><p><br>DALMIR ROGÉRIO PEREIRA, HENRIQUE SITCHIN,<br>KELY DE CASTRO, AILTON ROSA<br>ROGÉRIO UCHOAS, AGUINALDO RODRIGUES<br>JORGE MIYASHIRO e LUCIANA SEMENSATTO</p><p>&nbsp;</p><h4>Trilha Sonora:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Iluminação:</h4><p><br>HENRIQUE SITCHIN e AGUINALDO RODRIGUES</p><p>&nbsp;</p>','2022-03-02 18:31:42','2022-03-13 17:51:48'),(26,18,'espectaculo-cidade-azul-cia-truks_20220302153403fasABtm7PD.jpg','cidade-azul','CIDADE AZUL','Teatro para todas as idades','<p><strong>ATENÇÃO: ESTE ESPETÁCULO ESTÁ TEMPORARIAMENTE FORA DO REPERTÓRIO DA CIA TRUKS</strong></p><p>A premiada peça conta a bonita história do encontro acidental de duas crianças de realidades bem diferentes: uma menina rica, perdida pelas ruas, e um menino que vive nas ruas. Desenvolve-se entre os dois uma sólida e comovente amizade, que revela a sábia capacidade que as crianças têm de se aproximarem umas das outras, vencendo os preconceitos através de suas brincadeiras e jogos. As duas crianças <span style=\"color:black;\">oferecem-nos o sonho de uma cidade melhor: Uma Cidade Azul.</span></p>','<h2>A imprensa comenta</h2><p>&nbsp;</p><p>\"... Sucesso de público e crítica, a premiada peça Cidade Azul entra mais uma vez em cartaz, para encantar adultos e crianças. O enredo é de uma simplicidade que emociona! ... A manipulação dos bonecos pontua com delicadeza, graça e sofisticação, toda a apresentação...\"</p><h4><strong>Edilamar Galvão - Guia Da Folha - 20/03/1998</strong></h4><p>&nbsp;</p><p>\"... É a chance para as crianças levarem destas férias impressões de uma bela peça que prima, principalmente, pelo conteúdo do texto e pelo resultado plástico da animação dos bonecos... O espetáculo é tão delicado quanto alegre e surpreendente. E faz jus á conquista de diversos prêmios...\"</p><h4><strong>Ana Weiss - Jornal Da Tarde - 02/01/1998</strong></h4><p>&nbsp;</p><p>\"... Foi o ano da Cia. Truks. O impecável grupo de teatro de bonecos mostrou para as crianças espetáculos nota dez. Cidade Azul, uma singela visão da criança na grande cidade, fez chorar pais e filhos na platéia do centro Cultural São Paulo...\"</p><h4><strong>Dib Carneiro Neto - O Estado De São Paulo - 28/12/1997</strong></h4><p>&nbsp;</p><p>\"... A amizade, por enquanto, está vencendo. E ao queparece, vai vencer sempre, independente de condição econômica, raça ou coisas do gênero. É disso que fala o enredo de Cidade Azul, da Cia. Truks... A plateia se une em uma só torcida. Nada de gente grande ou pequena. Só gente feliz...\"</p><h4><strong>Pedro Autran Ribeiro - Jornal Da Tarde - 12/09/1997</strong></h4><p>&nbsp;</p><p>\"...CIDADE AZUL é um espetáculo imperdível. Conquista de imediato a criança menor, com seus truques e técnicas, revelando a surpreendente versatilidade da Cia. Truks\".</p><h4><strong>Mônica Rodrigues Da Costa - A Folha De São Paulo - 18/07/1997</strong></h4>','<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>CONCEPÇÃO GERAL E DIREÇÃO:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>CONCEPÇÃO E CONSTRUÇÃO<br>DE BONECOS E CENÁRIOS:</h4><p><br>VERÔNICA GERCHMAN<br>SANDRA GRASSO<br>VALÉRIA PERUSSO<br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>TRILHA SONORA:</h4><p><br>HENRIQUE SITCHIN<br>e ESTÚDIO \"FREQUÊNCIA RARA\"</p><p>&nbsp;</p><h4>VOZES DOS BONECOS GRAVADAS<br>PELAS (ENTÃO...) CRIANÇAS:</h4><p><br>JÚLIA POLYCARPO<br>GABRIEL SITCHIN<br>GABRIEL POLYCARPO</p><p>&nbsp;</p><h4>ILUMINAÇÃO:</h4><p><br>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>ELENCO:</h4><p><br>GUILHERME CONRADI<br>DRIELY PALÁCIO<br>THAÍS ROSSI<br>BIANCA MUNIZ<br>STEFANY ARAÚJO<br>AGUINALDO RODRIGUES</p><p>&nbsp;</p><h4>OPERAÇÃO DE SOM E ILUMINAÇÃO:</h4><p><br>JOÃO SANTIAGO</p>','2022-03-02 18:34:05','2022-03-13 17:54:30'),(27,19,'os-vizinhos_20220313145911PZ7k7ICtg6.jpg','os-vizinhos','OS VIZINHOS','Teatro para todas as idades','<p><strong>ATENÇÃO: ESTE ESPETÁCULO ESTÁ TEMPORARIAMENTE FORA DO REPERTÓRIO DA CIA. TRUKS</strong></p><p>A menina Clara precisa decidir entre emprestar seu melhor brinquedo à nova vizinha ou... brincar sozinha. Simples assim. E justamente na reflexão das possíveis sensações de Clara ao ter que se decidir, é que encontramos os conflitos e encruzilhadas de nosso tempo. Ou seremos mais solidários, e saberemos nos encontrar com o outro, e saberemos dividir, ou vamos viver sozinhos. Ou compartilhamos recursos ou perpetuamos nossas mazelas. O prazo para uma decisão, que tem a garota, talvez tenhamos todos nós, a cada momento do dia, em pequenas coisas. É aí que a avó conta para a menina a curiosa história de azuis e amarelos, que precisam uns dos outros para viver, mas que decretam uma guerra!</p>',NULL,'<h2>FICHA TÉCNICA</h2><p>&nbsp;</p><h4>Texto:&nbsp;</h4><p>HENRIQUE SITCHIN E VERÔNICA GERCHMAN</p><p>&nbsp;</p><h4>Direção:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Elenco:&nbsp;</h4><p>JOSÉ ANTÔNIO DO CARMO, AGUINALDO RODRIGUES, HELDER PARRA RIBEIRO, KELLY DE CASTRO, FÁBIO ROSA e CAMILA DE OLIVEIRA.</p><p>&nbsp;</p><h4>Técnico de Som e Iluminação:</h4><p>&nbsp;CLAUDEMIR SANTANA</p><p>&nbsp;</p><h4>Confecção de Cenografia, Bonecos e Formas Animadas:</h4><p>&nbsp;VERÔNICA GERCHMAN, HENRIQUE SITCHIN, JOSÉ ANTÔNIO DO CARMO, CAMILA PRIETTO, HELDER PARRA RIBEIRO, KELLY DE CASTRO, FÁBIO ROSA, AGUINALDO RODRIGUES, PAULO LOUREIRO Jr. e CAMILA DE OLIVEIRA</p><p>&nbsp;</p><h4>Figurinos dos bonecos:&nbsp;</h4><p>VALÉRIA PERUSSO</p><p>&nbsp;</p><h4>Desenhos:&nbsp;</h4><p>ÁLVARO MORAU</p><p>&nbsp;</p><h4>Trilha Sonora:&nbsp;</h4><p>HENRIQUE SITCHIN</p><p>&nbsp;</p><h4>Iluminação:&nbsp;</h4><p>HENRIQUE SITCHIN e CLAUDEMIR SANTANA</p>','2022-03-02 18:36:04','2022-03-13 17:59:36');
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_home`
--

DROP TABLE IF EXISTS `espetaculos_home`;
CREATE TABLE `espetaculos_home` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_home_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_home_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_home`
--

LOCK TABLES `espetaculos_home` WRITE;
INSERT INTO `espetaculos_home` VALUES (9,9,0,'2022-03-02 19:29:27','2022-03-02 19:29:27'),(10,14,0,'2022-03-02 19:29:41','2022-03-02 19:29:41'),(11,12,0,'2022-03-02 19:29:55','2022-03-02 19:29:55');
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_imagens`
--

DROP TABLE IF EXISTS `espetaculos_imagens`;
CREATE TABLE `espetaculos_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_imagens_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_imagens_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_imagens`
--

LOCK TABLES `espetaculos_imagens` WRITE;
INSERT INTO `espetaculos_imagens` VALUES (10,9,3,'a-bruxinha-foto-alberto-rocha_20220302142714VgkK9n37qD.JPG','2022-03-02 17:27:15','2022-03-02 17:27:15'),(11,9,2,'a-bruxinha-foto-alberto-rocha-a_202203021427157eTVIOLDIZ.jpg','2022-03-02 17:27:16','2022-03-02 17:27:16'),(12,9,1,'a-bruxinha-foto-alberto-rocha-2_20220302142715fSIhTYkijG.jpg','2022-03-02 17:27:17','2022-03-02 17:27:17'),(13,9,4,'a-bruxinha-foto-vlademir-canela-1_20220302142715c8iUk9F6el.jpg','2022-03-02 17:27:17','2022-03-02 17:27:17'),(14,9,10,'bruxinha-com-principe_202203021427171SeYUvZLAf.JPG','2022-03-02 17:27:17','2022-03-02 17:27:17'),(15,9,9,'bruxinha-com-leao-1_20220302142717tw2dVn7suA.JPG','2022-03-02 17:27:17','2022-03-02 17:27:17'),(17,9,7,'a-bruxinha-foto-vlademir-canela-4_20220302142716ZsohnDixze.jpg','2022-03-02 17:27:19','2022-03-02 17:27:19'),(18,9,12,'espetaculo-a-bruxinha-cia-truks-foto-alberto-rocha-1-2_20220302142718bfmax45ELK.jpg','2022-03-02 17:27:19','2022-03-02 17:27:19'),(19,9,11,'espetaculo-a-bruxinha-cia-truks-foto-alberto-rocha-1-1_202203021427183Ftg1wIVCJ.jpg','2022-03-02 17:27:19','2022-03-02 17:27:19'),(20,9,5,'a-bruxinha-foto-vlademir-canela-2_20220302142715GZOv8g2Wnf.jpg','2022-03-02 17:27:19','2022-03-02 17:27:19'),(21,9,6,'a-bruxinha-foto-vlademir-canela-3_20220302142715bOJuoPyAit.jpg','2022-03-02 17:27:19','2022-03-02 17:27:19'),(22,9,13,'espetaculo-a-bruxinha-cia-truks-foto-alberto-rocha-1-3_20220302142719YS4EI9adR9.jpg','2022-03-02 17:27:20','2022-03-02 17:27:20'),(24,10,0,'espetaculo-o-senhor-dos-sonhos-foto-alberto-rocha-2_20220302143300FybozIkBLZ.jpg','2022-03-02 17:33:01','2022-03-02 17:33:01'),(26,10,0,'o-senhor-dos-sonhos-foto-thiago-cardinalli-2_202203021433026EPphzNGrk.jpg','2022-03-02 17:33:03','2022-03-02 17:33:03'),(28,10,0,'o-senhor-dos-sonhos-foto-thiago-cardinali-2_20220302143301QCuHvzd21Y.jpg','2022-03-02 17:33:04','2022-03-02 17:33:04'),(29,10,0,'o-senhor-dos-sonhos-foto-thiago-cardinali-b_20220302143302cIQqUjpCDo.jpg','2022-03-02 17:33:05','2022-03-02 17:33:05'),(30,10,0,'o-senhor-dos-sonhos-foto-thiago-cardinalli_20220302143302FJdFSKiPuX.jpg','2022-03-02 17:33:05','2022-03-02 17:33:05'),(31,10,0,'o-senhor-dos-sonhos-foto-thiago-cardinali-3_20220302143301T0AT63eS0H.jpg','2022-03-02 17:33:05','2022-03-02 17:33:05'),(32,10,0,'o-senhor-dos-sonhos-1-foto-henrique-sitchin_202203021433042PTe7iXKGF.jpg','2022-03-02 17:33:05','2022-03-02 17:33:05'),(33,10,0,'o-senhor-dos-sonhos-1-foto-thiago-cardinalli_202203021433044S7jW0XRRs.jpg','2022-03-02 17:33:05','2022-03-02 17:33:05'),(34,10,0,'o-senhor-dos-sonhos-2-imagem-henrique-sitchin_20220302143306Z1Q7ZdP7rQ.jpg','2022-03-02 17:33:06','2022-03-02 17:33:06'),(35,10,0,'o-senhor-dos-sonhos-1_20220302143305YEZQMwxE3J.jpg','2022-03-02 17:33:07','2022-03-02 17:33:07'),(37,10,0,'o-senhor-dos-sonhos-3-foto-henrique-sitchin_20220302143306bYGV8GMHbw.jpg','2022-03-02 17:33:07','2022-03-02 17:33:07'),(38,10,0,'o-senhor-dos-sonhos-3-foto-thiago-cardinali_20220302143306J30mZQ3OWr.jpg','2022-03-02 17:33:08','2022-03-02 17:33:08'),(39,10,0,'o-senhor-dos-sonhos-b_20220302143308AqRkZbjnaj.JPG','2022-03-02 17:33:09','2022-03-02 17:33:09'),(41,10,0,'o-senhor-dos-sonhos-199_20220302143308w27rmm0Kd7.jpg','2022-03-02 17:33:10','2022-03-02 17:33:10'),(42,10,0,'o-senhor-dos-sonhos-2_20220302143309h4SDwe3Ffr.jpg','2022-03-02 17:33:10','2022-03-02 17:33:10'),(43,10,0,'o-senhor-dos-sonhos-4-foto-henrique-sitchin_202203021433107HxRjpi4v2.jpg','2022-03-02 17:33:11','2022-03-02 17:33:11'),(44,10,0,'o-senhor-dos-sonhos-bb_20220302143310bEgz9JcqpJ.jpg','2022-03-02 17:33:11','2022-03-02 17:33:11'),(46,10,0,'o-senhor-dos-sonhos-c_20220302143310Wbjn8P48Ya.jpg','2022-03-02 17:33:12','2022-03-02 17:33:12'),(47,10,0,'o-senhor-dos-sonhos-foto-3_20220302143312A7TXeggKRv.jpg','2022-03-02 17:33:15','2022-03-02 17:33:15'),(48,10,0,'o-senhor-dos-sonhos-foto-2_20220302143311veVKVnsTt6.jpg','2022-03-02 17:33:15','2022-03-02 17:33:15'),(49,10,0,'o-senhor-dos-sonhos-foto-1_20220302143311TuwqmusbPh.jpg','2022-03-02 17:33:16','2022-03-02 17:33:16'),(50,10,0,'o-senhor-dos-sonhos-foto-4_20220302143312AcK2A0V1Fe.jpg','2022-03-02 17:33:18','2022-03-02 17:33:18'),(52,11,5,'vovo-cia-truks-foto-henrique-sitchin_20220302143840K4JLrRknZm.jpg','2022-03-02 17:38:41','2022-03-02 17:38:41'),(53,11,3,'espetaculo-vovo-cia-truks-foto-alberto-rocha-2_20220302143841T86NWcxPiL.jpg','2022-03-02 17:38:41','2022-03-02 17:38:41'),(54,11,2,'espetaculo-vovo-3-cia-truks-foto-alberto-rocha_20220302143841MRSFyMgys0.jpg','2022-03-02 17:38:42','2022-03-02 17:38:42'),(55,11,1,'espetaculo-vovo-cia-truks-foto-alberto-rocha-3_20220302143841MEEnOIQUMH.jpg','2022-03-02 17:38:42','2022-03-02 17:38:42'),(56,11,6,'vovo-1-cia-truks-foto-henrique-sitchin_20220302143841sbJMZaAdQP.jpg','2022-03-02 17:38:42','2022-03-02 17:38:42'),(57,11,4,'vovo-foto-henrique-sitchin_20220302143841V94KfyeUU1.jpg','2022-03-02 17:38:42','2022-03-02 17:38:42'),(61,11,8,'vovo-3-cia-truks-foto-henrique-sitchin_20220302143842C2H7O6xIVZ.jpg','2022-03-02 17:38:45','2022-03-02 17:38:45'),(62,11,9,'vovo-4-cia-truks-foto-henrique-sitchin_202203021438436cHkCEGZFK.jpg','2022-03-02 17:38:45','2022-03-02 17:38:45'),(63,11,7,'vovo-foto-2_20220302143843RWeMVadtz4.jpg','2022-03-02 17:38:45','2022-03-02 17:38:45'),(64,11,10,'vovo-foto-3_20220302143845ADnNeGerXE.jpg','2022-03-02 17:38:46','2022-03-02 17:38:46'),(65,12,9,'zooilogico-5-foto-pietro-feliciano_202203021454284OnbqJmsUr.jpg','2022-03-02 17:54:29','2022-03-02 17:54:29'),(66,12,1,'zoo-ilogico-2-foto-pietro-feliciano_20220302145428oX4W3uz7kL.jpg','2022-03-02 17:54:29','2022-03-02 17:54:29'),(67,12,8,'zooilogico-3-foto-pietro-feliciano_20220302145428wLlsxMILBY.jpg','2022-03-02 17:54:29','2022-03-02 17:54:29'),(68,12,10,'zoo-ilogico-1-foto-pietro-feliciano_20220302145428oQPFi6wM8B.JPG','2022-03-02 17:54:29','2022-03-02 17:54:29'),(69,12,5,'zoo-ilogico-4-foto-pietro-feliciano_20220302145428mzyoHxXhRG.jpg','2022-03-02 17:54:29','2022-03-02 17:54:29'),(70,12,2,'zoo-ilogico-2018-a-foto-nathalya-moleda_20220302145428JGmhrSrL6u.JPG','2022-03-02 17:54:33','2022-03-02 17:54:33'),(71,12,3,'zoo-ilogico-2018-b-foto-nathalya-moleda_20220302145429cs3SEpBUXZ.JPG','2022-03-02 17:54:34','2022-03-02 17:54:34'),(72,12,6,'zoo-ilogico-2018-c-foto-nathalya-moleda_20220302145430L5r9eAsaeb.JPG','2022-03-02 17:54:35','2022-03-02 17:54:35'),(73,12,7,'zoo-ilogico-2018-d-foto-nathalya-moleda_20220302145430wA8VQjpOwt.JPG','2022-03-02 17:54:35','2022-03-02 17:54:35'),(74,12,4,'zoo-ilogico-2018-e-foto-nathalya-moleda_20220302145431rIrbhRx4rG.JPG','2022-03-02 17:54:35','2022-03-02 17:54:35'),(76,13,4,'e-se-as-historias-fossem-diferentes-3_20220302145653w9xRu5giq9.JPG','2022-03-02 17:56:53','2022-03-02 17:56:53'),(77,13,2,'e-se-as-historias-fossem-diferentes-9-foto-pietro-s-feliciano_20220302145653rBYmUnbG1s.JPG','2022-03-02 17:56:53','2022-03-02 17:56:53'),(78,13,3,'e-se-as-historias-fossem-diferentes-2_20220302145653kk3OvrhYd0.jpg','2022-03-02 17:56:54','2022-03-02 17:56:54'),(79,13,1,'e-se-as-historias-fossem-diferentes-1-foto-pietro-feliciano_20220302145653Oy5NqW1lgc.JPG','2022-03-02 17:56:55','2022-03-02 17:56:55'),(80,14,12,'espetaculo-sonhatorio-2-cia-truks-foto-fred-gustavos_20220302145940lG4uA59H8I.jpg','2022-03-02 17:59:40','2022-03-02 17:59:40'),(81,14,11,'dsc6603_20220302145940aAMT9JCIkh.jpg','2022-03-02 17:59:40','2022-03-02 17:59:40'),(82,14,3,'espetaculo-sonhatorio-3-cia-truks-foto-fred-gustavos_20220302145940UJ2SVwEL5I.jpg','2022-03-02 17:59:40','2022-03-02 17:59:40'),(83,14,15,'psf3032-2_20220302145940vJHjVXNLBH.jpg','2022-03-02 17:59:41','2022-03-02 17:59:41'),(84,14,1,'sem-titulo-1_20220302145940ghQcQjT9mD.jpg','2022-03-02 17:59:41','2022-03-02 17:59:41'),(85,14,7,'espetaculo-sonhatorio-4-cia-truks-foto-fred-gustavos_20220302145940qQYQC0lHUj.jpg','2022-03-02 17:59:41','2022-03-02 17:59:41'),(87,14,13,'sonhatorio-1-cia-truks-foto-alberto-rocha_20220302145941mMrG25NRdo.jpg','2022-03-02 17:59:42','2022-03-02 17:59:42'),(89,14,16,'sonhatorio-2-foto-pietro-feliciano_20220302145943DlPZvOJiCJ.jpg','2022-03-02 17:59:43','2022-03-02 17:59:43'),(91,14,6,'sonhatorio-1-foto-heloisa-bortz_20220302145942GMRKZ0K02h.jpg','2022-03-02 17:59:44','2022-03-02 17:59:44'),(92,14,18,'sonhatorio-3-foto-pietro-feliciano_202203021459446qzRtlJQ1G.jpg','2022-03-02 17:59:44','2022-03-02 17:59:44'),(93,14,10,'sonhatorio-3-cia-truks-foto-alberto-rocha_20220302145943tjInS4ycSk.jpg','2022-03-02 17:59:44','2022-03-02 17:59:44'),(94,14,17,'sonhatorio-3-foto-heloisa-bortz_20220302145944w5ryVezmyx.jpg','2022-03-02 17:59:45','2022-03-02 17:59:45'),(95,14,5,'sonhatorio-4-cia-truks-foto-henrique-sitchin_20220302145944Z3YOybwI2c.jpg','2022-03-02 17:59:45','2022-03-02 17:59:45'),(96,14,4,'sonhatorio-5-cia-truks-foto-alberto-rocha_20220302145945SnnxOea2mH.jpg','2022-03-02 17:59:45','2022-03-02 17:59:45'),(100,14,8,'sonhatorio-1-foto-henrique-sitchin-cia-truks_20220302145942wnLU4hYEeb.JPG','2022-03-02 17:59:47','2022-03-02 17:59:47'),(104,15,1,'construtorio-10-web_20220302150314Ldaj6cPr4i.jpg','2022-03-02 18:03:14','2022-03-02 18:03:14'),(105,15,6,'construtorio-9-red_20220302150314ndP9xMxitI.jpg','2022-03-02 18:03:14','2022-03-02 18:03:14'),(106,15,4,'construtorio-foto-alberto-greiber_20220302150314QhU708cEKT.jpg','2022-03-02 18:03:15','2022-03-02 18:03:15'),(107,15,3,'construtorio-foto-alberto-greiber-2_20220302150314gqqhgDxcf0.jpg','2022-03-02 18:03:15','2022-03-02 18:03:15'),(108,15,2,'construtorio-5-web_20220302150316ZGhfNNX5Ab.jpg','2022-03-02 18:03:16','2022-03-02 18:03:16'),(109,15,5,'construtorio-2-foto-henrique-sitchin_20220302150314DMtEAZw8OY.jpg','2022-03-02 18:03:19','2022-03-02 18:03:19'),(110,16,5,'acampatorio-2-foto-alberto-rocha-1_20220302150459DrBhI5Ov8J.jpg','2022-03-02 18:05:00','2022-03-02 18:05:00'),(111,16,3,'acampatorio-05-foto-alberto-rocha_202203021504591kaFbiPEl2.jpg','2022-03-02 18:05:00','2022-03-02 18:05:00'),(112,16,6,'acampatorio-11-foto-alberto-rocha_202203021504596BMLXfk6NA.jpg','2022-03-02 18:05:01','2022-03-02 18:05:01'),(113,16,8,'acampatorio-10-foto-alberto-rocha_20220302150500Kes2VOyma3.jpg','2022-03-02 18:05:01','2022-03-02 18:05:01'),(114,16,7,'acampatorio-3-foto-alberto-rocha_20220302150500VLp0lBUJ4L.jpg','2022-03-02 18:05:01','2022-03-02 18:05:01'),(115,16,2,'acampatorio-04-foto-alberto-rocha_20220302150459BMYljQpbMh.jpg','2022-03-02 18:05:01','2022-03-02 18:05:01'),(116,16,1,'acampatorio-foto-alberto-rocha-2_20220302150500mhxd0cmPA5.jpg','2022-03-02 18:05:01','2022-03-02 18:05:01'),(117,16,4,'acampatorio-85_20220302150501Onl1oQDlAU.jpg','2022-03-02 18:05:02','2022-03-02 18:05:02'),(118,16,9,'acampatorio-foto-alberto-rocha-3_20220302150501eaae8ixfXP.jpg','2022-03-02 18:05:02','2022-03-02 18:05:02'),(119,17,0,'img-9975_20220302150747TMgOHurStU.jpg','2022-03-02 18:07:47','2022-03-02 18:07:47'),(120,17,0,'img-9987_20220302150747I4YbBNvUja.JPG','2022-03-02 18:07:48','2022-03-02 18:07:48'),(122,17,0,'espetaculo-ultima-noticia-2-foto-alberto-rocha_20220302150748kVUd6HJQd5.jpg','2022-03-02 18:07:49','2022-03-02 18:07:49'),(123,17,0,'img-9983_20220302150748D34cKEK7cM.JPG','2022-03-02 18:07:49','2022-03-02 18:07:49'),(124,17,0,'espetaculo-ultima-noticia-3-foto-alberto-rocha_20220302150748SMGmoXHnjt.jpg','2022-03-02 18:07:49','2022-03-02 18:07:49'),(125,17,0,'img-9999_20220302150748rH17c6zKEF.JPG','2022-03-02 18:07:49','2022-03-02 18:07:49'),(126,18,3,'08e18142-8f42-40fd-bd0e-cd87afa5a0ae_20220302150949R0NTqfWA1F.jpg','2022-03-02 18:09:50','2022-03-02 18:09:50'),(127,18,11,'266a21c3-6a7c-4054-8695-7dbced599685_20220302150949QhanwO1y6I.jpg','2022-03-02 18:09:50','2022-03-02 18:09:50'),(129,18,8,'86b86311-4653-4f6a-9fa3-90b3dd9cebe3_20220302150949yK9oNgBBNh.jpg','2022-03-02 18:09:50','2022-03-02 18:09:50'),(130,18,5,'expedicao-pacifico-cia-truks-c_20220302150949FXgWH6w7kb.JPG','2022-03-02 18:09:50','2022-03-02 18:09:50'),(131,18,14,'expedicao-pacifico-cia-truks-b_20220302150950YdXfqClgWV.JPG','2022-03-02 18:09:50','2022-03-02 18:09:50'),(132,18,6,'expedicao-pacifico-cia-truks-d_20220302150950qBuXrmIffR.JPG','2022-03-02 18:09:51','2022-03-02 18:09:51'),(133,18,9,'expedicao-pacifico-cia-truks-e_20220302150950N0JPKGrzaB.JPG','2022-03-02 18:09:51','2022-03-02 18:09:51'),(134,18,15,'expedicao-pacifico-1-foto-alberto-rocha-atores-gabriel-sitchin-e-rogerio-uchoas_20220302150951xXegBZqtqV.jpg','2022-03-02 18:09:51','2022-03-02 18:09:51'),(135,18,12,'expedicao-pacifico-2-foto-alberto-rocha_20220302150951WRZcuMMLIm.jpg','2022-03-02 18:09:52','2022-03-02 18:09:52'),(136,18,13,'expedicao-pacifico-3-foto-alberto-rocha_20220302150951zXlwWDr8Bh.jpg','2022-03-02 18:09:52','2022-03-02 18:09:52'),(137,18,17,'expedicao-pacifico-foto-alberto-rocha-3_20220302150951jPtaAkcgUX.jpg','2022-03-02 18:09:52','2022-03-02 18:09:52'),(138,18,4,'expedicao-pacifico-foto-alberto-rocha_20220302150951XOfN1NggH7.jpg','2022-03-02 18:09:52','2022-03-02 18:09:52'),(139,18,7,'expedicao-pacifico-5-foto-alberto-rocha_20220302150952z4s21pwZ8Y.jpg','2022-03-02 18:09:52','2022-03-02 18:09:52'),(141,18,19,'expedicao-pacifico-8-foto-alberto-rocha_202203021509530xFkZgTM6v.jpg','2022-03-02 18:09:54','2022-03-02 18:09:54'),(142,18,2,'expedicao-pacifico-9-foto-alberto-rocha_20220302150953Bh2LxFNZRE.jpg','2022-03-02 18:09:54','2022-03-02 18:09:54'),(146,19,7,'isso-e-coisa-de-crianca-foto-lazerum_202203021514157wacjvGcqP.jpg','2022-03-02 18:14:16','2022-03-02 18:14:16'),(147,19,4,'isso-e-coisa-de-crianca-2-foto-lazerum_20220302151416WCPcKJc4pz.jpg','2022-03-02 18:14:18','2022-03-02 18:14:18'),(149,19,6,'isso-e-coisa-de-crianca-1-foto-alberto-rocha_20220302151416OBjsq7ymOR.jpg','2022-03-02 18:14:18','2022-03-02 18:14:18'),(152,19,14,'isso-e-coisa-de-crianca-3a-foto-alberto-rocha_20220302151420VyFobSMoSt.jpg','2022-03-02 18:14:22','2022-03-02 18:14:22'),(153,19,10,'isso-e-coisa-de-crianca-2-foto-alberto-rocha_20220302151419qPlp3D5r5O.jpg','2022-03-02 18:14:22','2022-03-02 18:14:22'),(156,19,15,'isso-e-coisa-de-crianca-3_20220302151420j2BCSmFTus.JPG','2022-03-02 18:14:22','2022-03-02 18:14:22'),(157,19,3,'isso-e-coisa-de-crianca-3-foto-alberto-rocha_202203021514208ReSSgTxi3.jpg','2022-03-02 18:14:22','2022-03-02 18:14:22'),(159,19,12,'isso-e-coisa-de-crianca-4_202203021514244WpcUMyzLo.JPG','2022-03-02 18:14:27','2022-03-02 18:14:27'),(160,19,16,'isso-e-coisa-de-crianca-5-foto-alberto-rocha_20220302151425oMtpC7grgc.jpg','2022-03-02 18:14:27','2022-03-02 18:14:27'),(161,19,1,'isso-e-coisa-de-crianca-4-foto-alberto-rocha_20220302151425KCkJbdgD7n.jpg','2022-03-02 18:14:27','2022-03-02 18:14:27'),(162,19,5,'isso-e-coisa-de-crianca-4-foto-alberto-rocha_20220302151425iguacIklRb.jpg','2022-03-02 18:14:27','2022-03-02 18:14:27'),(163,19,18,'isso-e-coisa-de-crianca-5-foto-alberto-rocha_20220302151425opd5yWDam0.jpg','2022-03-02 18:14:27','2022-03-02 18:14:27'),(165,19,19,'isso-e-coisa-de-crianca-6-foto-alberto-rocha_202203021514264Saybh7I7S.jpg','2022-03-02 18:14:29','2022-03-02 18:14:29'),(167,19,2,'isso-e-coisa-de-crianca-e-foto-alberto-rocha_20220302151428F0yxlLGO61.jpg','2022-03-02 18:14:30','2022-03-02 18:14:30'),(170,20,1,'60341524-1225071620980592-8102522186918526976-o_20220302151935CuE2fjLQGY.jpg','2022-03-02 18:19:35','2022-03-02 18:19:35'),(171,20,2,'60356331-1225070770980677-6228259726652604416-o_202203021519357Bm1GOpZ54.jpg','2022-03-02 18:19:35','2022-03-02 18:19:35'),(172,20,5,'60337307-1225070704314017-156463953491263488-o_20220302151935swbVVdZiYs.jpg','2022-03-02 18:19:35','2022-03-02 18:19:35'),(173,20,12,'img-1524_2022030215193538GvKGxQIn.JPG','2022-03-02 18:19:39','2022-03-02 18:19:39'),(174,20,6,'img-1492_20220302151935OUtQrksQCh.JPG','2022-03-02 18:19:40','2022-03-02 18:19:40'),(175,20,7,'img-1503_20220302151935pDn4zjZIFE.JPG','2022-03-02 18:19:40','2022-03-02 18:19:40'),(176,20,10,'img-1614_20220302151937gW87NGhfr5.JPG','2022-03-02 18:19:41','2022-03-02 18:19:41'),(177,20,8,'img-1578_20220302151937UMZYpSIOxd.JPG','2022-03-02 18:19:41','2022-03-02 18:19:41'),(178,20,11,'img-1599_20220302151937hiORpF2Yym.JPG','2022-03-02 18:19:41','2022-03-02 18:19:41'),(179,20,4,'img-1622_20220302151940u1faaNOg7K.JPG','2022-03-02 18:19:44','2022-03-02 18:19:44'),(180,20,9,'img-1623_20220302151940KETnXbdGsr.JPG','2022-03-02 18:19:45','2022-03-02 18:19:45'),(181,20,3,'img-1650_20220302151941gunTngkl2Q.JPG','2022-03-02 18:19:45','2022-03-02 18:19:45'),(182,21,0,'historia-de-bar-1_20220302152135I9gLlAZkTl.jpg','2022-03-02 18:21:36','2022-03-02 18:21:36'),(183,21,0,'historia-de-bar-3_20220302152135nb0uSVDuuw.jpg','2022-03-02 18:21:36','2022-03-02 18:21:36'),(184,21,0,'historia-de-bar-2_20220302152135uxDrWDICVS.jpg','2022-03-02 18:21:36','2022-03-02 18:21:36'),(186,22,5,'big-bang-2-foto-paulo-brasil_202203021523073H1sVXN5Sj.jpg','2022-03-02 18:23:07','2022-03-02 18:23:07'),(187,22,2,'big-bang-3-foto-paulo-brasil_20220302152307plZl4YbpGf.jpg','2022-03-02 18:23:07','2022-03-02 18:23:07'),(188,22,3,'big-bang-1-foto-paulo-brasil_202203021523079ftYoKDb4j.jpg','2022-03-02 18:23:08','2022-03-02 18:23:08'),(189,22,1,'big-bang-1-foto-henrique-sitchin-2_20220302152307U4bLXMQyLm.jpg','2022-03-02 18:23:09','2022-03-02 18:23:09'),(190,22,4,'big-bang-2-foto-henrique-sitchin_20220302152308N1JoG3o1Gz.jpg','2022-03-02 18:23:09','2022-03-02 18:23:09'),(191,23,0,'isto-nao-e-um-cachimbo-13_2022030215245495YpzAN4xj.jpg','2022-03-02 18:24:56','2022-03-02 18:24:56'),(192,23,0,'isto-nao-e-um-cachimbo-cia-truks_20220302152454WhhjyNSV5K.jpg','2022-03-02 18:24:57','2022-03-02 18:24:57'),(194,25,6,'por-uma-estrela-5-foto-henrique-sitchin_20220302153206TyHV62XE8F.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(195,25,2,'por-uma-estrela-2-foto-henrique-sitchin_20220302153206B5elihZuwW.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(196,25,1,'por-uma-estrela-1_20220302153206IOGfzmODKg.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(197,25,7,'por-uma-estrela-7-foto-henrique-sitchin_20220302153206S8X68E5rxS.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(198,25,4,'por-uma-estrela-6_20220302153206WvcNq8tTF9.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(199,25,5,'por-uma-estrela-10-foto-henrique-sitchin_20220302153207jxsUsvCncz.JPG','2022-03-02 18:32:08','2022-03-02 18:32:08'),(200,25,3,'por-uma-estrela-12-foto-henrique-sitchin_20220302153209US5pWUjFAK.JPG','2022-03-02 18:32:10','2022-03-02 18:32:10'),(201,25,8,'por-uma-estrela-foto-henrique-sitchin_20220302153209Oep2sFFgA7.JPG','2022-03-02 18:32:10','2022-03-02 18:32:10'),(202,26,3,'cidade-azul-1_20220302153423EEJySu0P56.jpg','2022-03-02 18:34:23','2022-03-02 18:34:23'),(203,26,2,'cidade-azul-3_20220302153423QzWBxM4Zi5.JPG','2022-03-02 18:34:24','2022-03-02 18:34:24'),(204,26,1,'cidade-azul-2-foto-henrique-sitchin-2_20220302153423SSdACXjgGI.jpg','2022-03-02 18:34:24','2022-03-02 18:34:24'),(205,26,4,'cidade-azul-2_20220302153423QamBi32Qbq.JPG','2022-03-02 18:34:25','2022-03-02 18:34:25'),(206,26,5,'espectaculo-cidade-azul-cia-truks_2022030215342437tHxzS9pl.jpg','2022-03-02 18:34:26','2022-03-02 18:34:26'),(207,27,0,'os-vizinhos-red-c_20220302153622XcFldYLlQd.jpg','2022-03-02 18:36:23','2022-03-02 18:36:23'),(208,27,0,'os-vizinhos-red-7_20220302153622ay22O1rwFE.jpg','2022-03-02 18:36:23','2022-03-02 18:36:23'),(209,27,0,'os-vizinhos-red-3_20220302153622caWZjo28Rv.jpg','2022-03-02 18:36:23','2022-03-02 18:36:23'),(210,27,0,'os-vizinhos-red-6_20220302153622m8UXJdVisl.jpg','2022-03-02 18:36:23','2022-03-02 18:36:23'),(211,27,0,'os-vizinhos-foto-henrique-sitchin_20220302153622HI47lXHraQ.jpg','2022-03-02 18:36:23','2022-03-02 18:36:23'),(212,19,0,'isso-e-coisa-de-crianca-d-foto-alberto-rocha_202203131413091EUqzvrUOO.jpg','2022-03-13 17:13:10','2022-03-13 17:13:10'),(213,23,0,'isto-nao-e-um-cachimbo-9_20220313153106tc9QHNSWax.jpg','2022-03-13 18:31:06','2022-03-13 18:31:06'),(214,23,0,'isto-nao-e-um-cachimbo-2_20220313153126B0mm1KNuZv.jpg','2022-03-13 18:31:26','2022-03-13 18:31:26'),(215,23,0,'isto-nao-e-um-cachimbo-4_20220313153133lvfP1T1wEW.jpg','2022-03-13 18:31:34','2022-03-13 18:31:34'),(216,23,0,'isto-nao-e-um-cachimbo-7_20220313153142EpkW4JkKxb.jpg','2022-03-13 18:31:43','2022-03-13 18:31:43'),(217,23,0,'isto-nao-e-um-cachimbo-8_20220313153149ZcC2jmlVaY.jpg','2022-03-13 18:31:49','2022-03-13 18:31:49');
UNLOCK TABLES;

--
-- Table structure for table `espetaculos_videos`
--

DROP TABLE IF EXISTS `espetaculos_videos`;
CREATE TABLE `espetaculos_videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `espetaculos_videos_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `espetaculos_videos_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `espetaculos_videos`
--

LOCK TABLES `espetaculos_videos` WRITE;
INSERT INTO `espetaculos_videos` VALUES (3,9,0,'nSkWHloIAuA','2022-03-05 15:13:30','2022-03-05 15:13:30'),(4,10,0,'OL5Q6os3PAg','2022-03-05 15:20:40','2022-03-05 15:20:40'),(5,14,0,'x9brpRLg-Ww','2022-03-08 16:47:25','2022-03-08 16:47:25'),(6,15,0,'3ju3XYpc6-E','2022-03-08 16:52:25','2022-03-08 16:52:25'),(7,16,0,'cc7X8CepaM4','2022-03-08 16:57:07','2022-03-08 16:57:07'),(8,17,0,'xxlEtZ4fs18','2022-03-08 17:08:10','2022-03-08 17:08:10'),(9,18,0,'r5Skpl9meBU','2022-03-08 17:12:55','2022-03-08 17:12:55'),(10,19,0,'6SeC3D0ws34','2022-03-08 17:17:32','2022-03-08 17:17:32'),(11,21,0,'X_nZ4SqET7Q','2022-03-08 19:12:19','2022-03-08 19:12:19'),(12,22,0,'kqcly_8qgGk','2022-03-08 19:15:40','2022-03-08 19:15:40'),(13,23,0,'FvWxoP1d_KA','2022-03-08 19:19:46','2022-03-08 19:19:46'),(14,25,0,'NvGMaHGYe3w','2022-03-08 19:25:05','2022-03-08 19:25:05');
UNLOCK TABLES;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `espetaculo_id` bigint(20) unsigned DEFAULT NULL,
  `data` date NOT NULL,
  `horario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cidade_uf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gratuito` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo_link_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agenda_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `agenda_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
INSERT INTO `agenda` VALUES (14,13,'2022-01-03','1500','Teatro Central Vip','Rua Cavalcanti Moly','Chapada Verde','INDAIATUBA','19 9393 9393',0,'https://www.trupe.net/previa-ciatruks/contato','CONTATO','FALE CONOSCO','EVENTO PATROCINADO','2022-03-02 20:52:48','2022-03-02 20:52:48'),(15,14,'2022-03-06','11h00','SESC IPIRANGA - SP','R. Bom Pastor, 822 - Ipiranga, São Paulo - SP, 04203-001','IPIRANGA','SÃO PAULO - SP',NULL,0,'https://trupe.net/previa-ciatruks/espetaculos/sonhatorio','SONHATÓRIO','ESPETÁCULO PARA TODA A FAMÍLIA',NULL,'2022-03-05 14:59:10','2022-03-05 14:59:10'),(16,9,'2022-04-02','16h','SESC PIRACICABA',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2022-03-31 12:54:59','2022-03-31 12:54:59'),(17,14,'2022-04-02','16H','SESC MOGI DAS CRUZES',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2022-03-31 12:56:14','2022-03-31 12:56:14'),(18,18,'2022-04-02','12H00','SESC SANTO ANDRÉ',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2022-03-31 12:57:03','2022-03-31 12:57:03');
UNLOCK TABLES;

--
-- Table structure for table `depoimentos`
--

DROP TABLE IF EXISTS `depoimentos`;
CREATE TABLE `depoimentos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `espetaculo_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `depoimentos_espetaculo_id_foreign` (`espetaculo_id`),
  CONSTRAINT `depoimentos_espetaculo_id_foreign` FOREIGN KEY (`espetaculo_id`) REFERENCES `espetaculos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

alter table mensagens change texto mensagem text COLLATE utf8mb4_unicode_ci NOT NULL;
alter table mensagens change aprovado lido tinyint(1) NOT NULL DEFAULT '0';
update mensagens set lido = 0;
alter table mensagens change destaque aprovado tinyint(1) NOT NULL DEFAULT '0';
update mensagens set aprovado = 0;
alter table mensagens add column espetaculo_id bigint(20) unsigned DEFAULT NULL after aprovado;
rename table mensagens to depoimentos;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
CREATE TABLE `fotos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
INSERT INTO `fotos` VALUES (39,0,'a-bruxinha-b-foto-alberto-greiber_20220302165641PjgNUH4Acu.jpg','2022-03-02 19:56:41','2022-03-02 19:56:41'),(40,0,'acampatorio-2-foto-alberto-rocha-1_20220302165641M1sBrQ8X75.jpg','2022-03-02 19:56:41','2022-03-02 19:56:41'),(41,0,'a-bruxinha-a-foto-alberto-rocha_20220302165641mtGOTt7ij5.JPG','2022-03-02 19:56:41','2022-03-02 19:56:41'),(42,0,'a-bruxinha-c-foto-alberto-greiber_20220302165641P0r7G993xr.jpg','2022-03-02 19:56:41','2022-03-02 19:56:41'),(43,0,'cidade-azul-3-foto-henrique-sitchin_20220302165641SS1NTIP7LV.jpg','2022-03-02 19:56:41','2022-03-02 19:56:41'),(44,0,'construtorio-2-foto-alberto-greiber_20220302165641cKtB4hyJmY.jpg','2022-03-02 19:56:41','2022-03-02 19:56:41'),(45,0,'e-se-as-historias-fossem-diferentes-3-foto-pietro-feliciano_20220302165642TgQPR7kL1Y.JPG','2022-03-02 19:56:42','2022-03-02 19:56:42'),(46,0,'isto-nao-e-um-cachimbo-4_20220302165642R118D8QByY.jpg','2022-03-02 19:56:42','2022-03-02 19:56:42'),(47,0,'isto-nao-e-um-cachimbo-1_20220302165642H9vDhtLu9x.jpg','2022-03-02 19:56:43','2022-03-02 19:56:43'),(48,0,'sonhatorio-1-foto-henrique-sitchin-cia-truks_20220302165642Wge4THs0ll.JPG','2022-03-02 19:56:43','2022-03-02 19:56:43'),(49,0,'o-senhor-dos-sonhos-3-foto-henrique-sitchin_202203021656420ldozdGiCZ.jpg','2022-03-02 19:56:43','2022-03-02 19:56:43'),(50,0,'sonhatorio-b-foto-alberto-greiber_20220302165643t4A2cl2hsT.jpg','2022-03-02 19:56:43','2022-03-02 19:56:43'),(51,0,'o-senhor-dos-sonhos-2-foto-henrique-sitchin_20220302165642u8QyqdBJ6X.jpg','2022-03-02 19:56:43','2022-03-02 19:56:43'),(52,0,'sonhatorio-a-foto-alberto-greiber_202203021656438VEHLydkLl.jpg','2022-03-02 19:56:43','2022-03-02 19:56:43'),(53,0,'ltima-noticia-4-a-foto-henrique-sitchin_20220302165643C8D0ZmT92u.jpg','2022-03-02 19:56:44','2022-03-02 19:56:44'),(54,0,'vovo-foto-henrique-sitchin_20220302165643RkQTb4Brsj.jpg','2022-03-02 19:56:44','2022-03-02 19:56:44'),(55,0,'zoo-ilogico-2-foto-pietro-feliciano_20220302165644aBoGSQwQzS.jpg','2022-03-02 19:56:44','2022-03-02 19:56:44'),(56,0,'zoo-ilogico-4-foto-pietro-feliciano_20220302165644Bfx5P6Olsf.jpg','2022-03-02 19:56:44','2022-03-02 19:56:44'),(58,0,'068r_20220318140333b1ZdNsw7OX.jpg','2022-03-18 17:03:33','2022-03-18 17:03:33'),(59,0,'2677811022-c05989b170_20220318140340WBt7x8nu9L.jpg','2022-03-18 17:03:41','2022-03-18 17:03:41'),(61,0,'big-bang-3-foto-henrique-sitchin_20220318140417bJVplGRFZc.jpg','2022-03-18 17:04:18','2022-03-18 17:04:18'),(62,0,'big-bang-em-cena-14-red_20220318140424JrpLNrGlin.jpg','2022-03-18 17:04:25','2022-03-18 17:04:25'),(64,0,'cidade-azul-em-cena-1-red_20220318140508diqi0t31Om.jpg','2022-03-18 17:05:08','2022-03-18 17:05:08'),(65,0,'por-uma-estrela_20220318140648sYjcGuIQrG.jpg','2022-03-18 17:06:48','2022-03-18 17:06:48'),(66,0,'60356331-1225070770980677-6228259726652604416-o_20220318140719ttERhUMe0M.jpg','2022-03-18 17:07:19','2022-03-18 17:07:19'),(67,0,'espectaculo-ultima-noticia-cia-truks_202203181407467JI4NFSwac.jpg','2022-03-18 17:07:48','2022-03-18 17:07:48'),(68,0,'vovo-1-cia-truks-foto-henrique-sitchin_20220318140803SGf76KYv2d.jpg','2022-03-18 17:08:04','2022-03-18 17:08:04'),(69,0,'vovo-2-foto-henrique-sitchin_20220318140815BW0Am1FKKP.jpg','2022-03-18 17:08:17','2022-03-18 17:08:17'),(70,0,'vovo-3-cia-truks-foto-henrique-sitchin_20220318140836mLUL0atiMV.jpg','2022-03-18 17:08:38','2022-03-18 17:08:38'),(71,0,'acampatorio-85_20220318140853m1vQoNynKL.jpg','2022-03-18 17:08:54','2022-03-18 17:08:54'),(72,0,'img-1650_20220318140909QZeIOvMks0.JPG','2022-03-18 17:09:14','2022-03-18 17:09:14'),(73,0,'o-senhor-dos-sonhos-1-foto-henrique-sitchin_20220318140932vOzf9c8Rdb.jpg','2022-03-18 17:09:34','2022-03-18 17:09:34'),(74,0,'isso-e-coisa-de-crianca-3-foto-alberto-rocha_20220318141009dl8YseLWs5.jpg','2022-03-18 17:10:11','2022-03-18 17:10:11'),(75,0,'isso-e-coisa-de-crianca-6-foto-alberto-rocha_20220318141022aJKqty8QIH.jpg','2022-03-18 17:10:25','2022-03-18 17:10:25');
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
CREATE TABLE `home` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `frase_agenda` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem_fotos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem_videos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link1_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link1_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link2_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link2_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link3_titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link3_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
INSERT INTO `home` VALUES (1,'Confira as datas dos próximos espetáculos e compareça!','acampatorio-11-foto-alberto-rocha_20220302161302FmygfwfGGt.jpg','expedicao-pacifico-8-foto-alberto-rocha_20220302161158QDIH5GdJOx.jpg','CONHEÇA A COMPANHIA','https://www.trupe.net/previa-ciatruks/a-companhia','ASSISTA A UM ESPETÁCULO','https://www.trupe.net/previa-ciatruks/espetaculos','APRENDA COM A GENTE','https://www.trupe.net/previa-ciatruks/cursos-e-oficinas',NULL,'2022-03-02 19:28:59');
UNLOCK TABLES;

--
-- Table structure for table `tipos`
--

DROP TABLE IF EXISTS `tipos`;
CREATE TABLE `tipos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipos`
--

LOCK TABLES `tipos` WRITE;
INSERT INTO `tipos` VALUES (1,'Galeria',NULL,NULL),(2,'Link',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` date NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materias_tipo_id_foreign` (`tipo_id`),
  CONSTRAINT `materias_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materias`
--

LOCK TABLES `materias` WRITE;
INSERT INTO `materias` VALUES (8,1,13,'2016-04-01','foto6_202203021746177fQmoI5k9G.jpg',NULL,NULL,'2022-03-02 20:46:17','2022-03-02 20:46:17'),(9,1,1,'2020-07-01','sonh-3_20220302174738ZtbdfmT0MV.jpg',NULL,NULL,'2022-03-02 20:47:38','2022-03-02 20:47:38'),(10,1,12,'2016-04-01','foto17_20220302174826jZagyNLqZR.jpg',NULL,NULL,'2022-03-02 20:48:26','2022-03-02 20:48:26'),(11,1,2,'2002-08-03','vovo-dib_20220311161357LUnTL3wGvd.jpg',NULL,NULL,'2022-03-11 19:13:58','2022-03-11 19:13:58'),(12,1,34,'2008-09-26','20140127074625757-0001_20220311161752pQVK105uUh.jpg',NULL,NULL,'2022-03-11 19:17:53','2022-03-11 19:17:53'),(13,1,33,'2008-10-11','20140127074831841-0002_20220311161856ummwOxcLq7.jpg',NULL,NULL,'2022-03-11 19:18:57','2022-03-11 19:18:57'),(14,1,29,'2010-05-10','20140127074831841-0003_20220311161942bawEDTujMe.jpg',NULL,NULL,'2022-03-11 19:19:43','2022-03-11 19:19:43'),(15,1,5,'2010-06-11','20140127075001569-0001_202203111620363mZLvzA0qV.jpg',NULL,NULL,'2022-03-11 19:20:36','2022-03-11 19:20:36'),(16,1,32,'2009-06-05','20140127075013187-0001_20220311162115upcZBpK1Cn.jpg',NULL,NULL,'2022-03-11 19:21:15','2022-03-11 19:21:15'),(17,1,14,'2015-09-08','acampatorio-cristiane-rogerio-1_20220311162147D7pj8WjpOA.jpg',NULL,NULL,'2022-03-11 19:21:47','2022-03-11 19:21:47'),(18,1,17,'2015-08-28','acampatorio-dib-carneiro-neto-2_20220311162259R2FhlbpObV.jpg',NULL,NULL,'2022-03-11 19:22:59','2022-03-11 19:22:59'),(19,1,16,'2015-09-02','acampatorio-veja-sp_20220311162335OLEadp48I7.jpg',NULL,NULL,'2022-03-11 19:23:35','2022-03-11 19:23:35'),(20,1,52,'1997-07-11','cidade-azul-1_20220311162421hd9nux7CRd.jpg',NULL,NULL,'2022-03-11 19:24:21','2022-03-11 19:24:21'),(21,1,50,'1998-01-02','cidade-azul-2_20220311162452iz8REWsCU4.jpg',NULL,NULL,'2022-03-11 19:24:53','2022-03-11 19:24:53'),(22,1,49,'1998-03-20','cidade-azul-3_20220311162522te9ArveM4b.jpg',NULL,NULL,'2022-03-11 19:25:22','2022-03-11 19:25:22'),(23,1,48,'1998-03-27','cidade-azul-5_20220311162604UOjqgHpUxU.jpg',NULL,NULL,'2022-03-11 19:26:04','2022-03-11 19:26:04'),(24,1,51,'1997-09-12','cidade-azul-6_20220311162631zigt7nXEC7.jpg',NULL,NULL,'2022-03-11 19:26:32','2022-03-11 19:26:32'),(25,1,41,'2004-04-09','critica-zoo-ilogico-1_2022031116270362sIVf1Ez2.jpg',NULL,NULL,'2022-03-11 19:27:04','2022-03-11 19:27:04'),(26,1,36,'2008-06-06','e-se-as-hist_20220311162733Ssy7mLL6tO.jpg',NULL,NULL,'2022-03-11 19:27:34','2022-03-11 19:27:34'),(27,1,37,'2007-06-05','gigante_20220311162833fSsMBnpwGa.jpg',NULL,NULL,'2022-03-11 19:28:34','2022-03-11 19:28:34'),(28,1,26,'2012-04-12','imagem-por-uma-estrela-crescer_20220311162911GqnCcSbcGy.jpg',NULL,NULL,'2022-03-11 19:29:11','2022-03-11 19:29:11'),(29,1,21,'2012-07-11','img035_202203111630088jgQ4PlES7.jpg',NULL,NULL,'2022-03-11 19:30:09','2022-03-11 19:30:09'),(30,1,25,'2012-08-04','img037_20220311163051YiIuodCYra.jpg',NULL,NULL,'2022-03-11 19:30:53','2022-03-11 19:30:53'),(31,1,35,'2008-06-20','materia-biblioteca-e-se-as-historias-fossem-diferentes_202203111631369aC6WDH1sa.jpg',NULL,NULL,'2022-03-11 19:31:36','2022-03-11 19:31:36'),(32,1,38,'2006-05-11','materia-big-bang_20220311163220BHmYUT5KCn.jpg',NULL,NULL,'2022-03-11 19:32:20','2022-03-11 19:32:20'),(33,1,20,'2014-04-16','materia-construtorio-sesc-pinheiros-veja-recomenda-abril-2014-1_20220311163302ITTRjSW6cg.jpg',NULL,NULL,'2022-03-11 19:33:02','2022-03-11 19:33:02'),(34,1,11,'2016-10-01','materia-exped-pacifico_20220311163335FkBciHzjRt.jpg',NULL,NULL,'2022-03-11 19:33:35','2022-03-11 19:33:35'),(35,1,23,'2013-05-01','materia-veja-2013-mais-vida-inteligente-nos-palcos_20220311163419JmQgqHZRPC.jpg',NULL,NULL,'2022-03-11 19:34:20','2022-03-11 19:34:20'),(36,1,28,'2010-09-03','os-vizinhos-revista-veja-sp_20220311163502VpedTLAIar.jpg',NULL,NULL,'2022-03-11 19:35:02','2022-03-11 19:35:02'),(37,1,7,'2003-06-03','photo-2007-1-24-1-11-37_202203111635378DIZn5MuBP.jpg',NULL,NULL,'2022-03-11 19:35:38','2022-03-11 19:35:38'),(38,1,44,'2002-08-10','photo-2007-1-24-1-13-6_20220311163616uReZASThw7.jpg',NULL,NULL,'2022-03-11 19:36:17','2022-03-11 19:36:17'),(39,1,43,'2003-06-09','photo-2007-1-24-1-15-40_20220311163657iNLSyoSCka.jpg',NULL,NULL,'2022-03-11 19:36:57','2022-03-11 19:36:57'),(40,1,40,'2005-09-04','photo-2007-1-24-1-20-4_20220311163736S0h09h9Zfp.jpg',NULL,NULL,'2022-03-11 19:37:37','2022-03-11 19:37:37'),(41,1,39,'1999-06-18','photo-2007-1-24-1-22-14_202203111640532vE1hzSl8j.jpg',NULL,NULL,'2022-03-11 19:40:54','2022-03-11 19:40:54'),(42,1,46,'1999-06-02','photo-2007-1-24-1-24-15_20220311164141VCFvg74L57.jpg',NULL,NULL,'2022-03-11 19:41:42','2022-03-11 19:41:42'),(43,1,47,'1999-03-10','photo-2007-1-24-1-27-18_202203111642271WEXfzBChS.jpg',NULL,NULL,'2022-03-11 19:42:28','2022-03-11 19:42:28'),(44,1,45,'2002-02-01','photo-2007-1-24-1-31-36_20220311164330mESB2MOX2i.jpg',NULL,NULL,'2022-03-11 19:43:30','2022-03-11 19:43:30'),(45,1,24,'2012-10-31','sem-titulo-1_20220311164505MgyLrwl789.jpg',NULL,NULL,'2022-03-11 19:45:05','2022-03-11 19:45:05'),(46,1,22,'2013-08-14','sonhatorio-estadao_20220311164632rszntAeyvU.jpg',NULL,NULL,'2022-03-11 19:46:32','2022-03-11 19:46:32'),(47,1,27,'2012-02-03','sonhatorio-estadao_20220311164719RTfSOKHuf8.jpg',NULL,NULL,'2022-03-11 19:47:19','2022-03-11 19:47:19'),(48,1,31,'2010-03-05','truks-20-anos-guia-da-folha-05-03-2010_202203111648119VyC74Tdr9.jpg',NULL,NULL,'2022-03-11 19:48:12','2022-03-11 19:48:12'),(49,1,18,'2014-07-02','ultima-noticia-avaliacao-espetaculo-1_20220311164855eQcHtudH4N.jpg',NULL,NULL,'2022-03-11 19:48:55','2022-03-11 19:48:55'),(50,1,19,'2014-06-27','ultima-noticia-guia-da-folha-06-2014_20220311164929g1z2CR5M9V.jpg',NULL,NULL,'2022-03-11 19:49:30','2022-03-11 19:49:30'),(51,1,15,'2014-06-18','ultima-noticia-veja-sp_20220311165030Ml6o32LJQS.jpg',NULL,NULL,'2022-03-11 19:50:31','2022-03-11 19:50:31'),(52,1,30,'2010-03-10','vejinha-10-03-2010_202203111650556ZwPEFA4mN.JPG',NULL,NULL,'2022-03-11 19:50:55','2022-03-11 19:50:55'),(53,1,42,'2004-03-03','zoo-e_20220311165131meD6rA2QOq.jpg',NULL,NULL,'2022-03-11 19:51:31','2022-03-11 19:51:31'),(54,1,3,'2019-09-13','cairo-2_20220311165321V3umJSVIi2.jpg',NULL,NULL,'2022-03-11 19:53:22','2022-03-11 19:53:22'),(55,1,4,'2019-09-13','cairo-3_20220311165352bId7IFoM2n.jpg',NULL,NULL,'2022-03-11 19:53:53','2022-03-11 19:53:53'),(56,1,6,'2018-09-20','egito-1a_20220311165605vwGOUTdUJF.jpg',NULL,NULL,'2022-03-11 19:56:05','2022-03-11 19:56:05'),(57,1,9,'2018-09-14','egito-2a_20220311165628jkzG9dWNdj.jpg',NULL,NULL,'2022-03-11 19:56:28','2022-03-11 19:56:28'),(58,1,10,'2018-09-14','egito-3_20220311165649BbNFdEJtfJ.jpg',NULL,NULL,'2022-03-11 19:56:49','2022-03-11 19:56:49'),(59,1,8,'2018-09-15','egito-7_20220311165708xoMmkckX3t.jpg',NULL,NULL,'2022-03-11 19:57:08','2022-03-11 19:57:08'),(60,1,0,'2012-08-03','sonhatorio-dib_202203111707230ZfTyvyEjx.jpg',NULL,NULL,'2022-03-11 20:07:25','2022-03-11 20:07:25');
UNLOCK TABLES;

--
-- Table structure for table `materias_imagens`
--

DROP TABLE IF EXISTS `materias_imagens`;
CREATE TABLE `materias_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `materia_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materias_imagens_materia_id_foreign` (`materia_id`),
  CONSTRAINT `materias_imagens_materia_id_foreign` FOREIGN KEY (`materia_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materias_imagens`
--

LOCK TABLES `materias_imagens` WRITE;
INSERT INTO `materias_imagens` VALUES (5,9,0,'sonh-3_20220302174748Lm0Gn4IxYq.jpg','2022-03-02 20:47:49','2022-03-02 20:47:49'),(6,9,0,'sonh-4_20220302174749fNyLv0z6hb.jpg','2022-03-02 20:47:49','2022-03-02 20:47:49'),(7,9,0,'sonh-5_20220302174749e7UVA9Fpoa.jpg','2022-03-02 20:47:49','2022-03-02 20:47:49');
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE `noticias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
INSERT INTO `noticias` VALUES (6,'2021-12-12','cidade-azul_20220302163733aqa3Hgu56J.jpg','espetaculo-cidade-azul-faz-apresentacoes-gratuitas-em-macatuba-e-lencois-paulista','ESPETÁCULO CIDADE AZUL FAZ APRESENTAÇÕES GRATUITAS EM MACATUBA E LENÇÓIS PAULISTA','As apresentações do premiado espetáculo CIDADE AZUL são gratuitas','<p>PEÇA É APRESENTADA GRAÇAS AO PATROCÍNIUO DA ZILOR E MINISTÉRIO DO TURISMO - Lei de Incentivo à Cultura do Governo Federal</p><p>As apresentações do premiado espetáculo CIDADE AZUL são gratuitas e acontecem do dia 12 ao dia 14 de dezembro - Não Percam!</p><p>PARA AS CRIANÇAS - PORQUE BRINCAR AINDA É O MELHOR REMÉDIO!</p>','2022-03-02 19:37:33','2022-03-02 19:37:33'),(7,'2021-08-08','logo-sds_20220302163905EKzGyAvYCm.jpg','canal-truks-tv-apresenta-historias-divertidas-com-o-boneco-lucas','CANAL TRUKS TV APRESENTA HISTÓRIAS DIVERTIDAS COM O BONECO LUCAS','O menino Lucas vive incríveis aventuras!','<p>Lucas, o simpático e sonhador menino, protagonista do espetáculo O SENHOR DOS SONHOS, vive incríveis aventuras quando deixa solta a sua imaginação. Vídeos curtos e divertidos para todas as idades!!!</p>','2022-03-02 19:39:05','2022-03-02 19:39:05'),(10,'2022-03-28','capa-noticia_202203291901263txpO0D4bB.png','o-surpreendente-mundo-dos-objetos','O surpreendente mundo dos objetos','Premiado espetáculo da Cia Truks, Sonhatório faz várias apresentações gratuitas em municípios do RJ e do PR','<p>Patrocinado pelo Atacadão e viabilizado pelo Ministério do Turismo, por meio da Lei de Incentivo à Cultura do Governo Federal, a Cia Truks, referência nacional na arte do teatro de animação, apresenta no projeto “O Surpreendente Mundo dos Objetos”, o espetáculo <strong>Sonhatório </strong>(Prêmio APCA e Coca Cola FEMSA) em municípios dos estados do Rio de Janeiro e Paraná. A peça foi criada por <strong>Henrique Sitchin</strong> e <strong>Gabriel Sitchin</strong>, tem texto e direção de <strong>Henrique Sitchin</strong> e elenco formado por <strong>Gabriel Sitchin</strong>, <strong>Rogério Uchoas</strong> e <strong>Thaís Rossi</strong>.</p><p>As apresentações são gratuitas e acontecem nas seguintes datas:</p><p>&nbsp;</p><p><i><strong>RIO DE JANEIRO</strong></i></p><ul><li><strong>Dias 4 e 5/4 – às 10h e às 14h | CIEP 489&nbsp;</strong><br>Rua dos Tupis, 50, Cidade Alegria<br>Resende (RJ)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 6/4 | às 8h, às 9h30, às 13h30 e às 15h30 | ESCOLA MUNICIPAL PROFESSOR WLADIR DE SOUZA TELLES&nbsp;</strong><br>Rua Trinta e Cinco, S/Nº, Jardim Vila Rica&nbsp;<br>Volta Redonda (RJ)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 11/4 | às 9h e às 10h30 | ESCOLA MUNICIPAL OSWALDO CRUZ&nbsp;</strong><br>Av. dos Democráticos, 683, Higienópolis<br>Rio de Janeiro (RJ)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 12/4 | às 9h e às 10h30 | ESCOLA MUNICIPAL D JOÃO VI</strong><br>Rua Darke De Matos, 166, Higienópolis&nbsp;<br>Rio de Janeiro (RJ)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 13/4 | às 8h30, às 10h30, às 13h30 e às 15h30 | COMPLEXO EDUCACIONAL NOSSA SENHORA DAS GRAÇAS</strong><br>Rua Maria D\'Angelo Magliano, 58, Olaria&nbsp;<br>Nova Friburgo (RJ)</li></ul><p>&nbsp;</p><p><i><strong>PARANÁ</strong></i></p><ul><li><strong>Dia 2/5 | às 10h e às 14h | ESCOLA MUNICIPAL ROSA PALMA PLANAS&nbsp;</strong><br>Rua Haiti, 830, Vila Morangueira<br>Maringá (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 3/5 | às 9h e às 10h30 | ESCOLA MUNICIPAL ODETE RIBAROLI&nbsp;</strong><br>Rua Saint Hilaire, 1080, Zona 05&nbsp;<br>Maringá (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 4/5 | às 10h e às 13h30 | ESCOLA MUNICIPAL OLAVO SOARES BARROS Rua Graciosa Buratti, 505, Parque Res. Ana Rosa</strong><br>Cambé (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 5/5 | às 10h e às 13h30 | ESCOLA MUNICIPAL LOURDES GOBI RODRIGUES &nbsp;</strong><br>Rua Francisco Lopes Hernandez, 432, Parque Res. Ana Rosa<br>Cambé (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 10/5 | às 9h e às 14h | COLÉGIO ESTADUAL RUI BARBOSA</strong><br>Av. Wilson Luís Silvério Martins, 61, Santana<br>Guarapuava (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 11/5 | às 9h e às 10h30 | COLÉGIO ESTADUAL PROFESSOR PEDRO CARLI</strong><br>Rua Ernesto Martins, 1111, Vila Bela&nbsp;<br>Guarapuava (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 12/5 | às 10h30 e às 15h30 | ESCOLA SÃO FRANCISCO DE ASSIS&nbsp;</strong><br>Rua Benvenuto Gazi, 2965, Jardim Alphavile&nbsp;<br>Umuarama (PR)</li></ul><p>&nbsp;</p><ul><li><strong>Dia 13/5 | às 10h30 e às 15h30 | ESCOLA MANUEL BANDEIRA&nbsp;</strong><br>Rua Florianópolis, 6085, Alto São Francisco&nbsp;<br>Umuarama (PR)</li></ul><p>&nbsp;</p><p><strong>Sobre o espetáculo</strong><br>É hora do almoço no Sanatório Boa Cabeça. Sentam-se à mesa para a refeição três supostos loucos. Porém, não há nada para comer ou beber. Para passarem o tempo, os amigos resolvem brincar com os objetos que têm à sua volta. É então que partem para uma deliciosa viagem pela imaginação, que os levará para áridos desertos, para o fundo do mar e para longínquos planetas. Incríveis e criativos personagens feitos de guardanapos, bacias, copos, garrafas pet, sacolas plásticas, talheres e pratos os acompanharão por suas “superaventuras”. Após finalmente almoçarem, revela-se ao público a surpresa: eram eles de fato os loucos, ou são loucos aqueles incapazes de brincar? Nossos amigos oferecem ao público um dos melhores remédios para tudo: a possibilidade da construção de uma vida mais saudável, feita da sincera amizade, e de muito bom humor. Eles transformam o que seria um sanatório em um... sonhatório!</p><p>Nesta peça, a Cia Truks utiliza a técnica de teatro conhecida por “Teatro de Objetos”, ou então, como gostam de chamar, à maneira Truks, de “Teatro Com Objetos”. Aqui, o uso cotidiano do objeto é mudado para construir nossas criaturas, ou simbolizar personagens. Uma colher de pau se transforma em uma cozinheira, um algodão pode ser um pintinho, ou, então, uma simpática vaquinha é construída com canecas e um cantil. “Em Sonhatório transformamos sacos de lixo em águas vivas, garrafas térmicas em pinguins, pequenas xícaras em espevitados patinhos, uma chaleira branca em um esplendoroso cisne, entre outras dezenas de criaturas”, contam os criadores.</p><p>Henrique Sitchin conta que o procedimento tem clara e direta relação com o que o educador Jean Piaget definiu como ‘jogo simbólico’. “É uma forma de comparação que as crianças encontram para entenderem o mundo ao seu redor, bem como fortalecerem a sua individualidade. A criança, pela pouca experiência de vida, não tem repertórios para fazer comparações e ou entendimentos racionais, elaborados, de certos assuntos. Então, para isso, elas usam do artifício do jogo simbólico: brincam de ser como o papai, para entenderem, na prática, que são necessárias regras de convívio; brincam de boneca para experimentarem ser como a mamãe; empenham uma espada para sentirem-se fortes como os príncipes e os heróis, conversam com bichinhos imaginários, são capazes de enxergar vida onde não há vida. Passam a conhecer a si mesmas e, a partir daí, terão subsídios também para começar o processo de identificação do outro - prática fundamental para o convívio em sociedade”.</p><p>&nbsp;</p><p><strong>FICHA TÉCNICA&nbsp;</strong></p><ul><li>Criação: &nbsp;HENRIQUE SITCHIN e GABRIEL SITCHIN</li><li>Texto e Direção: HENRIQUE SITCHIN</li><li>Elenco: &nbsp;GABRIEL SITCHIN, ROGÉRIO UCHOAS e THAÍS ROSSI</li><li>Criação e Confecção de Bonecos e Figuras: HENRIQUE SITCHIN, GABRIEL SITCHIN, RAFAEL SENATORE, HUGO REIS, PIETRO &nbsp;SITCHIN, KARINA PRALL E CAMILA OLIVEIRA</li><li>Trilha Sonora: RAFAEL SENATORE</li><li>Iluminação e Cenografia: &nbsp;HENRIQUE SITCHIN</li><li>Produção: ADRYELA RODRIGUES | SENDERO CULTURAL</li></ul><p>&nbsp;</p><p><strong>Serviço</strong><br>Duração: 45 min<br>Classificação indicativa: Livre<br>GRATUITO</p><p>&nbsp;</p><p><strong>Sobre o Atacadão</strong><br>Maior atacadista brasileiro em número de lojas, o Atacadão está presente quase em 180 cidades espalhadas por todos os estados do Brasil. Com uma história de 60 anos e cerca de 62 mil colaboradores, a empresa tem mais de 250 unidades de autosserviço e 33 atacados de entrega que garantem o abastecimento de comerciantes, transformadores e consumidores finais. Também atua no <i>e-commerce</i>, integrando o canal de vendas online do próprio Atacadão com uma robusta operação de <i>marketplace</i>, com mais de 300 <i>sellers </i>parceiros de atacado.</p>','2022-03-29 22:01:27','2022-03-29 22:01:27');
UNLOCK TABLES;

--
-- Table structure for table `noticias_imagens`
--

DROP TABLE IF EXISTS `noticias_imagens`;
CREATE TABLE `noticias_imagens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `noticia_id` bigint(20) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `noticias_imagens_noticia_id_foreign` (`noticia_id`),
  CONSTRAINT `noticias_imagens_noticia_id_foreign` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias_imagens`
--

LOCK TABLES `noticias_imagens` WRITE;
INSERT INTO `noticias_imagens` VALUES (3,10,0,'imagem1_20220329190326nZ81MwW7Bc.jpg','2022-03-29 22:03:26','2022-03-29 22:03:26'),(4,10,0,'capa-site_20220329190326yJAjorV4jB.png','2022-03-29 22:03:27','2022-03-29 22:03:27');
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `politica_de_privacidade`
--

DROP TABLE IF EXISTS `politica_de_privacidade`;
CREATE TABLE `politica_de_privacidade` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `politica_de_privacidade`
--

LOCK TABLES `politica_de_privacidade` WRITE;
INSERT INTO `politica_de_privacidade` VALUES (1,'',NULL,'2022-03-01 04:15:07');
UNLOCK TABLES;

--
-- Table structure for table `premiacoes`
--

DROP TABLE IF EXISTS `premiacoes`;
CREATE TABLE `premiacoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` date DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `premiacoes`
--

LOCK TABLES `premiacoes` WRITE;
INSERT INTO `premiacoes` VALUES (10,0,'1994-01-01','a-bruxinha-foto-vlademir-canela-1_20220302171536lxivdgX0xD.jpg','MAMBEMBE – MINISTÉRIO DA CULTURA','<p>Espetáculo “TRUKS: A BRUXINHA” – CATEGORIA ESPECIAL.</p>','2022-03-02 20:15:37','2022-03-02 20:24:04'),(11,0,'1994-01-01','a-bruxinha-foto-vlademir-canela-1_20220302171632bTMiAzeDta.jpg','INDICAÇÃO AO MAMBEMBE – MINISTÉRIO DA CULTURA','<p>Espetáculo “TRUKS: A BRUXINHA” – MELHOR DIREÇÃO PARA TEATRO INFANTIL.</p>','2022-03-02 20:16:33','2022-03-02 20:24:20'),(12,0,'1996-01-01','espectaculo-cidade-azul-cia-truks_20220302171734cyNzwNXdtT.jpg','ESTÍMULO DA SECRETARIA DE ESTADO DA CULTURA DE SÃO PAULO','<p>Pelo Projeto “CIDADE AZUL”.</p>','2022-03-02 20:17:35','2022-03-02 20:23:43'),(13,0,'1997-01-01','espectaculo-cidade-azul-cia-truks_20220302171806sV0tUI2VoO.jpg','ASSOCIAÇÃO PAULISTA DE CRÍTICOS DE ARTE','<p>Espetáculo “CIDADE AZUL” - MELHOR ESPETÁCULO PARA CRIANÇAS</p>','2022-03-02 20:18:07','2022-03-02 20:22:19'),(14,0,'1997-01-01','espectaculo-cidade-azul-cia-truks_202203021718422uRsgyDSJR.jpg','ASSOCIAÇÃO PAULISTA DE CRÍTICOS DE ARTE','<p>Espetáculo “CIDADE AZUL” - MELHOR TEXTO PARA TEATRO INFANTIL.</p>','2022-03-02 20:18:43','2022-03-02 20:22:30'),(15,0,'1997-01-01','espectaculo-cidade-azul-cia-truks_20220302171928gCJGh6NRSE.jpg','COCA COLA DE TEATRO JOVEM','<p>Com “CIDADE AZUL” - MELHOR ESPETÁCULO DO ANO.</p>','2022-03-02 20:19:28','2022-03-02 20:22:50'),(16,0,'1997-01-01','espectaculo-cidade-azul-cia-truks_20220302172005KLzlUmn1Ax.jpg','COCA COLA DE TEATRO JOVEM','<p>Espetáculo “CIDADE AZUL” - MELHOR DIREÇÃO.</p>','2022-03-02 20:20:06','2022-03-02 20:23:06'),(17,0,'1997-01-01','espectaculo-cidade-azul-cia-truks_20220302172127mnTgAqVKSj.jpg','INDICAÇÃO AO PRÊMIO COCA COLA DE TEATRO JOVEM','<p>Espetáculo “CIDADE AZUL” - MELHOR TEXTO.</p>','2022-03-02 20:21:28','2022-03-02 20:23:21'),(18,0,'1997-01-01',NULL,'MAMBEMBE - MINISTÉRIO DA CULTURA','<p>COMPANHIA TRUKS - GRUPO DESTAQUE DO TEATRO PARA CRIANÇAS DE 1997.</p>','2022-03-02 20:28:51','2022-03-02 20:28:51'),(19,0,'0001-01-01',NULL,'INDICAÇÃO AO PRÊMIO MAMBEMBE - MINISTÉRIO DA CULTURA','<p>&nbsp;COMPANHIA TRUKS – com o espetáculo “CIDADE AZUL” – MELHOR TEXTO PARA TEATRO INFANTIL.</p>','2022-03-02 20:30:41','2022-03-02 20:30:41'),(20,0,'0001-01-01',NULL,'INDICAÇÃO AO PRÊMIO MAMBEMBE - MINISTÉRIO DA CULTURA','<p>COMPANHIA TRUKS – com o espetáculo “CIDADE AZUL” – MELHOR DIREÇÃO PARA TEATRO INFANTIL</p>','2022-03-02 20:31:32','2022-03-02 20:31:32'),(21,0,'1999-01-01',NULL,'INDICAÇÃO AO PRÊMIO COCA COLA DE TEATRO JOVEM','<p>Espetáculo “O SENHOR DOS SONHOS” - MELHOR TEXTO.</p>','2022-03-02 20:32:05','2022-03-02 20:32:05'),(22,0,'1999-01-01',NULL,'INDICAÇÃO AO PRÊMIO COCA COLA DE TEATRO JOVEM','<p>Espetáculo “O SENHOR DOS SONHOS” - MELHOR ATRIZ.</p>','2022-03-02 20:32:28','2022-03-02 20:32:28'),(23,0,'2002-01-01','oficina-de-animacao-036_20220310171319RUXsuqyykm.jpg','PRÊMIO TEATRO CIDADÃO DA PREFEITURA DE SÃO PAULO','<p>PROJETO CENTRO DE ESTUDOS E PRÁTICAS DO TEATRO DE ANIMAÇÃO DE SÃO PAULO.</p>','2022-03-02 20:33:08','2022-03-10 20:13:20'),(24,0,'2002-01-01',NULL,'PRÊMIO PANAMCO NO TEATRO','<p>Espetáculo “VOVÔ” - MELHOR CENOGRAFIA.</p>','2022-03-02 20:33:43','2022-03-02 20:33:43'),(25,0,'2004-01-01',NULL,'ASSOCIAÇÃO PAULISTA DE CRÍTICOS DE ARTE','<p>Espetáculo “ZÔO-ILÓGICO” - PARCERIA COM O GRUPO CIRCO DE BONECOS - MELHOR ATOR PARA TEATRO INFANTIL.</p>','2022-03-02 20:34:20','2022-03-02 20:34:20'),(26,0,'0004-01-01',NULL,'PRÊMIO PANAMCO NO TEATRO','<p>Espetáculo “ZÔO-ILÓGICO” – CATEGORIA ESPECIAL – INOVAÇÃO DE LINGUAGEM TEATRAL.</p>','2022-03-02 20:34:51','2022-03-02 20:34:51'),(27,0,'2004-01-01',NULL,'INDICAÇÃO AO PRÊMIO PANAMCO NO TEATRO','<p>Espetáculo “ZÔO-ILÓGICO” – MELHOR ESPETÁCULO.</p>','2022-03-02 20:35:24','2022-03-02 20:35:24'),(28,0,'2004-01-01',NULL,'INDICAÇÃO AO PRÊMIO PANAMCO NO TEATRO','<p>Espetáculo “ZÔO-ILÓGICO” – MELHOR DIREÇÃO.</p>','2022-03-02 20:35:50','2022-03-02 20:35:50'),(29,0,'2005-01-01',NULL,'FESTIVAL INTERNACIONAL DE TEATRO DE BONECOS DE CANELA','<p>Espetáculo “O SENHOR DOS SONHOS” - JURI POPULAR - MELHOR ESPETÁCULO.</p>','2022-03-02 20:37:18','2022-03-02 20:37:18'),(30,0,'2004-01-01',NULL,'PRÊMIO PANAMCO NO TEATRO','<p>Espetáculo “INZÔONIA” – PARCERIA COM O GRUPO CIRCO DE BONECOS - MELHOR ESPETÁCULO DE ANIMAÇÃO.</p>','2022-03-02 20:37:45','2022-03-02 20:37:45'),(31,0,'2006-01-01',NULL,'PRÊMIO APCA','<p>Espetáculo \"GUARDA ZOOL\" - PARCERIA COM O GRUPO CIRCO DE BONECOS - MELHOR DIREÇÃO.</p>','2022-03-02 20:38:10','2022-03-02 20:38:10'),(32,0,'2006-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “BIG BANG”, na Categoria MELHOR ESPETÁCULO JOVEM.</p>','2022-03-02 20:38:35','2022-03-02 20:38:35'),(33,0,'2006-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “GIGANTE”, na Categoria MELHOR CENOGRAFIA.</p>','2022-03-02 20:38:57','2022-03-02 20:38:57'),(34,0,'2006-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “BIG BANG”, na Categoria ESPECIAL – PELA MONTAGEM DE TEATRO DE ANIMAÇÃO PARA O PÚBLICO JOVEM.</p>','2022-03-02 20:39:24','2022-03-02 20:39:24'),(35,0,'2008-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “E SE AS HISTÓRIAS FOSSEM DIFERENTES?” - MELHOR ESPETÁCULO INFANTIL.</p>','2022-03-02 20:39:51','2022-03-02 20:39:51'),(36,0,'2008-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “E SE AS HISTÓRIAS FOSSEM DIFERENTES?” – MELHOR ATOR.</p>','2022-03-02 20:40:17','2022-03-02 20:40:17'),(37,0,'2009-01-01',NULL,'INDICAÇÃO AO PRÊMIO COOPERATIVA PAULISTA DE TEATRO','<p>Publicação do livro “A POSSIBILIDADE DO NOVO NO TEATRO DE ANIMAÇÃO”.</p>','2022-03-02 20:40:45','2022-03-02 20:40:45'),(38,0,'2012-01-01','sonhatorio-a-foto-alberto-greiber_20220318142305zE6TQVPktd.jpg','INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - MELHOR ESPETÁCULO INFANTIL - FINALISTA</p>','2022-03-02 20:41:18','2022-03-18 17:23:05'),(39,0,'2012-01-01','sonhatorio-a-foto-alberto-greiber_20220318142405TqBjS4SrcP.jpg','INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - MELHOR TEXTO.</p>','2022-03-02 20:41:43','2022-03-18 17:24:06'),(40,0,'2012-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - MELHOR DIREÇÃO - FINALISTA</p>','2022-03-02 20:42:06','2022-03-02 20:42:06'),(41,0,'2012-01-01',NULL,'PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - MELHOR TRILHA SONORA</p>','2022-03-02 20:42:35','2022-03-02 20:42:35'),(42,0,'2012-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - MELHOR PRODUÇÃO</p>','2022-03-02 20:42:59','2022-03-02 20:42:59'),(43,0,'2012-01-01',NULL,'PRÊMIO FEMSA','<p>Espetáculo “SONHATÓRIO” - ATORES REVELAÇÃO DO ANO.</p>','2022-03-02 20:43:20','2022-03-02 20:43:20'),(44,0,'2012-01-01',NULL,'INDICAÇÃO AO PRÊMIO FEMSA','<p>Espetáculo “POR UMA ESTRELA” - CATEGORIA ESPECIAL, PELA QUALIDADE DE ANIMAÇÃO DOS BONECOS DO ESPETÁCULO.</p>','2022-03-02 20:43:50','2022-03-02 20:43:50'),(45,0,'2012-01-01',NULL,'PRÊMIO APCA','<p>Espetáculo \" SONHATÓRIO\" - ATORES REVELAÇÃO DO ANO</p>','2022-03-02 20:44:28','2022-03-02 20:44:28'),(46,0,NULL,'oficina-de-animacao-036_202203311127164FBbejwfST.jpg','PRÊMIO TEATRO CIDADÃO - SP','<p>Projeto Centro de Estudos e Práticas do Teatro de Animação de São Paulo</p>','2022-03-31 14:27:17','2022-03-31 14:27:17');
UNLOCK TABLES;

--
-- Table structure for table `projetos_especiais`
--

DROP TABLE IF EXISTS `projetos_especiais`;
CREATE TABLE `projetos_especiais` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `texto` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projetos_especiais`
--

LOCK TABLES `projetos_especiais` WRITE;
INSERT INTO `projetos_especiais` VALUES (7,1,'<p><br><strong>A Cia. Truks trabalha com a criação, produção e encenação de vídeos e performances teatrais diversas com bonecos e ou objetos animados. Elabora e produz roteiros para campanhas publicitárias, clipes e afins. Veja alguns destes trabalhos, como o Canal TRUKS TV &nbsp;- </strong><a href=\"https://www.youtube.com/c/TRUKSTV\"><strong>https://www.youtube.com/c/TRUKSTV</strong></a></p><p><strong>São dezenas de vídeos super divertidos, para toda a família!</strong></p>','FUQDfMjKsGk','2022-03-10 11:35:52','2022-03-13 18:21:07'),(8,2,NULL,'TqJVh3NP2Vo','2022-03-10 11:38:44','2022-03-10 11:38:44'),(9,3,NULL,'0QtLbdxHHF0','2022-03-13 18:04:50','2022-03-13 18:04:50'),(10,6,'<p><strong>Vídeos feitos para a “GOLDEN CROSS”, por meio da agência “LEW LARA”, em ocasião dos Jogos Panamericanos de 2007, quando os filmes promocionais, feitos com bonecos e animadores do grupo foram veiculados nacionalmente, alcançando excelente aceitação.</strong></p>','cXAkRwq6oFo','2022-03-13 18:06:40','2022-03-13 18:09:59'),(11,7,NULL,'4tuj0uOMNlU','2022-03-13 18:07:29','2022-03-13 18:07:29'),(12,5,'<p><strong>Em outra marcante iniciativa, Truks trabalhou para a empresa “DOVE”, em filme criado pela Agência OGILVY, com parceria da &nbsp;produtora “BOSSA NOVA FILMS”. Aqui, procedimentos da animação misturam-se à ação de atores de carne e osso, produzindo um resultado cênico fantástico.</strong></p>','yxgG7r3ZXBU','2022-03-13 18:11:28','2022-03-13 18:13:44'),(13,8,'<p><strong>A Cia. Truks trabalhou, ainda no clipe de ARNALDO ANTUNES da música \"LONGE\", em que dá vida a toda a sorte de elementos objetos que “acompanham a viagem” do músico.</strong></p>','4pTnFUQtj7k','2022-03-13 18:15:42','2022-03-13 18:15:42'),(14,9,'<p><strong>Em outro trabalho exposto mais abaixo, a Cia. Truks anima objetos em criativo filme comercial da empresa NOKIA, que recebeu prêmios no México, pela inovação de linguagem e qualidade artística.</strong></p>','RmyffUN4rTE','2022-03-13 18:17:34','2022-03-13 18:17:34'),(15,10,'<p><strong>Truks trabalha, ainda, na montagem e criação de espetáculos promocionais para empresas ou afins. A partir do tema proposto pelo contratante, podem ser elaboradas pequenas peças para serem apresentadas em feiras de comércio, lançamentos de produtos, campanhas publicitárias, exposições e etc.</strong></p><p><strong>Truks já trabalhou, nestas iniciativas, para importantes empresas, em feiras como o Salão do Automóvel e Feira UD, além de inúmeras campanhas publicitárias ou beneficentes.</strong></p><p><strong>A Cia. prestou serviços para as seguintes empresas, entre outras:</strong></p><p><strong>BANCO BRADESCO</strong><br><strong>GRADIENTE</strong><br><strong>ALBARUS AUTO PEÇAS</strong><br><strong>CONSTRUTORA CAMARGO CORRÊA</strong><br><strong>DSB CELULARES</strong><br><strong>ASSOCIAÇÃO BRASILEIRA DAS INDÚSTRIAS DE VIDRO</strong><br><strong>PRICE WATERHOUSE</strong><br><strong>GRUPO GERDAU</strong><br><strong>GRUPO ACHÈ</strong></p>',NULL,'2022-03-13 18:18:51','2022-03-13 18:18:51'),(16,4,'<p><strong>Série de vídeos produzidos para a UOL, sobre os craques da seleção brasileira - Copa de 2018</strong></p>','jopgRh4rgLs','2022-03-18 17:17:29','2022-03-18 17:17:29');
UNLOCK TABLES;

--
-- Table structure for table `subtitulos`
--

DROP TABLE IF EXISTS `subtitulos`;
CREATE TABLE `subtitulos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subtitulos`
--

LOCK TABLES `subtitulos` WRITE;
INSERT INTO `subtitulos` VALUES (1,'A Companhia','a-companhia','O GRUPO: Cia Truks - Teatro de Bonecos',NULL,NULL),(2,'Agenda','agenda','Acompanhe aqui todas as nossas apresentações e compareça!',NULL,NULL),(3,'Espetáculos','espetaculos','Conheça cada detalhe de todos os nossos espetáculos',NULL,NULL),(4,'Cursos e Oficinas','cursos-e-oficinas','Aprenda com a gente: Nossa metodologia de trabalho',NULL,NULL),(5,'Projetos Especiais','projetos-especiais','Saiba mais sobre alguns projetos em que trabalhamos',NULL,NULL),(6,'Centro de Estudos','centro-de-estudos','Centro de estudos e práticas do teatro de animação',NULL,NULL),(7,'Captação de Patrocínios','captacao-de-patrocinios','Invista na Cia Truks',NULL,'2022-03-31 15:29:33'),(8,'Contratações','contratacoes','Informações gerais',NULL,NULL),(9,'Galeria de Fotos','galeria-de-fotos','Confira momentos inesquecíveis da Cia Truks',NULL,NULL),(10,'Vídeos','videos','Nossos registros animados',NULL,NULL),(11,'Notícias','noticias','Acompanhe aqui todas as nossas novidades!',NULL,NULL),(12,'Premiações','premiacoes','Reconhecimento e orgulho!',NULL,NULL),(13,'Críticas e Matérias','criticas-e-materias','Onde fomos citados e comentados',NULL,NULL),(14,'Contato','contato','Fale conosco',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net',NULL,'$2y$10$KdN74HfFHRVs16KLEo8if.x8koCMpJ2GplV60LWWZicM4m/wqavca',NULL,NULL,NULL),(2,'henrique','contato@escapejunior.com.br',NULL,'$2y$10$HMVisyjMGdU8gKyOjSGxRO5Nag71OZREATcrH9rgyAx1KRB5VSr1S',NULL,'2022-03-04 13:34:42','2022-03-04 13:34:42');
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
INSERT INTO `videos` VALUES (1,0,'fyRlJJteVUI','2022-03-18 17:12:42','2022-03-18 17:12:42'),(2,0,'fCEkEbZAwJs','2022-03-18 17:13:35','2022-03-18 17:13:35'),(4,0,'hnELqBEL3A4','2022-03-18 17:15:11','2022-03-18 17:15:11');
UNLOCK TABLES;


-- Dump completed on 2022-03-31 20:44:33
